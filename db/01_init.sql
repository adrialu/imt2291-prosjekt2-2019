-- install pgcrypto
CREATE EXTENSION pgcrypto;

-- create enumerated user type
CREATE TYPE userType AS ENUM ('student', 'teacher', 'admin');

-- create "users" table, containing all users
CREATE TABLE IF NOT EXISTS users (
	id         serial,
	givenname  text NOT NULL,
	familyname text NOT NULL,
	email      text NOT NULL UNIQUE,
	password   text NOT NULL,
	type       userType DEFAULT 'student',
	verifyme   boolean DEFAULT FALSE,
	PRIMARY KEY (id)
);

-- create "videos" table, containing all videos
CREATE TABLE IF NOT EXISTS videos (
	id        serial,
	userid    integer NOT NULL,
	title     text NOT NULL,
	descr     text NOT NULL,
	course    text NOT NULL,
	subject   text NOT NULL,
	views     integer DEFAULT 0,
	uploaded  timestamp with time zone DEFAULT NOW(),
	PRIMARY KEY (id),
	FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE
);

-- create "video_rating" table, containing the user's ratings of videos
CREATE TABLE IF NOT EXISTS video_rating (
	videoid integer NOT NULL,
	userid  integer NOT NULL,
	rating  integer NOT NULL,
	PRIMARY KEY (videoid, userid),
	FOREIGN KEY (videoid) REFERENCES videos(id) ON DELETE CASCADE,
	FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE,
	CHECK (rating >= 0 AND rating <= 5)
);

-- create "video_comments" table, containing the user's comments on videos
CREATE TABLE IF NOT EXISTS video_comments (
	id      serial,
	videoid integer NOT NULL,
	userid  integer NOT NULL,
	comment text NOT NULL,
	created timestamp with time zone DEFAULT NOW(),
	PRIMARY KEY (id),
	FOREIGN KEY (videoid) REFERENCES videos(id) ON DELETE CASCADE,
	FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE
);

-- create "playlists" table, containing all playlists and their video orders
CREATE TABLE IF NOT EXISTS playlists (
	id        serial,
	userid    integer NOT NULL,
	title     text NOT NULL,
	descr     text NOT NULL,
	videos    integer[] NOT NULL,
	updated   timestamp with time zone DEFAULT NOW(),
	PRIMARY KEY (id),
	FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE
);

-- create "playlist_subscriptions" table, containing all the playlist subscriptions by users
CREATE TABLE IF NOT EXISTS playlist_subscriptions (
	playlistid integer NOT NULL,
	userid     integer NOT NULL,
	PRIMARY KEY (playlistid, userid),
	FOREIGN KEY (playlistid) REFERENCES playlists(id) ON DELETE CASCADE,
	FOREIGN KEY (userid) REFERENCES users(id) ON DELETE CASCADE
);

-- create a function that updates the 'updated' timestamp
CREATE OR REPLACE FUNCTION update_updated_timestamp_column()
RETURNS TRIGGER AS $$
BEGIN
	NEW.updated = NOW();
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- set a update trigger for the 'playlists' table using the above function
CREATE TRIGGER update_playlists_timestamp BEFORE UPDATE ON playlists
FOR EACH ROW EXECUTE PROCEDURE update_updated_timestamp_column();
