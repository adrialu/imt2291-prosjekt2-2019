-- create an admin user
INSERT INTO users (
	email,
	password,
	givenname,
	familyname,
	type
) VALUES (
	'admin@example.com',
	crypt('imt2291', gen_salt('bf')),
	'Admin',
	'Admin',
	'admin'
);

-- make sure the admin user can't be deleted
CREATE RULE protect_admin AS ON DELETE TO users WHERE id = 1 DO INSTEAD NOTHING;
