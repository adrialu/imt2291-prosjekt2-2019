<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';
require_once '../html/classes/Video.php';

class MockVideo extends Video {
	// mock video that lets us set the user manually, the video itself
	// is deleted by cascading deletion of the authoring (mock) user
	function __construct(PDO $dbh, int $vid = -1, int $uid = -1){
		parent::__construct($dbh, $vid);
		$this->user = new User($dbh, $uid);
	}

	public function setMockUser(User $user){
		$this->user = $user;
	}
}

class VideoTest extends \Codeception\Test\Unit {
	use Codeception\AssertThrows;

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	private $dbh;
	private $users;
	private $video;
	private $rnd_token;

	protected function _before(){
		$this->dbh = DB::getConnection();
		$this->video = new MockVideo($this->dbh);

		$this->users = array();

		$this->rnd_token = substr(md5(mt_rand()), 0, 50);
	}

	protected function _after(){
		// cleanup
		foreach($this->users as $user){
			$user->destroy();
		}
	}

	// tests
	public function testUpload(){
		// the file/thumbnail upload is done outside of the class as part of the upload API

		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can upload new videos', function(){
			$this->video->upload();
		});

		$this->video->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can upload new videos', function(){
			$this->video->upload();
		});

		$this->video->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->title = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->title = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->title = $this->rnd_token;
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->description = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->description = str_repeat('x', 501);
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->description = $this->rnd_token;
		$this->assertThrowsWithMessage(Exception::class, 'Course is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->course = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Course is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->course = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Course is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->course = $this->rnd_token;
		$this->assertThrowsWithMessage(Exception::class, 'Subject is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->subject = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Subject is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->subject = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Subject is either empty or out of bounds', function(){
			$this->video->upload();
		});

		$this->video->subject = $this->rnd_token;
		$videoID = $this->video->upload();
		$this->assertGreaterThan(0, $videoID, 'Video was not created correctly');
	}

	public function testUpdate(){
		$this->video = $this->createVideo();
		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can edit videos', function(){
			$this->video->update();
		});

		$this->video->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can edit videos', function(){
			$this->video->update();
		});

		$this->video->setMockUser($this->createUser(UserType::Teacher));

		$this->video->title = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->title = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->title = $this->rnd_token;

		$this->video->description = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->description = str_repeat('x', 501);
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->description = $this->rnd_token;
		$this->video->course = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Course is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->course = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Course is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->course = $this->rnd_token;
		$this->video->subject = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Subject is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->subject = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Subject is either empty or out of bounds', function(){
			$this->video->update();
		});

		$this->video->subject = $this->rnd_token;
		$this->video->update();
	}

	public function testGetAuthor(){
		$this->assertThrowsWithMessage(Exception::class, 'Video doesn\'t exist', function(){
			$this->video->getAuthor();
		});

		$this->video = $this->createVideo();

		$author = $this->video->getAuthor();
		$this->assertEquals($author, $this->users[0], 'Author doesn\'t exist');
	}

	public function testGetAllFromUser(){
		$user = $this->createUser(UserType::Teacher);
		$this->video->setMockUser($user);

		$result = $this->video->getAllFromUser();
		$this->assertCount(0, $result, 'User shouldn\'t have any videos');

		$this->createVideo($user);
		$this->createVideo($user);
		$this->createVideo($user);

		$result = $this->video->getAllFromUser();
		$this->assertCount(3, $result, 'User has incorrect amount of videos');
	}

	public function testGetAllFromUserWithUserArgument(){
		$user = $this->createUser(UserType::Teacher);

		$result = $this->video->getAllFromUser($user);
		$this->assertCount(0, $result, 'User shouldn\'t have any videos');

		$this->createVideo($user);
		$this->createVideo($user);
		$this->createVideo($user);

		$result = $this->video->getAllFromUser($user);
		$this->assertCount(3, $result, 'User has incorrect amount of videos');
	}

	public function testIncreaseViewCount(){
		$this->createVideo();
		$this->assertEquals(0, $this->video->views, 'Video shouldn\'t have any views');

		$this->video->increaseViewCount();
		$this->video->increaseViewCount();
		$this->video->increaseViewCount();

		$this->assertEquals(3, $this->video->views, 'Video has incorrect amount of views');
	}

	public function testGetOrSetRating(){
		$this->video = $this->createVideo();

		$this->video->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can have ratings', function(){
			$this->video->getRating();
		});

		$this->assertThrowsWithMessage(Exception::class, 'Only students can rate videos', function(){
			$this->video->rate(0);
		});

		$this->video->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can have ratings', function(){
			$this->video->getRating();
		});

		$this->assertThrowsWithMessage(Exception::class, 'Only students can rate videos', function(){
			$this->video->rate(0);
		});

		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Score is out of bounds', function(){
			$this->video->rate(-1);
		});

		$this->assertThrowsWithMessage(Exception::class, 'Score is out of bounds', function(){
			$this->video->rate(6);
		});

		$this->video->rate(3);
		$this->assertEquals(3, $this->video->getRating(), 'Video shouldn\'t be rated');
	}

	public function testGetAverageRating(){
		$this->video = $this->createVideo();
		$this->assertEquals(0, $this->video->getAverageRating(), 'Video has incorrect average rate');

		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->video->rate(3);
		$this->assertEquals(3, $this->video->getAverageRating(), 'Video has incorrect average rate');

		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->video->rate(1);
		$this->video->setMockUser($this->createUser(UserType::Student));
		$this->video->rate(4);

		$rating = $this->video->getAverageRating();
		$this->assertTrue($rating > 2.6 && $rating < 2.7, 'Video has incorrect average rate');
	}

	public function testSearch(){
		$result = $this->video->search($this->rnd_token);
		$this->assertEmpty($result, 'Search should not result anything with this query');

		$video = $this->createVideo(); // create a mock video, now we ought to get one hit
		$result = $this->video->search($this->rnd_token);

		$this->assertNotEmpty($result, 'Search should not result anything with this query');
		$this->assertCount(1, $result, 'Search results didn\'t contain as expected');
		$this->assertEquals($video->getID(), $result[0]->getID(), 'Search results didn\'t contain the video');
	}

	// utility
	private function createUser($userType = UserType::Student){
		$user = new User($this->dbh);
		$user->email = substr(md5(mt_rand()), 0, 30) . '@example.com';
		$user->password = $this->rnd_token;
		$user->givenName = $this->rnd_token;
		$user->familyName = $this->rnd_token;
		$user->register();

		if($userType != UserType::Student)
			$user = $this->changeUserType($user, $userType);

		// make sure we clean up afterwards
		array_push($this->users, $user);

		return $user;
	}

	private function changeUserType(User $user, $userType){
		// invoke the admin user to do these actions
		$admin = new User($this->dbh, 1);
		$admin->changeUserType($user, $userType);
		return new User($this->dbh, $user->getID());
	}

	private function createVideo(User $author = null){
		if(!isset($author))
			$author = $this->createUser(UserType::Teacher);

		$video = new MockVideo($this->dbh, -1, $author->getID());
		$video->title = $this->rnd_token;
		$video->description = $this->rnd_token;
		$video->course = $this->rnd_token;
		$video->subject = $this->rnd_token;
		$video->upload();

		return $video;
	}

	// public methods not tested:
	// - getID (so simple it can't break, no need to test)
	// - getMime (depends on file system)
	// - destroy (being used in cleanup is enough of a test in my opinion)
}
