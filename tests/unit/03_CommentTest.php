<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';
require_once '../html/classes/Video.php';
require_once '../html/classes/Comment.php';

class MockComment extends Comment {
	// mock video that lets us set the user manually, the video itself
	// is deleted by cascading deletion of the authoring (mock) user
	function __construct(PDO $dbh, int $vid = -1, int $cid = -1, int $uid = -1){
		parent::__construct($dbh, $vid, $cid);
		$this->user = new User($dbh, $uid);
	}

	public function setMockUser(User $user){
		$this->user = $user;
	}

	public function setMockVideo(Video $video){
		$this->vid = $video->getID();
	}
}

class CommentTest extends \Codeception\Test\Unit {
	use Codeception\AssertThrows;

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	private $dbh;
	private $comment;
	private $users;
	private $rnd_token;

	protected function _before(){
		$this->dbh = DB::getConnection();
		$this->comment = new MockComment($this->dbh);

		$this->users = array();

		$this->rnd_token = substr(md5(mt_rand()), 0, 50);
	}

	protected function _after(){
		// cleanup
		foreach($this->users as $user){
			$user->destroy();
		}
	}

	// tests
	public function testCreate(){
		$this->comment->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students and teachers can add comments', function(){
			$this->comment->create('');
		});

		$this->comment->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'No video ID context', function(){
			$this->comment->create('');
		});

		$this->comment->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'No video ID context', function(){
			$this->comment->create('');
		});

		$video = $this->createVideo();
		$this->comment->setMockVideo($video);
		$this->assertThrowsWithMessage(Exception::class, 'Comment content is out of bounds', function(){
			$this->comment->create('x');
		});

		$this->assertThrowsWithMessage(Exception::class, 'Comment content is out of bounds', function(){
			$this->comment->create(str_repeat('x', 501));
		});

		$id = $this->comment->create($this->rnd_token);
		$this->assertGreaterThan(0, $id, 'Comment was not created correctly');
	}

	public function testGetAll(){
		$this->assertThrowsWithMessage(Exception::class, 'No video ID context', function(){
			$this->comment->getAll();
		});

		$video = $this->createVideo();
		$this->comment->setMockVideo($video);

		$this->assertCount(0, $this->comment->getAll(), 'Video had incorrect amount of comments');

		$this->createComment(null, $video);
		$this->createComment(null, $video);
		$this->createComment(null, $video);

		$this->assertCount(3, $this->comment->getAll(), 'Video had incorrect amount of comments');
	}

	public function testGetAllFromUser(){
		$video = $this->createVideo();
		$this->comment->setMockVideo($video);

		$author = $this->createUser();
		$this->comment->setMockUser($author);

		$this->assertCount(0, $this->comment->getAllFromUser(), 'User had incorrect amount of comments');

		$this->createComment($author);
		$this->createComment($author);
		$this->createComment($author);
		$this->createComment($this->createUser());

		$this->assertCount(3, $this->comment->getAllFromUser(), 'User had incorrect amount of comments');
	}

	public function testGetAllFromUserWithUserArgument(){
		$video = $this->createVideo();
		$this->comment->setMockVideo($video);

		$author = $this->createUser();
		$this->assertCount(0, $this->comment->getAllFromUser($author), 'User had incorrect amount of comments');

		$this->createComment($author);
		$this->createComment($author);
		$this->createComment($author);
		$this->createComment($this->createUser());

		$this->assertCount(3, $this->comment->getAllFromUser($author), 'User had incorrect amount of comments');
	}

	public function testGetVideo(){
		$this->assertThrowsWithMessage(Exception::class, 'No video ID context', function(){
			$this->comment->getVideo();
		});

		$video = $this->createVideo();
		$this->comment->setMockVideo($video);

		$this->assertEquals($video->getID(), $this->comment->getVideo()->getID(), 'Incorrect video returned');
	}

	// utility
	private function createUser($userType = UserType::Student){
		$user = new User($this->dbh);
		$user->email = substr(md5(mt_rand()), 0, 30) . '@example.com';
		$user->password = $this->rnd_token;
		$user->givenName = $this->rnd_token;
		$user->familyName = $this->rnd_token;
		$user->register();

		if($userType != UserType::Student)
			$user = $this->changeUserType($user, $userType);

		// make sure we clean up afterwards
		array_push($this->users, $user);

		return $user;
	}

	private function changeUserType(User $user, $userType){
		// invoke the admin user to do these actions
		$admin = new User($this->dbh, 1);
		$admin->changeUserType($user, $userType);
		return new User($this->dbh, $user->getID());
	}

	private function createVideo(User $author = null){
		if(!isset($author))
			$author = $this->createUser(UserType::Teacher);

		$video = new MockVideo($this->dbh, -1, $author->getID());
		$video->title = $this->rnd_token;
		$video->description = $this->rnd_token;
		$video->course = $this->rnd_token;
		$video->subject = $this->rnd_token;
		$video->upload();

		return $video;
	}

	private function createComment(User $author = null, Video $video = null){
		if(!isset($author))
			$author = $this->createUser(UserType::Student);

		if(!isset($video))
			$video = $this->createVideo();

		$comment = new MockComment($this->dbh, $video->getID(), -1, $author->getID());
		$comment->create($this->rnd_token);

		return $comment;
	}
}
