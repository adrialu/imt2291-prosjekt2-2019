<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';

class MockUser extends User {
	// mock user that deletes itself
	function __destruct(){
		$this->destroy();
	}
}

class UserTest extends \Codeception\Test\Unit {
	use Codeception\AssertThrows;

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	private $dbh;
	private $user;
	private $rnd_token;
	private $rnd_email_token;

	protected function _before(){
		$this->dbh = DB::getConnection();
		$this->user = new MockUser($this->dbh);

		$this->rnd_token = substr(md5(mt_rand()), 0, 80);
		$this->rnd_email_token = substr(md5(mt_rand()), 0, 30) . '@example.com';
	}

	// tests
	public function testRegister(){
		$this->assertThrowsWithMessage(Exception::class, 'Email is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->email = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Email is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->email = str_repeat('x', 81);
		$this->assertThrowsWithMessage(Exception::class, 'Email is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->email = substr(md5(mt_rand()), 0, 80);
		$this->assertThrowsWithMessage(Exception::class, 'Email is not valid', function(){
			$this->user->register();
		});

		$this->user->email = $this->rnd_email_token;
		$this->assertThrowsWithMessage(Exception::class, 'Password is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->password = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Password is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->password = str_repeat('x', 81);
		$this->assertThrowsWithMessage(Exception::class, 'Password is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->password = 'xxxxxx';
		$this->assertThrowsWithMessage(Exception::class, 'Given name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->givenName = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Given name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->givenName = str_repeat('x', 81);
		$this->assertThrowsWithMessage(Exception::class, 'Given name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->givenName = 'xxx';
		$this->assertThrowsWithMessage(Exception::class, 'Family name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->familyName = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Family name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->familyName = str_repeat('x', 81);
		$this->assertThrowsWithMessage(Exception::class, 'Family name is either empty or out of bounds', function(){
			$this->user->register();
		});

		$this->user->familyName = 'xxx';
		$this->user->register();

		$this->assertGreaterThan(0, $this->user->getID(), 'User ID was not as expected');
	}

	public function testChangeUserType(){
		$this->user = $this->createUser(UserType::Student);
		$this->assertThrowsWithMessage(Exception::class, 'Unauthorized access', function(){
			$this->user->changeUserType($this->user, UserType::Teacher);
		});

		$this->user = $this->createUser(UserType::Teacher);
		$this->assertThrowsWithMessage(Exception::class, 'Unauthorized access', function(){
			$this->user->changeUserType($this->user, UserType::Teacher);
		});

		$user = $this->createUser(); // student by default

		$this->user = $this->createUser(UserType::Admin);
		$this->user->changeUserType($user, UserType::Teacher);

		$user = new MockUser($this->dbh, $user->getID()); // need to fetch the user again to get updated info
		$this->assertTrue($user->isTeacher(), 'Failed to change user type');
	}

	public function testIsStudent(){
		$user = $this->createUser(UserType::Student);
		$this->assertTrue($user->isStudent(), 'Got wrong result');

		$user = $this->createUser(UserType::Teacher);
		$this->assertFalse($user->isStudent(), 'Got wrong result');

		$user = $this->createUser(UserType::Admin);
		$this->assertFalse($user->isStudent(), 'Got wrong result');
	}

	public function testIsTeacher(){
		$user = $this->createUser(UserType::Student);
		$this->assertFalse($user->isTeacher(), 'Got wrong result');

		$user = $this->createUser(UserType::Teacher);
		$this->assertTrue($user->isTeacher(), 'Got wrong result');

		$user = $this->createUser(UserType::Admin);
		$this->assertFalse($user->isTeacher(), 'Got wrong result');
	}

	public function testIsAdmin(){
		$user = $this->createUser(UserType::Student);
		$this->assertFalse($user->isAdmin(), 'Got wrong result');

		$user = $this->createUser(UserType::Teacher);
		$this->assertFalse($user->isAdmin(), 'Got wrong result');

		$user = $this->createUser(UserType::Admin);
		$this->assertTrue($user->isAdmin(), 'Got wrong result');
	}

	public function testSearch(){
		$result = $this->user->search($this->rnd_token);
		$this->assertEmpty($result, 'Search should not result anything with this query');

		$user = $this->createUser(); // create a mock user, now we ought to get one hit
		$result = $user->search($this->rnd_token);

		$this->assertNotEmpty($result, 'Search should result with this query');
		$this->assertCount(1, $result, 'Search results didn\'t contain as expected');
		$this->assertEquals($user->getID(), $result[0]->getID(), 'Search results didn\'t contain the user');
	}

	public function testGetAll(){
		$result = $this->user->getAll();
		$count = sizeof($result);

		$_ = $this->createUser(); // have to assign it to a variable, else it gets destroyed immediately
		$result = $this->user->getAll();

		$this->assertCount($count + 1, $result, 'Got wrong number of items');
	}

	// utility
	private function createUser($userType = UserType::Student){
		$user = new MockUser($this->dbh);
		$user->email = substr(md5(mt_rand()), 0, 30) . '@example.com';
		$user->password = $this->rnd_token;
		$user->givenName = $this->rnd_token;
		$user->familyName = $this->rnd_token;
		$user->register();

		if($userType != UserType::Student)
			$user = $this->changeUserType($user, $userType);

		return $user;
	}

	private function changeUserType(MockUser $user, $userType){
		// invoke the admin user to do these actions
		$admin = new User($this->dbh, 1);
		$admin->changeUserType($user, $userType);
		return new MockUser($this->dbh, $user->getID());
	}

	// public methods not tested:
	// - getID (so simple it can't break, no need to test)
	// - isRequesting (also so simple it can't break)
	// - destroy (being used in cleanup is enough of a test in my opinion)
	// - login (requires sessions)
	// - logout (requires sessions)
	// - loggedIn (requires sessions, kind of)
}
