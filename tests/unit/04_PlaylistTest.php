<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';
require_once '../html/classes/Video.php';
require_once '../html/classes/Playlist.php';

class MockPlaylist extends Playlist {
	// mock video that lets us set the user manually, the video itself
	// is deleted by cascading deletion of the authoring (mock) user
	function __construct(PDO $dbh, int $pid = -1, int $uid = -1){
		parent::__construct($dbh, $pid);
		$this->user = new User($dbh, $uid);
	}

	public function setMockUser(User $user){
		$this->user = $user;
	}
}

class PlaylistTest extends \Codeception\Test\Unit {
	use Codeception\AssertThrows;

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	private $dbh;
	private $playlist;
	private $users;
	private $rnd_token;

	protected function _before(){
		$this->dbh = DB::getConnection();
		$this->playlist = new MockPlaylist($this->dbh);

		$this->users = array();

		$this->rnd_token = substr(md5(mt_rand()), 0, 50);
	}

	protected function _after(){
		// cleanup
		foreach($this->users as $user){
			$user->destroy();
		}
	}

	// tests
	public function testCreate(){
		// the thumbnail upload is done outside of the class as part of the create API

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can create playlists', function(){
			$this->playlist->create();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can create playlists', function(){
			$this->playlist->create();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->title = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->title = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->title = $this->rnd_token;
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->description = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->description = str_repeat('x', 501);
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->playlist->create();
		});

		$this->playlist->description = $this->rnd_token;
		$id = $this->playlist->create();
		$this->assertGreaterThan(0, $id, 'Playlist was not created correctly');
	}

	public function testUpdate(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);
		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->update();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->update();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only modify their own playlists', function(){
			$this->playlist->update();
		});

		$this->playlist->setMockUser($author);
		$this->playlist->title = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->playlist->update();
		});

		$this->playlist->title = str_repeat('x', 51);
		$this->assertThrowsWithMessage(Exception::class, 'Title is either empty or out of bounds', function(){
			$this->playlist->update();
		});

		$this->playlist->title = $this->rnd_token;
		$this->playlist->description = 'x';
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->playlist->update();
		});

		$this->playlist->description = str_repeat('x', 501);
		$this->assertThrowsWithMessage(Exception::class, 'Description is either empty or out of bounds', function(){
			$this->playlist->update();
		});

		$this->playlist->description = $this->rnd_token;
		$this->playlist->update();
	}

	public function testSearch(){
		$result = $this->playlist->search($this->rnd_token);
		$this->assertEmpty($result, 'Search should not result anything with this query');

		$playlist = $this->createPlaylist(); // create a mock playlist, now we ought to get one hit
		$result = $this->playlist->search($this->rnd_token);

		$this->assertNotEmpty($result, 'Search should not result anything with this query');
		$this->assertCount(1, $result, 'Search results didn\'t contain as expected');
		$this->assertEquals($playlist->getID(), $result[0]->getID(), 'Search results didn\'t contain the playlist');
	}

	public function testGetAuthor(){
		$this->assertThrowsWithMessage(Exception::class, 'Playlist doesn\'t exist', function(){
			$this->playlist->getAuthor();
		});

		$this->playlist = $this->createPlaylist();

		$author = $this->playlist->getAuthor();
		$this->assertEquals($author, $this->users[0], 'Author doesn\'t exist');
	}

	public function testGetAllFromUser(){
		$user = $this->createUser(UserType::Teacher);
		$this->playlist->setMockUser($user);

		$result = $this->playlist->getAllFromUser();
		$this->assertCount(0, $result, 'User shouldn\'t have any videos');

		$this->createPlaylist($user);
		$this->createPlaylist($user);
		$this->createPlaylist($user);

		$result = $this->playlist->getAllFromUser();
		$this->assertCount(3, $result, 'User has incorrect amount of videos');
	}

	public function testGetAllFromUserWithUserArgument(){
		$user = $this->createUser(UserType::Teacher);

		$result = $this->playlist->getAllFromUser($user);
		$this->assertCount(0, $result, 'User shouldn\'t have any videos');

		$this->createPlaylist($user);
		$this->createPlaylist($user);
		$this->createPlaylist($user);

		$result = $this->playlist->getAllFromUser($user);
		$this->assertCount(3, $result, 'User has incorrect amount of videos');
	}

	public function testAddVideo(){
		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->addVideo(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->addVideo(0);
		});

		$author = $this->createUser(UserType::Teacher);
		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Playlist doesn\'t exist', function(){
			$this->playlist->addVideo(0);
		});

		$this->playlist = $this->createPlaylist($author);
		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only modify their own playlists', function(){
			$this->playlist->addVideo(0);
		});

		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Video doesn\'t exist', function(){
			$this->playlist->addVideo(0);
		});

		$this->video = $this->createVideo();
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only add videos they\'ve uploaded', function(){
			$this->playlist->addVideo($this->video->getID());
		});

		$this->video = $this->createVideo($author);
		$this->playlist->addVideo($this->video->getID());

		$this->assertCount(1, $this->playlist->getVideos(), 'Failed to add video to playlist');
	}

	public function testRemoveVideo(){
		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->removeVideo(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->removeVideo(0);
		});

		$author = $this->createUser(UserType::Teacher);
		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Playlist doesn\'t exist', function(){
			$this->playlist->removeVideo(0);
		});

		$this->playlist = $this->createPlaylist($author);
		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only modify their own playlists', function(){
			$this->playlist->removeVideo(0);
		});

		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Video is not in playlist', function(){
			$this->playlist->removeVideo(0);
		});

		$this->video = $this->createVideo($author);
		$this->playlist->addVideo($this->video->getID());
		$this->assertCount(1, $this->playlist->getVideos(), 'Failed to add video to playlist');

		$this->playlist->removeVideo($this->video->getID());
		$this->assertCount(0, $this->playlist->getVideos(), 'Failed to remove video from playlist');
	}

	public function testGetVideos(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);
		$this->assertCount(0, $this->playlist->getVideos(), 'There shouldn\'t be any videos');

		$video = $this->createVideo($author);
		$this->playlist->addVideo($video->getID());
		$this->assertCount(1, $this->playlist->getVideos(), 'There shouldn\'t be any videos');

		$this->playlist->removeVideo($video->getID());
		$this->assertCount(0, $this->playlist->getVideos(), 'There shouldn\'t be any videos');
	}

	public function testHasVideo(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);

		$this->assertFalse($this->playlist->hasVideo(0), 'Playlist shouldn\'t have this (non-existing) video');

		$video = $this->createVideo($author);
		$this->playlist->addVideo($video->getID());
		$this->assertTrue($this->playlist->hasVideo($video->getID()), 'Playlist should have this video');
	}

	public function testVideoHasPlaylist(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);

		$video = $this->createVideo($author);
		$this->assertFalse($this->playlist->videoHasPlaylist($video->getID()), 'Video shouldn\'t have a playlist');

		$this->playlist->addVideo($video->getID());
		$this->assertTrue($this->playlist->videoHasPlaylist($video->getID()), 'Video should have a playlist');
	}

	public function testGetVideoPlaylist(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);

		$this->video = $this->createVideo($author);
		$this->assertThrowsWithMessage(Exception::class, 'Video is not in a playlist', function(){
			$this->playlist->getVideoPlaylist($this->video->getID());
		});

		$this->playlist->addVideo($this->video->getID());
		$this->assertEquals(
			$this->playlist->getID(),
			$this->playlist->getVideoPlaylist($this->video->getID())->getID(),
			'Video should be in this playlist');
	}

	public function testMoveVideoUp(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->moveVideoUp(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->moveVideoUp(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only modify their own playlists', function(){
			$this->playlist->moveVideoUp(0);
		});

		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Array is empty', function(){
			$this->playlist->moveVideoUp(0);
		});

		$this->video1 = $this->createVideo($author);
		$this->video2 = $this->createVideo($author);
		$this->video3 = $this->createVideo($author);

		$this->playlist->addVideo($this->video1->getID());
		$this->playlist->addVideo($this->video2->getID());
		$this->playlist->addVideo($this->video3->getID());

		$videos = $this->playlist->getVideos();
		$this->assertCount(3, $videos, 'Unexpected number of videos');

		// videos gets added to the top of the playlist
		$this->assertEquals($this->video1->getID(), $videos[2]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video2->getID(), $videos[1]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video3->getID(), $videos[0]->getID(), 'Unexpected video found in array position');

		$this->assertThrowsWithMessage(Exception::class, 'Key doesn\'t exist in array', function(){
			$this->playlist->moveVideoUp(0);
		});

		$this->assertThrowsWithMessage(Exception::class, 'Key is already the first index', function(){
			$this->playlist->moveVideoUp($this->video3->getID());
		});

		$this->playlist->moveVideoUp($this->video2->getID());

		$videos = $this->playlist->getVideos();
		$this->assertCount(3, $videos, 'Unexpected number of videos');

		$this->assertEquals($this->video1->getID(), $videos[2]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video3->getID(), $videos[1]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video2->getID(), $videos[0]->getID(), 'Unexpected video found in array position');
	}

	public function testMoveVideoDown(){
		$author = $this->createUser(UserType::Teacher);
		$this->playlist = $this->createPlaylist($author);

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->moveVideoDown(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only teachers can modify playlists', function(){
			$this->playlist->moveVideoDown(0);
		});

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Teachers can only modify their own playlists', function(){
			$this->playlist->moveVideoDown(0);
		});

		$this->playlist->setMockUser($author);
		$this->assertThrowsWithMessage(Exception::class, 'Array is empty', function(){
			$this->playlist->moveVideoDown(0);
		});

		$this->video1 = $this->createVideo($author);
		$this->video2 = $this->createVideo($author);
		$this->video3 = $this->createVideo($author);

		$this->playlist->addVideo($this->video1->getID());
		$this->playlist->addVideo($this->video2->getID());
		$this->playlist->addVideo($this->video3->getID());

		$videos = $this->playlist->getVideos();
		$this->assertCount(3, $videos, 'Unexpected number of videos');

		// videos gets added to the top of the playlist
		$this->assertEquals($this->video1->getID(), $videos[2]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video2->getID(), $videos[1]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video3->getID(), $videos[0]->getID(), 'Unexpected video found in array position');

		$this->assertThrowsWithMessage(Exception::class, 'Key doesn\'t exist in array', function(){
			$this->playlist->moveVideoDown(0);
		});

		$this->assertThrowsWithMessage(Exception::class, 'Key is already the last index', function(){
			$this->playlist->moveVideoDown($this->video1->getID());
		});

		$this->playlist->moveVideoDown($this->video2->getID());

		$videos = $this->playlist->getVideos();
		$this->assertCount(3, $videos, 'Unexpected number of videos');

		$this->assertEquals($this->video2->getID(), $videos[2]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video1->getID(), $videos[1]->getID(), 'Unexpected video found in array position');
		$this->assertEquals($this->video3->getID(), $videos[0]->getID(), 'Unexpected video found in array position');
	}

	public function testSubscribe(){
		$this->playlist = $this->createPlaylist();

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can subscribe to playlists', function(){
			$this->playlist->subscribe();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can subscribe to playlists', function(){
			$this->playlist->subscribe();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->playlist->subscribe();

		$subs = $this->playlist->getSubscriptions();
		$this->assertEquals($this->playlist->getID(), $subs[0]->getID(), 'Playlist not found in subscriptions');
	}

	public function testUnsubscribe(){
		$this->playlist = $this->createPlaylist();

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can unsubscribe from playlists', function(){
			$this->playlist->unsubscribe();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can unsubscribe from playlists', function(){
			$this->playlist->unsubscribe();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertThrowsWithMessage(Exception::class, 'User isn\'t subscribed to this playlist', function(){
			$this->playlist->unsubscribe();
		});

		$this->playlist->subscribe();
		$subs = $this->playlist->getSubscriptions();
		$this->assertEquals($this->playlist->getID(), $subs[0]->getID(), 'Playlist not found in subscriptions');

		$this->playlist->unsubscribe();
		$this->assertEmpty($this->playlist->getSubscriptions(), 'Playlist found in subscriptions when it shouldn\t be');
	}

	public function testIsSubscribed(){
		$this->playlist = $this->createPlaylist();

		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can be subscribed to playlists', function(){
			$this->playlist->isSubscribed();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students can be subscribed to playlists', function(){
			$this->playlist->isSubscribed();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Student));
		$this->assertFalse($this->playlist->isSubscribed(), 'User shouldn\t be subscribed to playlist');

		$this->playlist->subscribe();
		$this->assertTrue($this->playlist->isSubscribed(), 'User should be subscribed to playlist');
	}

	public function testGetSubscriptions(){
		$this->playlist->setMockUser($this->createUser(UserType::Teacher));
		$this->assertThrowsWithMessage(Exception::class, 'Only students have subscriptions', function(){
			$this->playlist->getSubscriptions();
		});

		$this->playlist->setMockUser($this->createUser(UserType::Admin));
		$this->assertThrowsWithMessage(Exception::class, 'Only students have subscriptions', function(){
			$this->playlist->getSubscriptions();
		});

		$student = $this->createUser(UserType::Student);
		$this->playlist->setMockUser($student);
		$this->assertEmpty($this->playlist->getSubscriptions(), 'Subscriptions was not empty');

		$author = $this->createUser(UserType::Teacher);
		$playlist1 = $this->createPlaylist($author);
		$playlist2 = $this->createPlaylist($author);
		$playlist3 = $this->createPlaylist($author);

		$playlist1->setMockUser($student);
		$playlist1->subscribe();
		$playlist2->setMockUser($student);
		$playlist2->subscribe();
		$playlist3->setMockUser($student);
		$playlist3->subscribe();

		$subs = $this->playlist->getSubscriptions();
		$this->assertCount(3, $subs, 'Incorrect amount of subscriptions');

		// subscriptions are sorted by newest first, so the last one created (thus updated) is first
		$this->assertEquals($playlist3->getID(), $subs[0]->getID(), 'Unexpected playlist found in subscription position');
		$this->assertEquals($playlist2->getID(), $subs[1]->getID(), 'Unexpected playlist found in subscription position');
		$this->assertEquals($playlist1->getID(), $subs[2]->getID(), 'Unexpected playlist found in subscription position');

		$playlist2->unsubscribe();
		$subs = $this->playlist->getSubscriptions();
		$this->assertCount(2, $subs, 'Incorrect amount of subscriptions');

		// subscriptions are sorted by newest first, so the last one created (thus updated) is first
		$this->assertEquals($playlist3->getID(), $subs[0]->getID(), 'Unexpected playlist found in subscription position');
		$this->assertEquals($playlist1->getID(), $subs[1]->getID(), 'Unexpected playlist found in subscription position');

		$playlist1->setMockUser($author);
		$playlist1->addVideo($this->createVideo($author)->getID());

		// the first playlist was updated, it should be the most recently updated
		$subs = $this->playlist->getSubscriptions();
		$this->assertCount(2, $subs, 'Incorrect amount of subscriptions');

		$this->assertEquals($playlist1->getID(), $subs[0]->getID(), 'Unexpected playlist found in subscription position');
		$this->assertEquals($playlist3->getID(), $subs[1]->getID(), 'Unexpected playlist found in subscription position');
	}

	// utility
	private function createUser($userType = UserType::Student){
		$user = new User($this->dbh);
		$user->email = substr(md5(mt_rand()), 0, 30) . '@example.com';
		$user->password = $this->rnd_token;
		$user->givenName = $this->rnd_token;
		$user->familyName = $this->rnd_token;
		$user->register();

		if($userType != UserType::Student)
			$user = $this->changeUserType($user, $userType);

		// make sure we clean up afterwards
		array_push($this->users, $user);

		return $user;
	}

	private function changeUserType(User $user, $userType){
		// invoke the admin user to do these actions
		$admin = new User($this->dbh, 1);
		$admin->changeUserType($user, $userType);
		return new User($this->dbh, $user->getID());
	}

	private function createVideo(User $author = null){
		if(!isset($author))
			$author = $this->createUser(UserType::Teacher);

		$video = new MockVideo($this->dbh, -1, $author->getID());
		$video->title = $this->rnd_token;
		$video->description = $this->rnd_token;
		$video->course = $this->rnd_token;
		$video->subject = $this->rnd_token;
		$video->upload();

		return $video;
	}

	private function createPlaylist(User $author = null, Video $video = null){
		if(!isset($author))
			$author = $this->createUser(UserType::Teacher);

		if(!isset($video))
			$video = $this->createVideo();

		$playlist = new MockPlaylist($this->dbh, -1, $author->getID());
		$playlist->title = $this->rnd_token;
		$playlist->description = $this->rnd_token;
		$playlist->create();

		return $playlist;
	}

	// public methods not tested:
	// - getID (so simple it can't break, no need to test)
	// - destroy (being used in cleanup is enough of a test in my opinion)
}
