// this file contains the initator for the Redux store, using helpers from
// pwa-helpers to aid in the setup (lit-redux-router requires this).

import {createStore, compose, combineReducers} from 'redux'
import {lazyReducerEnhancer} from 'pwa-helpers'

// import reducers
import {userReducer, errorReducer} from './reducers.js'

// create the store
const store = createStore(
	(state, action) => state,
	compose(lazyReducerEnhancer(combineReducers))
)

// add my reducers
store.addReducers({user: userReducer})
store.addReducers({error: errorReducer})

// export it
export default store
