// this file contains the reducers for Redux, one for session/user handling, and one
// for error page variables.

import {LOGIN, LOGOUT, STORE_ERROR, CLEAR_ERROR} from './actions.js'

const USER_INITIAL_STATE = {
	uid: 0,
	name: null,
	type: null,
	isStudent: false,
	isTeacher: false,
	isAdmin: false,
}

export const userReducer = (state = USER_INITIAL_STATE, action) => {
	switch(action.type){
		case LOGIN:
			// update the state
			return {
				...state,
				...action.details,
			}
		case LOGOUT:
			// reset the state
			return {
				...state,
				...USER_INITIAL_STATE
			}
		default:
			// use the existing state
			return state
	}
}

const ERROR_INITIAL_STATE = {
	title: null,
	msg: null,
}

export const errorReducer = (state = ERROR_INITIAL_STATE, action) => {
	switch(action.type){
		case STORE_ERROR:
			return {
				...state,
				...action.details,
			}
		case CLEAR_ERROR:
			return {
				...state,
				...ERROR_INITIAL_STATE,
			}
		default:
			return state
	}
}
