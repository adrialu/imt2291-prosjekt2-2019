// this file contains actions that can be performed to the Redux store, and their
// constant names to go along with them.
// actions include login/logout handling, and store/clear errors for error pages.

export const LOGIN = 'login'
export const LOGOUT = 'logout'

export const login = user => {
	return {
		type: LOGIN,
		details: user,
	}
}

export const logout = _ => {
	return {
		type: LOGOUT,
	}
}

export const STORE_ERROR = 'store-error'
export const CLEAR_ERROR = 'clear-error'

export const error = error => {
	return {
		type: STORE_ERROR,
		details: error
	}
}

export const clear = _ => {
	return {
		type: CLEAR_ERROR,
	}
}
