import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-lumo-styles'

import './../components/shared-style.js'

/**
 * Class that implements the home page for guests.
 * The page displays a welcome message with a link to the login form.
 */
class ViewHomeGuest extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-home-guest'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				max-width: 800px;
				text-align: center;
			}

			h4 {
				margin-top: 20px;
			}
		`
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<h4>Welcome to the lecture site!</h4>
			<p>Feel free to search and browse the site as a guest, or
			<a href='/login'>log in</a> to access more advanced features.</p>
		`
	}
}

// define the class as a component
customElements.define(ViewHomeGuest.is, ViewHomeGuest)
