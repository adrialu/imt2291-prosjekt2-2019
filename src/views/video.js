import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-icons'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-icon'

import {get, post, serverURL} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {error} from './../redux/actions.js'

import './../components/shared-style.js'
import './../components/card-box.js'
import './../components/lecture-video.js'
import './../components/lecture-stars.js'

/**
 * Class that implements a page that displays the video and its associated user-submitted comments.
 * The video display features custom controls and captions in a clickable fashion on the side.
 * Students visiting the page will also be able to rate the video in 1-5 stars.
 */
class ViewVideo extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-video'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			vid: Number, // router param
			video: Object,
			user: Object,
			comments: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				width: 100%;
				display: grid;
				grid-template-columns: 1fr 1000px 1fr;
				grid-template-rows: 600px 1fr 105px;
			}

			lecture-video {
				width: 100%;
				justify-content: center;
				background: black;
				grid-column-start: 1;
				grid-column-end: 4;

				/* hacky shit */
				position: absolute;
				left: 0;
				right: 0;
				top: 60px;
				height: 600px;
			}

			lecture-stars {
				position: absolute;
				top: 0;
				right: 0;
			}

			#info,
			#comment,
			#comments {
				grid-column-start: 2;
			}

			#info {
				/* hacky shit */
				margin-top: 600px;
			}

			#info div[slot=header] {
				display: flex;
			}

			#info div[slot=header] p {
				margin: 0
			}

			#info div[slot=header] p:first-of-type {
				margin-right: 30px;
			}

			.uploaded iron-icon,
			.views iron-icon {
				color: var(--lumo-primary-color);
				width: 18px;
				height: 18px;
				margin-bottom: 4px;
			}

			#edit {
				position: absolute;
				top: 0;
				right: 0;
			}

			#comment {
				position: relative;
				height: 80px;

				/* hacky shit */
				margin-top: 350px;
			}

			#comment form {
				margin: 10px;
				padding-right: 60px;
			}

			#comment textarea {
				resize: none;
				border: none;
				width: 100%;
			}

			#comment textarea:required {
				box-shadow: none;
			}

			#comment vaadin-button {
				position: absolute;
				bottom: 8px;
				right: 12px;
			}

			#comments {
				margin-bottom: 20px;

				/* hacky shit */
				margin-top: 26px;
			}

			@media only screen and (max-width: 1400px){
				lecture-video {
					height: 400px;
				}

				#info {
					margin-top: 400px;
				}

				#comment {
					margin-top: 150px;
				}
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// load user from storage
		const state = store.getState()
		this.user = state.user
		this.user.isAuthor = false
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// fetch video metadata
		get('api/video/get.php?vid=' + this.vid).then(video => {
			// update author flag
			this.user.isAuthor = video.author.uid == this.user.uid

			// update video info
			this.video = video

			// fetch comments
			get('api/comment/video.php?vid=' + this.vid).then(comments => {
				// update comments array
				this.comments = comments
			}).catch(err => {
				notify('Failed to load comments', err)
			})

			// update the view count for the video
			const data = new FormData()
			data.append('vid', this.vid)
			post('api/video/views.php', data)
		}).catch(err => {
			// store error message
			store.dispatch(error({
				title: 'Failed to get video',
				msg: err
			}))

			// redirect to error page
			navigate('/error')
		})
	}

	/**
	 * Handles uploading of new comments and locking/disabling of the textarea input.
	 */
	_postComment(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('#comment iron-form')
		if(!form.validate())
			return
		const serialized = form.serializeForm()

		// fill a FormData object with the serialized data
		const data = new FormData()
		data.append('vid', this.video.vid)
		data.append('comment', serialized.comment)

		// lock the input field
		const textarea = form.querySelector('textarea')
		textarea.disabled = true

		post('api/comment/create.php', data).then(comment => {
			// update comments array
			this.comments = [comment, ...this.comments]

			// clear the field and unlock it
			textarea.value = ''
			textarea.disabled = false

			// TODO: turn button green for X seconds
		}).catch(err => {
			notify('Failed to post comment', err)
			// TODO: turn button red for X seconds
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<lecture-video
				video='${serverURL}api/video/stream.php?vid=${this.vid}'
				caption='${serverURL}api/video/caption.php?vid=${this.vid}'
				thumbnail='${serverURL}api/video/thumb.php?vid=${this.vid}'
			>
			</lecture-video>

			<card-box id='info'>
				${this._renderVideoInfo()}
			</card-box>

			${this._renderPostComment()}
			${this._renderComments()}
		`
	}

	/**
	 * Renderer for video information, only loaded if there is information available.
	 */
	_renderVideoInfo(){
		if(this.video)
			return html`
				<h4 slot='header' class='title'>${this.video.title}</h4>
				<div slot='header'>
					<p class='views' title='Views'>
						<iron-icon icon='vaadin:eye'></iron-icon>
						${this.video.views + 1}
					</p>
					<p class='uploaded' title='${this.video.uploaded.date}'>
						<iron-icon icon='vaadin:calendar'></iron-icon>
						${formatTime(this.video.uploaded.date)}
					</p>
				</div>
				<p class='author'>Author: <a href='/user/${this.video.author.uid}'>${this.video.author.name}</a></p>
				<p class='subject'>Subject: ${this.video.subject}</p>
				<p class='course'>Course: ${this.video.course}</p>
				<p class='rating'>Average rating: ${this.video.rating > 0 ? this.video.rating + '/5' : 'Unknown'}</p>
				<br>
				<p class='description'>${this.video.description}</p>

				${this._renderEditButton()}
				${this._renderRatings()}
			`
	}

	/**
	 * Renderer for the video edit button, only loaded if the user is the authoring teacher of the
	 * video.
	 */
	_renderEditButton(){
		if(this.user.isAuthor)
			return html`
				<a id='edit' href='/edit/video/${this.video.vid}'>
					<vaadin-button theme='secondary icon' title='Edit'>
						<iron-icon icon='vaadin:pencil'></iron-icon>
					</vaadin-button>
				</a>
			`
	}

	/**
	 * Renderer for the video ratings, only loaded if the user is a student.
	 */
	_renderRatings(){
		if(this.user.isStudent)
			return html`
				<lecture-stars .video='${this.video.vid}'></lecture-stars>
			`
	}

	/**
	 * Renderer for the comments parent, only loaded if there are comments available.
	 */
	_renderComments(){
		if(this.comments)
			return html`
				<div id='comments'>
					${this.comments.map(comment => {
						return this._renderComment(comment)
					})}
				</div>
			`
	}

	/**
	 * Renderer for the comment submit form.
	 * Only rendered for logged-in users.
	 */
	_renderPostComment(){
		if(this.user.uid)
			return html`
				<card-box id='comment' no-header no-margin no-radius-bottom>
					<iron-form>
						<form>
							<textarea name='comment' rows='4' cols='50' minlength='10' maxlength='500' placeholder='Comment...' required></textarea>
							<vaadin-button theme='secondary icon' title='Submit' @click='${this._postComment}'>
								<iron-icon icon='vaadin:paperplane'></iron-icon>
							</vaadin-button>
						</form>
					</iron-form>
				</card-box>
			`
	}

	/**
	 * Renderer for each individual comment, only loaded on demand.
	 */
	_renderComment(data){
		return html`
			<card-box class='comment' no-radius no-margin-outside>
				<span slot='header'><a href='/user/${data.user.uid}'>${data.user.name}</a> commented:</span>
				<p slot='header' class='uploaded' title='${data.created.date}'>
					<iron-icon icon='vaadin:calendar'></iron-icon>
					${formatTime(data.created.date)}
				</p>
				<p>${data.comment}</p>
			</card-box>
		`
	}
}

// define the class as a component
customElements.define(ViewVideo.is, ViewVideo)
