import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js'
import '@vaadin/vaadin-form-layout/vaadin-form-item.js'
import '@vaadin/vaadin-text-field/vaadin-email-field.js'
import '@vaadin/vaadin-text-field/vaadin-password-field.js'
import '@vaadin/vaadin-button/vaadin-button.js'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-form/iron-form.js'

import {post} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {login} from './../redux/actions.js'
import './../components/shared-style.js'

/**
 * Class that implements a login page, containing a form to submit credentials.
 * It also has a button to redirect to the registration form page.
 */
class ViewLogin extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-login'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				justify-content: center;
			}

			vaadin-form-layout {
				max-width: 300px;
			}

			vaadin-form-item {
				width: calc(50%) !important;
			}

			vaadin-form-item:last-of-type {
				align-items: flex-end;
			}

			h2 {
				margin-top: 20px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		super()

		// bail out if the user is already logged in (the state knows the user ID)
		const state = store.getState()
		if(state.user.uid)
			navigate('/')
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// listen to the Enter key in the form elements to attempt a submit
		this.shadowRoot.querySelectorAll('iron-form *[required]').forEach(el => {
			el.addEventListener('keyup', e => {
				if(e.keyCode == 13)
					this._login()
			})
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<iron-form>
				<form>
					<vaadin-form-layout>
						<h2>Login</h2>

						<vaadin-email-field label='Email' name='email' minlength='5' maxlength='80' error-message='Must be a valid email address between 5 and 80 characters.' required></vaadin-email-field>
						<vaadin-password-field label='Password' name='password' minlength='5' maxlength='72' error-message='Must be between 5 and 72 characters.' required></vaadin-password-field>
						<vaadin-form-item colspan='1'>
							<vaadin-button theme='primary' @click='${this._login}'>Submit</vaadin-button>
						</vaadin-form-item>
						<vaadin-form-item colspan='1'>
							<vaadin-button theme='secondary' @click='${this._register}'>Register</vaadin-button>
						</vaadin-form-item>
					</vaadin-form-layout>
				</form>
			</iron-form>
		`
	}

	/**
	 * Callback handler for the form's submit click event and inputs' Enter presses.
	 * Handles authentication with the server using the form data from the element.
	 */
	_login(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('iron-form')
		if(!form.validate())
			return
		const serialized = form.serializeForm()

		// fill a FormData object with the serialized data
		const data = new FormData()
		data.append('email', serialized.email)
		data.append('password', serialized.password)

		// lock form
		form.querySelectorAll('vaadin-email-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-password-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-button').forEach(el => el.disabled = true)

		// attempt to query the server for authentication using the FormData
		post('api/user/login.php', data).then(user => {
			// dispatch the changes to the store
			store.dispatch(login({
				uid: user.uid,
				name: user.name,
				type: user.type,
				isStudent: user.type == 'student',
				isTeacher: user.type == 'teacher',
				isAdmin: user.type == 'admin',
			}))

			// redirect to home page
			navigate('/')
		}).catch(err => {
			// show error
			notify('Failed to log in', err)

			// unlock form
			form.querySelectorAll('vaadin-email-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-password-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-button').forEach(el => el.disabled = false)
		})
	}

	/**
	 * Redirects to the registration view.
	 */
	_register(){
		navigate('/register')
	}
}

// define the class as a component
customElements.define(ViewLogin.is, ViewLogin)
