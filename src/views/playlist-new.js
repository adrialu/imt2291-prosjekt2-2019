import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-form-layout'
import '@vaadin/vaadin-text-field'
import '@vaadin/vaadin-text-field/vaadin-text-area'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-item'
import '@vaadin/vaadin-progress-bar'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-form'

import {upload} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import './../components/shared-style.js'

/**
 * Class that implements a page to create a new playlist.
 * This is presented as a form, with the option to change every attribute about the playlist.
 */
class ViewPlaylistNew extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-playlist-new'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				justify-content: center;
			}

			vaadin-form-layout {
				max-width: 500px;
			}

			vaadin-form-item {
				margin-top: 20px;
				display: unset;
			}

			vaadin-form-item vaadin-button {
				width: 40%;
			}

			vaadin-form-item vaadin-button:first-of-type {
				margin-right: 3.2%;
			}

			vaadin-text-field input[type=file] {
				padding-top: 10px;
				height: 10px;
			}

			@-moz-document url-prefix() {
				vaadin-text-field input[type=file] {
					height: 26px;
				}
			}

			h2 {
				margin-top: 20px;
			}

			#progress-parent {
				width: 100% !important;
				margin-left: 0 !important;
			}

			[hidden] {
				display: none;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// bail out if the user is already logged in (the state knows the user ID), or the user
		// is not a teacher
		const state = store.getState()
		if(!state.user.uid || !state.user.isTeacher)
			navigate('/')
	}

	/**
	 * Called when the component is added to DOM.
	 */
	firstUpdated(){
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<iron-form>
				<form>
					<vaadin-form-layout>
						<h2>Create playlist</h2>

						<vaadin-text-field label='Title' name='title' minlength='10' maxlength='50' error-message='Must be between 10 and 50 characters.' required></vaadin-text-field>
						<vaadin-text-area label='Description' name='description' minlength='10' maxlength='500' error-message='Must be between 10 and 500 characters.' required></vaadin-text-area>
						<vaadin-text-field label='Thumbnail' name='thumbnail' error-message='Max size 10 MB.' required>
							<input slot='input' type='file' accept='image/*'>
						</vaadin-text-field>

						<vaadin-form-item id='progress-parent' hidden>
							<vaadin-progress-bar id='progress'></vaadin-progress-bar>
							Uploading: <span id='progress-value'>0</span> %
						</vaadin-form-item>

						<vaadin-form-item>
							<vaadin-button theme='primary' @click='${this._create}'>Create</vaadin-button>
						</vaadin-form-item>
					</vaadin-form-layout>
				</form>
			</iron-form>
		`
	}

	/**
	 * Click event for the 'create' button.
	 * Handles form validation and file uploading to the server.
	 * On a successful upload it redirects to the playlist page.
	 */
	_create(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('iron-form')
		if(!form.validate())
			return
		const serialized = form.serializeForm()

		// get file upload form parts
		const thumb = this.shadowRoot.querySelector('[name=thumbnail] input[type=file]')

		// fill a FormData object with the serialized text data
		const data = new FormData()
		data.append('title', serialized.title)
		data.append('description', serialized.description)

		// validate and fill FormData with binary files
		if(thumb.files[0].size > 1e7){ // 10 MB
			notify('Thumbnail size too big')
			return
		} else
			data.append('thumbnail', thumb.files[0])

		// lock all fields before we continue
		form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-text-area').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-button').forEach(el => el.disabled = true)
		form.querySelectorAll('vaadin-text-field input').forEach(el => el.disabled = true)

		// show and reset progress bar
		const progress = this.shadowRoot.getElementById('progress')
		const progressValue = this.shadowRoot.getElementById('progress-value')
		const progressParent = this.shadowRoot.getElementById('progress-parent')
		progressParent.hidden = false
		progress.value = progressValue.innerText = 0

		// attempt to upload to server
		upload('api/playlist/create.php', data).progress(e => {
			// display progress as both text and a progress bar
			if(e.lengthComputable){
				const perc = (e.loaded / e.total) * 100
				progress.value = perc / 100
				progressValue.innerText = ~~perc
			}
		}).then(playlist => {
			navigate(`/playlist/${playlist.pid}`)
		}).catch(err => {
			notify('Failed to create playlist', err)

			// unlock all fields
			form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-text-area').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-button').forEach(el => el.disabled = false)
			form.querySelectorAll('vaadin-text-field input').forEach(el => el.disabled = false)
		})
	}
}

// define the class as a component
customElements.define(ViewPlaylistNew.is, ViewPlaylistNew)
