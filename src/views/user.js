import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-tabs'
import '@vaadin/vaadin-grid'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-pages'

import {get, serverURL} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {error} from './../redux/actions.js'
import './../components/shared-style.js'
import './../components/card-box.js'

/**
 * Class that implements a page for user information.
 * This page also features a tabbed box with the content the user has contributed to the site,
 * such as videos, playlist and/or comments.
 */
class ViewUser extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-user'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			uid: Number, // router param
			page: Number,
			isAdmin: Boolean,
			user: Object,
			videos: Array,
			playlists: Array,
			comments: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			.uploaded iron-icon {
				color: var(--lumo-primary-color);
				width: 18px;
				height: 18px;
				margin-bottom: 4px;
			}

			vaadin-tabs {
				box-shadow: none;
				margin-bottom: -1px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.videos = []
		this.playlists = []
		this.comments = []

		// load state from storage
		const state = store.getState()
		this.isAdmin = state.user && state.user.isAdmin
		this.isAdmin = true
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// get user info
		get('api/user/get.php?uid=' + this.uid).then(user => {
			this.user = user

			// get user content
			this.getUserContent()
		}).catch(err => {
			// store error message
			store.dispatch(error({
				title: 'Failed to get user',
				msg: err,
			}))

			// redirect to error page
			navigate('/error')
		})
	}

	/**
	 * Get the user submitted content from the server.
	 */
	getUserContent(){
		// get videos
		get('api/video/all.php?uid=' + this.uid).then(videos => {
			for(let i in videos){
				// convert all the dates before we copy over the array
				videos[i].uploaded = formatTime(videos[i].uploaded)
			}

			this.videos = videos
		}).catch(err => {
			notify('Failed to get user videos', err)
		})

		// get playlists
		get('api/playlist/all.php?uid=' + this.uid).then(playlists => {
			for(let i in playlists){
				// convert all the dates before we copy over the array
				playlists[i].updated = formatTime(playlists[i].updated)
			}

			this.playlists = playlists
		}).catch(err => {
			notify('Failed to get user playlists', err)
		})

		// get comments
		get('api/comment/user.php?uid=' + this.uid).then(comments => {
			this.comments = comments
		}).catch(err => {
			notify('Failed to get user comments', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<card-box id='info'>
				${this._renderUserInfo()}
			</card-box>

			${this._renderUserContent()}
		`
	}

	/**
	 * Renderer for user information, only loaded if there is information available.
	 */
	_renderUserInfo(){
		if(this.user)
			return html`
				<h4 slot='header'>${this.user.name}</h4>
				<p class='admin'>Email: <a href='mailto:${this.user.email}'>${this.user.email}</a></p>
			`
	}

	/**
	 * Renderer for user-submitted content, only loaded if there are some.
	 */
	_renderUserContent(){
		if(this.videos.length > 0 || this.playlists.length > 0 || this.comments.length > 0){
			// create an array with each type of content in it.
			// this is used to control the page index in the tabbed box.
			const tabs = []
			if(this.videos && this.videos.length > 0)
				tabs.push('videos')
			if(this.playlists && this.playlists.length > 0)
				tabs.push('playlists')
			if(this.comments && this.comments.length > 0)
				tabs.push('comments')

			return html`
				<card-box no-padding-header no-margin>
					<vaadin-tabs slot='header' selected='${this.page || 0}'>
						${this._renderTab(tabs, 'Videos')}
						${this._renderTab(tabs, 'Playlists')}
						${this._renderTab(tabs, 'Comments')}
					</vaadin-tabs>
					<iron-pages selected='${this.page || 0}'>
						${this._renderVideos()}
						${this._renderPlaylists()}
						${this._renderComments()}
					</iron-pages>
				</card-box>
			`
		}
	}

	/**
	 * Renderer for user-submitted videos, only loaded if there are any.
	 */
	_renderVideos(){
		if(this.videos && this.videos.length > 0)
			return html`
				<page id='videos'>
					<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.videos)}'>
						<vaadin-grid-column width='200px' flex-grow='0'>
							<template>
								<a href='/video/[[item.vid]]'>
									<image src='${serverURL}api/video/thumb.php?vid=[[item.vid]]' width='180px' height='101px'>
								</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='title'>
							<template>
								<a href='/video/[[item.vid]]'>[[item.title]]</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='uploaded'></vaadin-grid-column>
						<vaadin-grid-column path='views'></vaadin-grid-column>
					</vaadin-grid>
				</page>
			`
	}

	/**
	 * Renderer for user-submitted playlists, only loaded if there are any.
	 */
	_renderPlaylists(){
		if(this.playlists && this.playlists.length > 0)
			return html`
				<page id='playlists'>
					<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.playlists)}'>
						<vaadin-grid-column width='200px' flex-grow='0'>
							<template>
								<a href='/playlist/[[item.pid]]'>
									<image src='${serverURL}api/playlist/thumb.php?pid=[[item.pid]]' width='180px' height='101px'>
								</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='title'>
							<template>
								<a href='/playlist/[[item.pid]]'>[[item.title]]</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='updated'></vaadin-grid-column>
						<vaadin-grid-column path='videos'></vaadin-grid-column>
					</vaadin-grid>
				</page>
			`
	}

	/**
	 * Renderer for user-submitted comments, only loaded if there are any.
	 */
	_renderComments(){
		if(this.comments && this.comments.length > 0)
			return html`
				<page id='comments'>
					${this.comments.map(comment => {
						return this._renderComment(comment)
					})}
				</page>
			`
	}

	/**
	 * Renderer for each individual comment, only loaded on demand.
	 */
	_renderComment(data){
		return html`
			<card-box class='comment' no-border no-radius no-margin-outside>
				<span slot='header'>on <a href='/video/${data.video.vid}'>${data.video.title}</a></span>
				<p slot='header' class='uploaded' title='${data.created.date}'>
					<iron-icon icon='vaadin:calendar'></iron-icon>
					${formatTime(data.created.date)}
				</p>
				<p>${data.comment}</p>
			</card-box>
		`
	}

	/**
	 * Renderer for tabs.
	 */
	_renderTab(tabsList, tabName){
		if(tabsList.indexOf(tabName.toLowerCase()) >= 0)
			return html`
				<vaadin-tab @click='${() => this._setPage(tabsList.indexOf(tabName.toLowerCase()))}'>${tabName}</vaadin-tab>
			`
	}

	/**
	 * Called when tabs are clicked, this will switch the displayed page for the tab.
	 */
	_setPage(page){
		this.page = page
	}
}

// define the class as a component
customElements.define(ViewUser.is, ViewUser)
