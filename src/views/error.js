import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-lumo-styles'

import navigate from './../utils/navigate.js'
import store from './../redux/store.js'
import {clear} from './../redux/actions.js'
import './../components/shared-style.js'

/**
 * Class that implements the error page view, displayig the error stored in Redux.
 */
class ViewError extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-error'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				margin: 40px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		super()

		// bail out if error doesn't exist
		const state = store.getState()
		if(!state.error.title){
			navigate('/')
			return
		}

		// store the error
		this.title = state.error.title
		this.msg = state.error.msg
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		if(this.title)
			return html`
				<custom-style style='display:none;'>
					<style include='lumo-typography lumo-color shared-style'>
						/* lit-html will freak out if the style tag is empty */
					</style>
				</custom-style>

				<h2>Error: ${this.title}</h2>
				<p>${this.msg}</p>

				<a href='/'>Go home</a>
			`
	}
}

// define the class as a component
customElements.define(ViewError.is, ViewError)
