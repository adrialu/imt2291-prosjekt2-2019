import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-icons'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-grid'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-icon'

import {get, post, serverURL} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {error} from './../redux/actions.js'

import './../components/shared-style.js'
import './../components/card-box.js'

/**
 * Class that implements a page for a playlist.
 * This page shows the playlist information, and a list of all videos assigned to it.
 * It also has buttons for student users to (un)subscribe to the playlist.
 * The owner of the playlist will also see buttons to re-arrange the videos in it, and edit the
 * playlist itself.
 */
class ViewPlaylist extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-playlist'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			pid: Number, // router param
			playlist: Object,
			videos: Array,
			user: Object,
			subscribed: Boolean,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			#videos > h3,
			#videos > h4 {
				align-self: center;
			}

			.updated iron-icon,
			.user iron-icon {
				width: 18px;
				height: 18px;
				margin-bottom: 4px;
			}

			#info > div {
				display: flex;
				margin-top: 20px;
			}

			#info > p {
				margin-right: 180px;
			}

			#info .user {
				margin-right: 20px;
			}

			#videos > a iron-icon,
			#subscribe iron-icon {
				margin-bottom: 2px;
			}

			#edit,
			#subscribe {
				position: absolute;
				top: 0;
				right: 0;
			}

			.sorters {
				display: flex;
				flex-direction: column;
			}

			.sorters vaadin-button {
				width: 42px;
				height: 36px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.videos = []

		// load user from storage
		const state = store.getState()
		this.user = state.user
		this.user.isAuthor = false
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// fetch playlist metadata
		get('api/playlist/get.php?pid=' + this.pid).then(playlist => {
			// update playlist info
			this.playlist = playlist

			// update author flag
			this.user.isAuthor = this.playlist.author.uid == this.user.uid

			// fetch playlist videos
			this.getVideos()

			if(this.user.isStudent){
				// update subscription status
				get('api/playlist/subscribe.php?pid=' + this.pid).then(status => {
					this.subscribed = status.subscribed
				}).catch(err => {
					notify('Failed to get subscription status', err)
				})
			}
		}).catch(err => {
			// store error message
			store.dispatch(error({
				title: 'Failed to get playlist',
				msg: err,
			}))

			// redirect to error page
			navigate('/error')
		})
	}

	/**
	 * Query the server for a list of videos in the playlist.
	 */
	getVideos(){
		get('api/playlist/videos.php?pid=' + this.pid).then(videos => {
			for(let i in videos){
				// convert all the dates before we copy over the array
				videos[i].uploaded = formatTime(videos[i].uploaded)

				// add an empty playlist table if the video isn't assigned
				if(!videos[i].playlist)
					videos[i].playlist = {}
			}

			// update videos
			this.videos = videos
		}).catch(err => {
			notify('Failed to load playlist videos', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<card-box id='info'>
				${this._renderPlaylistInfo()}
			</card-box>

			<card-box id='videos' no-margin>
				<h4 slot='header'>${this.playlist ? this.playlist.videos : '0'} videos</h4>
				${this._renderAddVideoButton()}

				<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.videos)}'>
					<vaadin-grid-column flex-grow='0' .renderer='${this._renderArrangeColumn.bind(this)}'></vaadin-grid-column>
					<vaadin-grid-column class='thumb' width='200px' flex-grow='0'>
						<template>
							<a href='/video/[[item.vid]]'>
								<image src='${serverURL}api/video/thumb.php?vid=[[item.vid]]' width='180px' height='101px'>
							</a>
						</template>
					</vaadin-grid-column>
					<vaadin-grid-column class='title' path='title' flex-grow='10'>
						<template>
							<a href='/video/[[item.vid]]'>[[item.title]]</a>
						</template>
					</vaadin-grid-column>
					<vaadin-grid-column path='uploaded'></vaadin-grid-column>
					<vaadin-grid-column path='views'></vaadin-grid-column>
				</vaadin-grid>
			</card-box>
		`
	}

	/**
	 * Renderer for playlist information, only loaded if there is information available.
	 */
	_renderPlaylistInfo(){
		if(this.playlist)
			return html`
				<h3 slot='header'>${this.playlist.title}</h3>
				<p>${this.playlist.description}</p>

				<div>
					<p class='user'>
						<iron-icon icon='vaadin:user'></iron-icon>
						<a href='/user/${this.playlist.author.uid}'>${this.playlist.author.name}</a>
					</p>
					<p class='updated' title='${this.playlist.updated}'>
						<iron-icon icon='vaadin:calendar'></iron-icon>
						${formatTime(this.playlist.updated)}
					</p>
				</div>

				${this._renderEditButton()}
				${this._renderSubscribeButton()}
			`
	}

	/**
	 * Renderer for the arrange column, will create up/down arrange buttons for each video.
	 */
	_renderArrangeColumn(root, col, data){
		// store the number of videos for later
		const numVideos = this.videos.length

		// hide the column if the user is not the author or there are less than two videos
		if(!this.user.isAuthor || numVideos < 2){
			col.setAttribute('width', '0px')
			return
		}

		// make sure it shows up at all
		col.setAttribute('width', '60px')

		// render the arranging buttons
		root.innerHTML = `
			<div class='sorters'>
				<vaadin-button class='up' theme='icon' data-vid='${data.item.vid}' title='Move up'>
					<iron-icon icon='vaadin:angle-up'></iron-icon>
				</vaadin-button>
				<vaadin-button class='down' theme='icon' data-vid='${data.item.vid}' title='Move down'>
					<iron-icon icon='vaadin:angle-down'></iron-icon>
				</vaadin-button>
			</div>
		`

		// handle click events for the buttons
		const buttons = root.firstElementChild
		buttons.querySelectorAll('vaadin-button').forEach(button => {
			const index = data.index
			if(index == 0 && button.className == 'up')
				button.setAttribute('hidden', '')
			if(index == (numVideos - 1) && button.className == 'down')
				button.setAttribute('hidden', '')

			var vids = [1, 2, 3, 4]
			button.addEventListener('click', e => {
				// lock all buttons before we do anything
				this.shadowRoot.querySelectorAll('.sorters vaadin-button').forEach(btn => btn.disabled = true)

				// prepare form data
				const formData = new FormData()
				formData.append('pid', this.pid)
				formData.append('vid', button.dataset.vid)
				formData.append('direction', button.className)

				// query the server
				post('api/playlist/arrange.php', formData).then(_ => {
					// fetch the video list again to update the list
					this.getVideos()
				}).catch(err => {
					notify('Failed to arrange video', err)
				})
			})
		})
	}

	/**
	 * Renderer for a button that just links back to the home page, only useful for teachers.
	 */
	_renderAddVideoButton(){
		if(this.user.isAuthor)
			return html`
				<a slot='header' href='/'>
					<vaadin-button>
						<iron-icon icon='vaadin:plus'></iron-icon>
						Add video
					</vaadin-button>
				</a>
			`
	}

	/**
	 * Renderer for the playlist edit button, only loaded if the user is the authoring teacher of
	 * the playlist.
	 */
	_renderEditButton(){
		if(this.user.isAuthor)
			return html`
				<a id='edit' href='/edit/playlist/${this.playlist.pid}'>
					<vaadin-button theme='secondary icon' title='Edit'>
						<iron-icon icon='vaadin:pencil'></iron-icon>
					</vaadin-button>
				</a>
			`
	}

	/**
	 * Renderer for the subscribe toggle button, only loaded if the user is a registered student.
	 */
	_renderSubscribeButton(){
		if(this.user.isStudent)
			return html`
				<vaadin-button id='subscribe' theme='primary ${this.subscribed ? "error" : ""}' @click='${this._toggleSubscribe}'>
					<iron-icon icon='vaadin:star'></iron-icon>
					${this.subscribed ? 'Unsubscribe' : 'Subscribe'}
				</vaadin-button>
			`
	}

	/**
	 * Handles toggling of the subscription status of a playlist for a student.
	 */
	_toggleSubscribe(e){
		// create form data
		const data = new FormData()
		data.append('pid', this.playlist.pid)

		// disable button before we do anything
		const button = this.shadowRoot.getElementById('subscribe')
		button.disabled = true

		// attempt to query the server for subscribe change
		post('api/playlist/subscribe.php', data).then(status => {
			// update the button status
			if(status.subscribed){
				button.setAttribute('theme', 'primary error')
				button.innerHTML = button.innerHTML.replace(/Subscribe/, 'Unsubscribe')
			} else {
				button.setAttribute('theme', 'primary')
				button.innerHTML = button.innerHTML.replace(/Unsubscribe/, 'Subscribe')
			}

			button.disabled = false
		}).catch(err => {
			notify('Failed to toggle subscription', err)
			button.disabled = false
		})
	}
}

// define the class as a component
customElements.define(ViewPlaylist.is, ViewPlaylist)
