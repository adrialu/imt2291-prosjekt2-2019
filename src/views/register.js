import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-form-layout/vaadin-form-layout.js'
import '@vaadin/vaadin-form-layout/vaadin-form-item.js'
import '@vaadin/vaadin-text-field/vaadin-text-field.js'
import '@vaadin/vaadin-text-field/vaadin-email-field.js'
import '@vaadin/vaadin-text-field/vaadin-password-field.js'
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js'
import '@vaadin/vaadin-button/vaadin-button.js'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-form/iron-form.js'

import {post} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {login} from './../redux/actions.js'
import './../components/shared-style.js'

/**
 * Class that implements a registration page.
 * Once the visiting user has filled in the form, the user will also be logged in.
 */
class ViewRegister extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-register'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				justify-content: center;
			}

			vaadin-form-layout {
				max-width: 300px;
			}

			vaadin-checkbox {
				padding-top: 1rem;
			}

			vaadin-button {
				cursor: pointer;
			}

			h2 {
				margin: 20px 0;
			}
		`
	}

	/**
	 * Called when the component is added to DOM.
	 */
	firstUpdated(){
		// bail out if the user is already logged in (the state knows the user ID).
		const state = store.getState()
		if(state.user.uid)
			navigate('/')

		// listen to the Enter key in the form elements to attempt a submit
		this.shadowRoot.querySelectorAll('iron-form *[required]').forEach(el => {
			el.addEventListener('keyup', e => {
				if(e.keyCode == 13)
					this._register()
			})
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<iron-form>
				<form>
					<vaadin-form-layout>
						<h2>Register</h2>
						<p>Register as a user on the site to access more features.</p>

						<vaadin-text-field label='First name' name='givenName' minlength='3' maxlength='80' error-message='Must be between 3 and 80 characters.' required></vaadin-text-field>
						<vaadin-text-field label='Last name' name='familyName' minlength='3' maxlength='80' error-message='Must be between 3 and 80 characters.' required></vaadin-text-field>
						<vaadin-checkbox name='teacher'>Are you a teacher?</vaadin-checkbox>
						<vaadin-email-field label='Email' name='email' minlength='5' maxlength='80' error-message='Must be a valid email address between 5 and 80 characters.' required></vaadin-email-field>
						<vaadin-password-field label='Password' name='password' minlength='5' maxlength='72' error-message='Must be between 5 and 72 characters.' required></vaadin-password-field>
						<vaadin-form-item colspan='1'>
							<vaadin-button theme='primary' @click='${this._register}'>Register</vaadin-button>
						</vaadin-form-item>
					</vaadin-form-layout>
				</form>
			</iron-form>
		`
	}

	/**
	 * Callback handler for the form's submit click event and inputs' Enter presses.
	 * Handles registration with the server using the form data from the element.
	 */
	_register(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('iron-form')
		if(!form.validate())
			return
		const serialized = form.serializeForm()

		// fill a FormData object with the serialized data
		const data = new FormData()
		data.append('givenName', serialized.givenName)
		data.append('familyName', serialized.familyName)
		data.append('email', serialized.email)
		data.append('password', serialized.password)
		data.append('teacher', serialized.teacher == 'on')

		// lock form
		form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-email-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-password-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-checkbox').forEach(el => el.disabled = true)
		form.querySelectorAll('vaadin-button').forEach(el => el.disabled = true)

		// attempt to query the server for registration using the FormData
		post('api/user/register.php', data).then(user => {
			// dispatch the changes to the store
			store.dispatch(login({
				uid: user.uid,
				name: user.name,
				type: user.type,
				isStudent: user.type == 'student',
				isTeacher: user.type == 'teacher',
				isAdmin: user.type == 'admin',
			}))

			// redirect to home page
			navigate('/')
		}).catch(err => {
			// show error
			notify('Failed to register', err)

			// unlock form
			form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-email-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-password-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-checkbox').forEach(el => el.disabled = false)
			form.querySelectorAll('vaadin-button').forEach(el => el.disabled = false)
		})
	}
}

// define the class as a component
customElements.define(ViewRegister.is, ViewRegister)
