import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-ordered-layout'
import '@vaadin/vaadin-lumo-styles'

import {get, serverURL} from './../utils/request.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import './../components/shared-style.js'
import './../components/card-box.js'
import './../components/lecture-video-box.js'

/**
 * Class that implements the home page for student users.
 * The page displays a list of all subscriptions to playlists, sorted by latest updated first,
 * containing up to four of the latest videos added to the playlist.
 */
class ViewHomeStudent extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-home-student'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			playlists: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			vaadin-vertical-layout > div > a {
				margin-bottom: 10px;
				font-size: var(--lumo-font-size-l);
				font-weight: 600;
			}

			vaadin-horizontal-layout > lecture-video-box:nth-of-type(1n+5) {
				/* only show the first 4 */
				display: none;
			}

			vaadin-horizontal-layout > lecture-video-box {
				margin: 20px;
				margin-left: 0;
				max-width: 223px;
				max-height: 266px;
			}

			vaadin-horizontal-layout > lecture-video-box:nth-child(4) {
				margin-right: 0;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.playlists = []

		// grab info from server
		get('api/playlist/subscriptions.php').then(playlists => {
			for(let i in playlists){
				for(let j in playlists[i].videos){
					// convert all the dates before we copy over the array
					playlists[i].videos[j].uploaded = formatTime(playlists[i].videos[j].uploaded.date)
				}
			}

			this.playlists = playlists
		}).catch(err => {
			notify('Failed to get subscriptions', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<card-box>
				<h3 slot='header'>Subscriptions</h3>
				${this.playlists.length > 0 ?
					html`
						<vaadin-vertical-layout>
							${this.playlists.map(playlist => {
								return html`
									<div>
										<a href='/playlist/${playlist.pid}'>${playlist.title}</a>
										${playlist.videos.length > 0 ?
											html`
												<vaadin-horizontal-layout>
													${playlist.videos.map(video => {
														return html`
															<lecture-video-box
																link='/video/${video.vid}'
																title='${video.title}'
																thumbnail='${serverURL}api/video/thumb.php?vid=${video.vid}'
																description='${video.description}'
																meta='${video.uploaded}'
															>
															</lecture-video-box>
														`
													})}
												</vaadin-horizontal-layout>
											`:html`
												<p>Playlist is empty</p>
											`
										}
									</div>
								`
							})}
						</vaadin-vertical-layout>
					`: html`
						<p>You have no subscriptions, try adding some?</p>
					`
				}
			</card-box>
		`
	}
}

// define the class as a component
customElements.define(ViewHomeStudent.is, ViewHomeStudent)
