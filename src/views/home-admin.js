import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-grid'
import '@vaadin/vaadin-select'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-icons'
import '@vaadin/vaadin-lumo-styles'

import notify from './../utils/notify.js'
import {get, post} from './../utils/request.js'
import store from './../redux/store.js'
import './../components/card-box.js'
import './../components/shared-style.js'

/**
 * Class that implements the home page for admin users.
 * The page shows a list of users pending approval to become teachers, as well as a
 * list of all users, with the option to change their role.
 */
class ViewHomeAdmin extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-home-admin'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			users: Array,
			pending: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			vaadin-button {
				cursor: pointer;
			}

			vaadin-button[title='Approve'] {
				color: green;
			}

			vaadin-button[title='Deny'] {
				color: red;
			}

			vaadin-button[disabled] {
				color: gray;
			}

			.pending > p {
				margin: 20px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// load user from storage
		const state = store.getState()
		this.user = state.user

		// init properties
		this.users = []
		this.pending = []

		this._updateUsers()
		this._updatePending()
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<card-box class='pending' no-margin>
				<h4 slot='header'>Pending promotion</h4>

				${this.pending.length > 0 ?
					html`
						<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.pending)}'>
							<vaadin-grid-column path='uid' header='ID' width='80px' flex-grow='0'></vaadin-grid-column>
							<vaadin-grid-column path='name'>
								<template>
									<a href='/user/[[item.uid]]'>[[item.name]]</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column path='email'>
								<template>
									<a href='mailto:[[item.email]]'>[[item.email]]</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column width='224px' flex-grow='0' .renderer='${this._renderPendingButtons.bind(this)}'></vaadin-grid-column>
						</vaadin-grid>
					`:html`
						<p>There are no users pending promotion</p>
					`
				}
			</card-box>

			<card-box class='all-users' no-margin>
				<h4 slot='header'>Users</h4>

				<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.users)}'>
					<vaadin-grid-column path='uid' header='ID' width='80px' flex-grow='0'></vaadin-grid-column>
					<vaadin-grid-column path='name'>
						<template>
							<a href='/user/[[item.uid]]'>[[item.name]]</a>
						</template>
					</vaadin-grid-column>
					<vaadin-grid-column path='email'>
						<template>
							<a href='mailto:[[item.email]]'>[[item.email]]</a>
						</template>
					</vaadin-grid-column>
					<vaadin-grid-column header='Type' width='224px' flex-grow='0' .renderer='${this._renderRoleDropdowns.bind(this)}'></vaadin-grid-column>
				</vaadin-grid>
			</card-box>
		`
	}

	/**
	 * Fetches all users in the system from the server.
	 */
	_updateUsers(){
		get('api/user/all.php').then(users => this.users = users).catch(err => {
			notify('Failed to get users', err)
		})
	}

	/**
	 * Fetches all users pending approval to become teachers from the server.
	 */
	_updatePending(){
		get('api/user/pending.php').then(pending => this.pending = pending).catch(err => {
			notify('Failed to get pending requests', err)
		})
	}

	/**
	 * Renders dropdowns in the last column of the "all users" list.
	 */
	_renderRoleDropdowns(root, col, data){
		let dropdown = root.firstElementChild
		if(dropdown)
			// bail out early, the dropdown already exists
			return

		// create the dropdown
		root.innerHTML = `
			<vaadin-select value='${data.item.type.charAt(0).toUpperCase() + data.item.type.slice(1)}' data-uid='${data.item.uid}'>
				<template>
					<vaadin-list-box>
						<vaadin-item>Student</vaadin-item>
						<vaadin-item>Teacher</vaadin-item>
						<vaadin-item>Admin</vaadin-item>
					</vaadin-list-box>
				</template>
			</vaadin-select>
		`

		// get the dropdown
		dropdown = root.firstElementChild

		// disable dropdown on own user
		if(data.item.uid == this.user.uid){
			dropdown.readonly = true
			dropdown.setAttribute('title', 'You can\'t change your own role')
		}

		// hook 'value-changed' event for the dropdown
		dropdown.addEventListener('value-changed', e => {
			// lock the dropdown before we do anything
			dropdown.readonly = true

			// create form data
			const data = new FormData()
			data.append('uid', dropdown.dataset.uid)
			data.append('type', e.detail.value.toLowerCase())

			// attempt to query the server to change the user's type
			post('api/user/role.php', data).then(_ => {
				// unlock the dropdown again
				dropdown.readonly = false
			}).catch(err => {
				// unlock dropdown and show error
				dropdown.readonly = false
				notify('Failed to update user', err)
			})
		})
	}

	/**
	 * Renders buttons in the last column of the "pending approval" list.
	 */
	_renderPendingButtons(root, col, data){
		let buttons = root.firstElementChild
		if(buttons)
			// bail out early, the buttons already exists
			return

		// create the buttons
		root.innerHTML = `
			<div class='buttons' data-uid='${data.item.uid}'>
				<vaadin-button theme='secondary icon' title='Approve'>
					<iron-icon icon='vaadin:check'></iron-icon>
				</vaadin-button>
				<vaadin-button theme='secondary icon' title='Deny'>
					<iron-icon icon='vaadin:close'></iron-icon>
				</vaadin-button>
			</div>
		`

		// hook 'click' event for the buttons
		buttons = root.firstElementChild
		buttons.querySelectorAll('vaadin-button').forEach(button => {
			button.addEventListener('click', e => {
				// lock both buttons before we do anything
				buttons.querySelectorAll('vaadin-button').forEach(btn => btn.disabled = true)

				// create form data
				const data = new FormData()
				data.append('uid', buttons.dataset.uid)
				data.append('type', button.title == 'Approve' ? 'teacher' : 'student')

				// attempt to query the server to change the user's type
				post('api/user/role.php', data).then(_ => {
					// update pending list
					this._updatePending()
				}).catch(err => {
					// unlock buttons and show error
					buttons.querySelectorAll('vaadin-button').forEach(btn => btn.disabled = false)
					notify('Failed to update user', err)
				})
			})
		})
	}
}

// define the class as a component
customElements.define(ViewHomeAdmin.is, ViewHomeAdmin)
