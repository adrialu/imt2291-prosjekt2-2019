import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-icons'
import '@vaadin/vaadin-form-layout'
import '@vaadin/vaadin-custom-field'
import '@vaadin/vaadin-text-field'
import '@vaadin/vaadin-text-field/vaadin-text-area'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-item'
import '@vaadin/vaadin-progress-bar'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-form'
import '@polymer/iron-icon'

import {get, upload, serverURL} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {error} from './../redux/actions.js'
import './../components/shared-style.js'

/**
 * Class that implements a page to edit an existing video.
 * This is presented as a form, with the option to change every attribute about the video, except
 * for uploading a new video file.
 * It also has an option to grab the thumbnail directly from the video instead of uploading one.
 */
class ViewVideoEdit extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-video-edit'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			vid: Number, // router param
			video: Object,
			user: Object,
			thumbnail: String,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				justify-content: center;
			}

			vaadin-form-layout {
				max-width: 500px;
			}

			vaadin-form-item {
				margin-top: 20px;
				display: unset;
			}

			vaadin-form-item vaadin-button {
				width: 40%;
			}

			vaadin-form-item vaadin-button:first-of-type {
				margin-right: 3.2%;
			}

			vaadin-text-field input[type=file] {
				padding-top: 10px;
				height: 10px;
			}

			@-moz-document url-prefix() {
				vaadin-text-field input[type=file] {
					height: 26px;
				}
			}

			header {
				margin-top: 20px;
				display: flex;
				justify-content: space-between;
			}

			header img {
				width: 200px;
				border: 1px solid #dfdfdf;
				border-radius: 0.25rem;
			}

			vaadin-custom-field div {
				background: rgb(232, 235, 239);
				border-radius: 5px;
			}

			vaadin-custom-field div > * {
				width: calc(99.9% + 0rem - 20px);
				margin: 0 10px;
			}

			vaadin-custom-field div > video {
				margin-top: 10px;
			}

			vaadin-custom-field div > vaadin-button {
				margin-bottom: 10px;
			}

			#progress-parent {
				width: 100% !important;
				margin-left: 0 !important;
			}

			[hidden] {
				display: none;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.video = {}

		// bail out if the user is already logged in (the state knows the user ID), or the user
		// is not a teacher
		const state = store.getState()
		if(!state.user.uid || !state.user.isTeacher)
			navigate('/')
		this.user = state.user
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// fetch video metadata
		get('api/video/get.php?vid=' + this.vid).then(video => {
			if(this.user.uid != video.author.uid){
				// the user is not the author, bail out
				navigate('/')
			}

			// update video info
			this.video = video
		}).catch(err => {
			// store error message
			store.dispatch(error({
				title: 'Failed to edit video',
				msg: err
			}))

			// redirect to error page
			navigate('/error')
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<iron-form>
				<form>
					<vaadin-form-layout>
						<header>
							<h2>Edit video</h2>
							<img src='${serverURL}api/video/thumb.php?vid=${this.vid}'>
						</header>

						<vaadin-text-field label='Title' name='title' minlength='10' maxlength='50' error-message='Must be between 10 and 50 characters.' required value='${this.video.title}'></vaadin-text-field>
						<vaadin-text-field label='Course' name='course' minlength='3' maxlength='50' error-message='Must be between 3 and 50 characters.' required value='${this.video.course}'></vaadin-text-field>
						<vaadin-text-field label='Subject' name='subject' minlength='10' maxlength='50' error-message='Must be between 5 and 50 characters.' required value='${this.video.subject}'></vaadin-text-field>
						<vaadin-text-area label='Description' name='description' minlength='10' maxlength='500' error-message='Must be between 10 and 500 characters.' required value='${this.video.description}'></vaadin-text-area>

						<vaadin-text-field label='Thumbnail (optional)' name='thumbnail' error-message='Max size 10 MB.'>
							<input slot='input' type='file' accept='image/*' @change='${this._thumbnailFilePicked}'>
						</vaadin-text-field>
						<vaadin-custom-field name='thumbnail2' label='Thumbnail from video (optional)'>
							<div>
								<video id='thumbnail-video' oncontextmenu='return false;'> <!-- disable right-clicking -->
									<source src='${serverURL}api/video/stream.php?vid=${this.vid}'>
								</video>
								<input id='thumbnail-seek' type='range' @change='${this._videoSeek}' value='0'>
								<vaadin-button theme='secondary' @click='${this._pickThumbnail}'>
									<iron-icon icon='vaadin:check' style='display:none;'></iron-icon>
									Use thumbnail from video
								</vaadin-button>
							</div>
						</vaadin-custom-field>
						<vaadin-text-field label='Caption (optional)' name='caption' error-message='Max size 1 MB.'>
							<input slot='input' type='file' accept='.vtt'>
						</vaadin-text-field>

						<vaadin-form-item id='progress-parent' hidden>
							<vaadin-progress-bar id='progress'></vaadin-progress-bar>
							Uploading: <span id='progress-value'>0</span> %
						</vaadin-form-item>

						<vaadin-form-item>
							<vaadin-button theme='primary' @click='${this._upload}'>Save</vaadin-button>
						</vaadin-form-item>
					</vaadin-form-layout>
				</form>
			</iron-form>
		`
	}

	/**
	 * Click event for the save button.
	 * Handles form validation and file uploading to the server.
	 * It also controls the progress bar, indicating the upload status.
	 * On a successful upload it redirects to the video page.
	 */
	_upload(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('iron-form')
		if(!form.validate())
			return
		const serialized = form.serializeForm()

		// get file upload form parts
		const thumb = this.shadowRoot.querySelector('[name=thumbnail] input[type=file]')
		const caption = this.shadowRoot.querySelector('[name=caption] input[type=file]')

		// fill a FormData object with the serialized text data
		const data = new FormData()
		data.append('vid', this.vid)
		data.append('title', serialized.title)
		data.append('course', serialized.course)
		data.append('subject', serialized.subject)
		data.append('description', serialized.description)

		if(thumb.files.length == 1){
			if(thumb.files[0].size > 1e7){ // 10 MB
				notify('Thumbnail file too big')
				return
			} else
				data.append('thumbnail', thumb.files[0])
		} else if(this.thumbnail)
			data.append('thumbnail', this.thumbnail)

		if(caption.files.length == 1){
			if(caption.files[0].size > 1e6){ // 1 MB
				notify('Caption file too big')
				return
			} else
				data.append('caption', caption.files[0])
		}

		// lock all fields before we continue
		form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-text-area').forEach(el => el.readonly = true)
		form.querySelectorAll('vaadin-button').forEach(el => el.disabled = true)
		form.querySelectorAll('vaadin-text-field input').forEach(el => el.disabled = true)
		form.querySelector('vaadin-custom-field input').disabled = true
		form.querySelector('vaadin-custom-field vaadin-button').disabled = true
		form.querySelector('vaadin-custom-field video').style.opacity = 0.5

		// show and reset progress bar
		const progress = this.shadowRoot.getElementById('progress')
		const progressValue = this.shadowRoot.getElementById('progress-value')
		const progressParent = this.shadowRoot.getElementById('progress-parent')
		progressParent.hidden = false
		progress.value = progressValue.innerText = 0

		// attempt to upload to server
		upload('api/video/edit.php', data).progress(e => {
			// display progress as both text and a progress bar
			if(e.lengthComputable){
				const perc = (e.loaded / e.total) * 100
				progress.value = perc / 100
				progressValue.innerText = ~~perc
			}
		}).then(video => {
			navigate(`/video/${video.vid}`)
		}).catch(err => {
			notify('Failed to edit video', err)

			// unlock all fields
			form.querySelectorAll('vaadin-text-field').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-text-area').forEach(el => el.readonly = false)
			form.querySelectorAll('vaadin-button').forEach(el => el.disabled = false)
			form.querySelectorAll('vaadin-text-field input').forEach(el => el.disabled = false)
			form.querySelectorAll('vaadin-text-field input').forEach(el => el.value = '')
			form.querySelector('vaadin-custom-field input').disabled = false
			form.querySelector('vaadin-custom-field vaadin-button').disabled = false
			form.querySelector('vaadin-custom-field video').style.opacity = 1
		})
	}

	/**
	 * Picks the current frame from the video and stores it as a video file in memory.
	 * This is triggered by the "Use thumbnail from video" button in the form.
	 */
	_pickThumbnail(e){
		const video = this.shadowRoot.getElementById('thumbnail-video')

		// create a new canvas, set the size to max thumbnail size allowed by the server
		const canvas = document.createElement('canvas')
		canvas.width = 1280
		canvas.height = 720

		// render the video still to the canvas
		const context = canvas.getContext('2d')
		context.drawImage(video, 0, 0, 1280, 720)

		// save the image blob as a file to a variable
		canvas.toBlob(blob => {
			this.thumbnail = new File([blob], 'thumbnail.png', {
				type: 'image/png',
				lastModified: new Date
			})
		})

		// disable the upload button
		this.shadowRoot.querySelector('vaadin-text-field[name=thumbnail]').readonly = true
		this.shadowRoot.querySelector('vaadin-text-field[name=thumbnail] input').disabled = true

		// show the checkmark
		this.shadowRoot.querySelector('vaadin-custom-field iron-icon').style.display = 'inline-block'
	}

	/**
	 * Seeks thumbnail video to a specific time by the seek bar.
	 */
	_videoSeek(e){
		const video = this.shadowRoot.getElementById('thumbnail-video')
		const input = this.shadowRoot.getElementById('thumbnail-seek')
		video.currentTime = video.duration * (input.value / 100)

		// clear the stored image
		this.thumbnail = null

		// enable the upload button
		this.shadowRoot.querySelector('vaadin-text-field[name=thumbnail]').readonly = false
		this.shadowRoot.querySelector('vaadin-text-field[name=thumbnail] input').disabled = false

		// hide the checkmark
		this.shadowRoot.querySelector('vaadin-custom-field iron-icon').style.display = 'none'
	}

	/**
	 * Disable the thumbnail video time picker when a file has been selected from disk.
	 */
	_thumbnailFilePicked(){
		this.shadowRoot.querySelector('vaadin-custom-field video').style.opacity = 0.5
		this.shadowRoot.querySelector('vaadin-custom-field input').disabled = true
		this.shadowRoot.querySelector('vaadin-custom-field vaadin-button').disabled = true
	}
}

// define the class as a component
customElements.define(ViewVideoEdit.is, ViewVideoEdit)
