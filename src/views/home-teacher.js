import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-grid'
import '@vaadin/vaadin-tabs'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-select'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-pages'

import {get, post, serverURL} from './../utils/request.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import './../components/shared-style.js'
import './../components/card-box.js'

/**
 * Class that implements the home page for teacher users.
 * The page displays a two-tab layout, one for videos and one for playlists.
 * The tabbed pages lists all the videos/playlists the user has created, and buttons to create more.
 * Lastly, the video tab has a column to assign videos to playlists.
 */
class ViewHomeTeacher extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-home-teacher'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			page: Number,
			videos: Array,
			playlists: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			vaadin-tabs {
				box-shadow: none;
				margin-bottom: -1px;
			}

			page {
				position: relative;
			}

			page > a {
				position: absolute;
				top: 15px;
				left: 40px;
				z-index: 100;
			}

			page > a > vaadin-button {
				margin-top: 12px;
				margin-left: 12px;
			}

			#videos vaadin-grid vaadin-button {
				width: 192px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.videos = []
		this.playlists = []
		this.playlistsKnownLength = 0

		// get all videos
		get('api/video/all.php').then(videos => {
			for(let i in videos){
				// convert all the dates before we copy over the array
				videos[i].uploaded = formatTime(videos[i].uploaded)

				// add an empty playlist table if the video isn't assigned
				if(!videos[i].playlist)
					videos[i].playlist = {}
			}

			// update videos
			this.videos = videos
		}).catch(err => {
			notify('Failed to get videos', err)
		})

		// get all playlists
		get('api/playlist/all.php').then(playlists => {
			for(let i in playlists){
				// convert all the dates before we copy over the array
				playlists[i].updated = formatTime(playlists[i].updated)
			}

			this.playlists = playlists
		}).catch(err => {
			notify('Failed to get playlists', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<card-box no-padding-header no-margin>
				<vaadin-tabs slot='header' selected='${this.page || 0}'>
					<vaadin-tab @click='${() => this._setPage(0)}'>Videos</vaadin-tab>
					<vaadin-tab @click='${() => this._setPage(1)}'>Playlists</vaadin-tab>
				</vaadin-tabs>

				<iron-pages selected='${this.page || 0}'>
					<page id='videos'>
						<a href='/new/video'>
							<vaadin-button>New video</vaadin-button>
						</a>
						<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.videos)}'>
							<vaadin-grid-column class='thumb' width='200px' flex-grow='0'>
								<template>
									<a href='/video/[[item.vid]]'>
										<image src='${serverURL}api/video/thumb.php?vid=[[item.vid]]' width='180px' height='101px'>
									</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column class='title' path='title' flex-grow='3'>
								<template>
									<a href='/video/[[item.vid]]'>[[item.title]]</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column path='uploaded'></vaadin-grid-column>
							<vaadin-grid-column path='views'></vaadin-grid-column>
							<vaadin-grid-column path='playlist' width='220px' flex-grow='0' .renderer='${this._renderVideoPlaylistColumn.bind(this)}'></vaadin-grid-column>
						</vaadin-grid>
					</page>
					<page id='playlists'>
						<a href='/new/playlist'>
							<vaadin-button>New playlist</vaadin-button>
						</a>
						<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.playlists)}'>
							<vaadin-grid-column class='thumb' width='200px' flex-grow='0'>
								<template>
									<a href='/playlist/[[item.pid]]'>
										<image src='${serverURL}api/playlist/thumb.php?pid=[[item.pid]]' width='180px' height='101px'>
									</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column path='title' flex-grow='3'>
								<template>
									<a href='/playlist/[[item.pid]]'>[[item.title]]</a>
								</template>
							</vaadin-grid-column>
							<vaadin-grid-column path='updated'></vaadin-grid-column>
							<vaadin-grid-column path='videos'></vaadin-grid-column>
						</vaadin-grid>
					</page>
				</iron-pages>
			</card-box>
		`
	}

	/**
	 * Renderer for the video column 'playlist', creating dropdowns or buttons to assign videos
	 * to/from playlists.
	 */
	_renderVideoPlaylistColumn(root, col, data){
		// make sure we don't try to re-render the dropdown, unless we've got updates
		if(root.firstElementChild && root.__playlistsKnownLength == this.playlists.length)
			return

		// store the last length of the playlists
		root.__playlistsKnownLength = this.playlists.length

		if(data.item.playlist.pid)
			this._renderVideoButton(root, data.item)
		else
			this._renderVideoDropdown(root, data.item)
	}

	/**
	 * Renderer for the buttons in the video colum 'playlist'.
	 * This handles clearing the playlist assignment for a video.
	 * Once unassigned it will render a dropdown instead.
	 */
	_renderVideoButton(root, item){
		// render a button to remove from playlist
		root.innerHTML = `
			<vaadin-button theme='secondary'>
				${item.playlist.title}
			</vaadin-button>
		`

		// handle click to remove from playlist, replace with dropdown
		const button = root.firstElementChild
		button.addEventListener('click', e => {
			// lock button before we do anything
			button.disabled = true

			// create form data
			const data = new FormData()
			data.append('vid', item.vid)
			data.append('pid', item.playlist.pid)
			data.append('option', 'remove')

			// query the server
			post('api/video/assign.php', data).then(_ => {
				// update the data
				item.playlist = {}

				// replace the button with a dropdown
				this._renderVideoDropdown(root, item)
			}).catch(err => {
				notify('Failed to clear playlist assignment', err)

				// unlock the button
				button.disabled = false
			})
		})

		// handle mouse enter/leave states, rendering the button differently
		button.addEventListener('mouseenter', e => {
			button.setAttribute('theme', 'secondary error')
			button.innerHTML = `
				<iron-icon icon='vaadin:close' slot='prefix'></iron-icon>
				Unassign
			`
		})

		button.addEventListener('mouseleave', e => {
			button.setAttribute('theme', 'secondary')
			button.innerHTML = item.playlist.title
		})
	}

	/**
	 * Renderer for the dropdowns in the video column 'playlist'.
	 * This handles assigning playlists to videos.
	 * Once assigned it will render a button instead.
	 */
	_renderVideoDropdown(root, item){
		// render dropdown button and list of playlists
		root.innerHTML = `
			<vaadin-select placeholder='---' value='${item.playlist.pid}'>
				<template>
					<vaadin-list-box>
						${this.playlists.length > 0 ?
							`${this.playlists.map(playlist => {
								return `<vaadin-item value='${playlist.pid}'>${playlist.title}</vaadin-item>`
							}).join('')}`:
							`<vaadin-item disabled>No playlists</vaadin-item>`
						}
					</vaadin-list-box>
				</template>
			</vaadin-select>
		`

		// handle assignments when a playlist is chosen
		const dropdown = root.firstElementChild
		dropdown.addEventListener('value-changed', e => {
			// lock dropdown before we do anything
			dropdown.readonly = true

			// create form data
			const data = new FormData()
			data.append('vid', item.vid)
			data.append('pid', +e.detail.value)
			data.append('option', 'add')

			// query the server
			post('api/video/assign.php', data).then(_ => {
				// update the data
				item.playlist = {
					pid: data.get('pid'),
					title: this.playlists.find(p => p.pid == data.get('pid')).title
				}

				// replace the dropdown with a button
				this._renderVideoButton(root, item)
			}).catch(err => {
				notify('Failed to clear playlist assignment', err)

				// unlock the button
				dropdown.readonly = false
			})
		})
	}

	/**
	 * Called when tabs are clicked, this will switch the displayed page for the tab.
	 */
	_setPage(page){
		this.page = page
	}
}

// define the class as a component
customElements.define(ViewHomeTeacher.is, ViewHomeTeacher)
