import {html, LitElement} from 'lit-element'

import '@vaadin/vaadin-grid'
import '@vaadin/vaadin-tabs'
import '@vaadin/vaadin-lumo-styles'
import '@polymer/iron-pages'

import {get, serverURL} from './../utils/request.js'
import formatTime from './../utils/formatTime.js'
import notify from './../utils/notify.js'
import './../components/shared-style.js'
import './../components/card-box.js'

/**
 * Class that implements a page to display search results in a tabbed-layout.
 * Each tab on the page represents the search results based on the query, one tab each for videos,
 * playlists and users. Tabbed pages are not displayed if there are no results for the page.
 */
class ViewSearch extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-search'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			query: String, // router param
			page: Number,
			videos: Array,
			playlists: Array,
			users: Array,
		}
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.videos = []
		this.playlists = []
		this.users = []
	}

	/**
	 * Called when the component is added to DOM, handling ad-hoc tasks.
	 */
	firstUpdated(){
		// query the server
		get('api/search.php?q=' + this.query).then(results => {
			// format the time before we update the arrays
			for(let i in results.videos){
				results.videos[i].uploaded = formatTime(results.videos[i].uploaded.date)
			}
			for(let i in results.playlists){
				results.playlists[i].updated = formatTime(results.playlists[i].updated.date)
			}

			this.videos = results.videos
			this.playlists = results.playlists
			this.users = results.users
		}).catch(err => {
			notify('Failed to get search results', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			${this._renderPages()}
		`
	}

	/**
	 * Renderer for search result pages/tabs.
	 */
	_renderPages(){
		// create an array with each type of content in it.
		// this is used to control the page index in the tabbed box.
		const tabs = []
		if(this.videos && this.videos.length > 0)
			tabs.push('videos')
		if(this.playlists && this.playlists.length > 0)
			tabs.push('playlists')
		if(this.users && this.users.length > 0)
			tabs.push('users')

		if(tabs.length > 0)
			return html`
				<card-box no-padding-header no-margin>
					<vaadin-tabs slot='header' selected='${this.page || 0}'>
						${this._renderTab(tabs, 'Videos')}
						${this._renderTab(tabs, 'Playlists')}
						${this._renderTab(tabs, 'Users')}
					</vaadin-tabs>
					<iron-pages selected='${this.page || 0}'>
						${this._renderVideos()}
						${this._renderPlaylists()}
						${this._renderUsers()}
					</iron-pages>
				</card-box>
			`
		else
			return html`
				<card-box no-header>
					<h4>No results for "${this.query}"</h4>
				</card-box>
			`
	}

	/**
	 * Renderer for tabs.
	 */
	_renderTab(tabsList, tabName){
		if(tabsList.indexOf(tabName.toLowerCase()) >= 0)
			return html`
				<vaadin-tab @click='${() => this._setPage(tabsList.indexOf(tabName.toLowerCase()))}'>${tabName}</vaadin-tab>
			`
	}

	/**
	 * Renderer for videos, only loaded if there are any.
	 */
	_renderVideos(){
		if(this.videos && this.videos.length > 0)
			return html`
				<page id='videos'>
					<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.videos)}'>
						<vaadin-grid-column width='200px' flex-grow='0'>
							<template>
								<a href='/video/[[item.vid]]'>
									<image src='${serverURL}api/video/thumb.php?vid=[[item.vid]]' width='180px' height='101px'>
								</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='title'>
							<template>
								<a href='/video/[[item.vid]]'>[[item.title]]</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='uploaded'></vaadin-grid-column>
						<vaadin-grid-column path='views'></vaadin-grid-column>
					</vaadin-grid>
				</page>
			`
	}

	/**
	 * Renderer for playlists, only loaded if there are any.
	 */
	_renderPlaylists(){
		if(this.playlists && this.playlists.length > 0)
			return html`
				<page id='playlists'>
					<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.playlists)}'>
						<vaadin-grid-column width='200px' flex-grow='0'>
							<template>
								<a href='/playlist/[[item.pid]]'>
									<image src='${serverURL}api/playlist/thumb.php?pid=[[item.pid]]' width='180px' height='101px'>
								</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='title'>
							<template>
								<a href='/playlist/[[item.pid]]'>[[item.title]]</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='updated'></vaadin-grid-column>
						<vaadin-grid-column path='videos'></vaadin-grid-column>
					</vaadin-grid>
				</page>
			`
	}

	/**
	 * Renderer for users, only loaded if there are any.
	 */
	_renderUsers(){
		if(this.users && this.users.length > 0)
			return html`
				<page id='users'>
					<vaadin-grid height-by-rows theme='no-border' items='${JSON.stringify(this.users)}'>
						<vaadin-grid-column path='name'>
							<template>
								<a href='/user/[[item.uid]]'>[[item.name]]</a>
							</template>
						</vaadin-grid-column>
						<vaadin-grid-column path='type' header='Role' .renderer='${this._renderUser}'></vaadin-grid-column>
					</vaadin-grid>
				</page>
			`
	}

	/**
	 * Renderer for a single user 'type' column row.
	 * All it does is uppercase the first letter in the type.
	 */
	_renderUser(root, col, data){
		root.innerText = data.item.type.charAt(0).toUpperCase() + data.item.type.slice(1)
	}

	/**
	 * Called when tabs are clicked, this will switch the displayed page for the tab.
	 */
	_setPage(page){
		this.page = page
	}
}

// define the class as a component
customElements.define(ViewSearch.is, ViewSearch)
