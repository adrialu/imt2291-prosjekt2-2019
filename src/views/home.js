import {html, LitElement} from 'lit-element'

import store from './../redux/store.js'

/**
 * Class that imports and renders the appropriate home page view based on the user role.
 */
class ViewHome extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-home'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			user: Object,
		}
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// load user from storage
		const state = store.getState()
		this.user = state.user

		// subscribe to state changes in storage
		store.subscribe(state => {
			// store the new state
			this.user = store.getState().user
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		switch(this.user.type){
			case 'admin':
				import('./home-admin.js')
				return html`<view-home-admin></view-home-admin>`
			case 'teacher':
				import('./home-teacher.js')
				return html`<view-home-teacher></view-home-teacher>`
			case 'student':
				import('./home-student.js')
				return html`<view-home-student></view-home-student>`
			default:
				import('./home-guest.js')
				return html`<view-home-guest></view-home-guest>`
		}
	}
}

// define the class as a component
customElements.define(ViewHome.is, ViewHome)
