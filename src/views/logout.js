import {LitElement} from 'lit-element'

import {get} from './../utils/request.js'
import navigate from './../utils/navigate.js'
import notify from './../utils/notify.js'
import store from './../redux/store.js'
import {logout} from './../redux/actions.js'

/**
 * Class that implements a dummy page that will log any user out and redirect back home.
 */
class ViewLogout extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'view-logout'
	}

	/**
	 * Called when the component is added to DOM.
	 * Will log the user out, killing the stored state.
	 */
	firstUpdated(){
		// if the user is not logged in (state has an user ID), bail out early
		const data = store.getState()
		if(!data.user.uid)
			navigate('/')

		// attempt to query the server to kill the session
		get('api/user/logout.php').then(_ => {
			// kill session info
			store.dispatch(logout())

			// redirect to home
			navigate('/')
		}).catch(err => {
			notify('Failed to log out', err)
		})
	}
}

// define the class as a component
customElements.define(ViewLogout.is, ViewLogout)
