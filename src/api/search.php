<?php

/*
	# GET `/api/search.php`

	Returns all search results by query, may it be videos, playlists, or users.

	## Data constraints

	Takes the following URL parameters:

	- `query` - query string _(string, optional)_

	## Success response

	Returns a JSON object which contains arrays of resulting videos, playlists or users.  
	Each array contains objects with information about the video, playlist or user.

	```
	{
		"query": String, // the query itself
		"videos": [
			{
				"vid": Number, // video ID
				"title": String,
				"description": String,
				"uploaded": DateTime,
				"views": Number  // number of views
			}
			...
		],
		"playlists": [
			{
				"pid": Number, // playlist ID
				"title": String,
				"description": String,
				"updated": DateTime,
				"videos": Number // number of videos in the playlist
			}
			...
		],
		"users": [
			{
				"uid": Number, // user ID
				"name": String,
				"type": String // the user's role, "student", "teacher" or "admin"
			}
			...
		]
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$query = $_GET['q'];
if(!isset($query) || empty($query))
	respond(400, 'Missing search query');

try {
	// search for each type
	$user = new User($dbh);
	$users = $user->search($query);

	$video = new Video($dbh);
	$videos = $video->search($query);

	$playlist = new Playlist($dbh);
	$playlists = $playlist->search($query);

	// prepare the search results
	$res = [
		'query'     => $query,
		'users'     => [],
		'videos'    => [],
		'playlists' => [],
	];

	// iterate and build response
	foreach($users as $user){
		array_push($res['users'], [
			'uid'  => $user->getID(),
			'name' => $user->givenName . ' ' . $user->familyName,
			'type' => $user->getType(),
		]);
	}

	foreach($videos as $video){
		array_push($res['videos'], [
			'vid'         => $video->getID(),
			'title'       => $video->title,
			'description' => $video->description,
			'uploaded'    => $video->uploaded,
			'views'       => $video->views,
		]);
	}

	foreach($playlists as $playlist){
		array_push($res['playlists'], [
			'pid'         => $playlist->getID(),
			'title'       => $playlist->title,
			'description' => $playlist->description,
			'updated'     => $playlist->updated,
			'videos'      => count($playlist->getVideos()),
		]);
	}

	// respond
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
