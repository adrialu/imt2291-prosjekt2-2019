<?php

/*
	# GET `/api/playlist/all.php`

	Returns all playlists by the user.

	## Data constraints

	Takes the following URL parameters:

	- `uid` - user ID _(number, optional)_
		- if not supplied, the logged in user is considered

	## Success response

	Returns a JSON array of objects containing information about each playlist.

	```
	[
		{
			"pid": Number, // playlist ID
			"title": String,
			"description": String,
			"updated": DateTime,
			"videos": Number // number of videos in the playlist
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$uid = $_GET['uid'];
if(isset($uid) && !empty($uid))
	$uid = (int)$uid;
else
	$uid = -1;

try {
	// get the correct user we're querying
	$user = new User($dbh, $uid);
	if($uid == -1 && !$user->isTeacher())
		respond(401, 'Unauthorized access');

	// get playlists
	$playlist = new Playlist($dbh);
	$playlists = $playlist->getAllFromUser($user);

	// create associative array
	$res = array();
	foreach($playlists as $data){
		array_push($res, [
			'pid'         => $data->getID(),
			'title'       => $data->title,
			'description' => $data->description,
			'updated'     => $data->updated->format(DATE_ISO8601),
			'videos'      => count($data->getVideos()),
		]);
	}

	// respond with the data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
