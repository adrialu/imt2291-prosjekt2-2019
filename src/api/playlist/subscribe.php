<?php

/*
	# GET `/api/playlist/subscribe.php`

	Returns the user's subscription status for a playlist.  
	Only available to users with the "student" role.

	## Data constraints

	Requires the following URL parameters:

	- `pid` - playlist ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the user's rating.

	```
	{
		"pid": Number, // playlist ID
		"subscribed": Boolean
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

/*
	# POST `/api/playlist/subscribe.php`

	Toggles the user's subscription of a playlist.  
	Only available to users with the "student" role.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `pid` - playlist ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the user's rating.

	```
	{
		"pid": Number, // playlist ID
		"subscribed": Boolean
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isStudent())
	respond(401, 'Unauthorized access');

$pid = $_POST['pid'];
if(!isset($pid) || empty($pid))
	$pid = $_GET['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');

try {
	// get playlist subscription status
	$playlist = new Playlist($dbh, $pid);
	$subscribed = $playlist->isSubscribed();

	if(isset($_POST['pid'])){
		// user is attempting to TOGGLE subscription status
		if($subscribed)
			$playlist->unsubscribe();
		else
			$playlist->subscribe();
		$subscribed = !$subscribed;
	}

	// respond with data
	respond(200, [
		'pid'        => (int)$pid,
		'subscribed' => $subscribed,
	]);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
