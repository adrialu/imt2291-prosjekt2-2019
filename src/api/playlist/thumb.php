<?php

/*
	# GET `/api/playlist/thumb.php`

	Returns the thumbnail of a playlist.

	## Data constraints

	Requires the following URL parameters:

	- `pid` - playlist ID _(number, required)_

	## Success response

	The thumbnail blob.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';

$pid = $_GET['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');

$path = '/var/www/data/thumbnails/playlists/' . $pid;
if(!file_exists($path))
	respond(400, 'Thumbnail doesn\'t exist');

// serve the file
header('Content-Type: ' . mime_content_type($path));
header('Content-Disposition: attachment; filename="' . $pid . '"');
readfile($path);
