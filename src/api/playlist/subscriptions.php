<?php

/*
	# GET `/api/playlist/subscriptions.php`

	Returns the logged-in user's subscriptions.  
	Only available to users with the "student" role.

	## Success response

	Returns a JSON array of subscriptions (playlists) and their latest videos.

	```
	[
		{
			"pid": Number, // playlist ID
			"title": String,
			"videos": [
				{
					"vid": Number, // video ID
					"title": String,
					"description": String,
					"uploaded": DateTime
				}
				...
			]
		}
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isStudent())
	respond(401, 'Unauthorized access');

try {
	// get playlist subscription status
	$playlist = new Playlist($dbh);
	$playlists = $playlist->getSubscriptions();

	// create associative array
	$res = array();
	foreach($playlists as $playlist){
		$data = [
			"pid"    => $playlist->getID(),
			"title"  => $playlist->title,
			"videos" => [],
		];

		// attach videos to the list
		$videos = $playlist->getVideos();
		foreach($videos as $video){
			array_push($data['videos'], [
				"vid"         => $video->getID(),
				"title"       => $video->title,
				"description" => $video->description,
				"uploaded"    => $video->uploaded,
			]);
		}

		array_push($res, $data);
	}

	// respond with data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
