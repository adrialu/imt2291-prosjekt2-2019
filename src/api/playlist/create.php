<?php

/*
	# POST `/api/playlist/create.php`

	Create a new playlist.  
	Only available to users with the "teacher" role.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `title` - name of the playlist _(string, required)_
	- `description` - description of the playlist _(string, required)_
	- `thumbnail` - the playlist thumbnail _(blob, optional)_

	## Success response

	Returns a JSON object containing the playlist ID.

	```
	{
		"pid": Number // playlist ID
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

if(!isset($_FILES['thumbnail']) || empty($_FILES['thumbnail']['tmp_name']))
	respond(400, 'Missing thumbnail file');

if(!is_uploaded_file($_FILES['thumbnail']['tmp_name']))
	respond(400, 'Invalid thumbnail file');

try {
	// prepare the playlist object
	$playlist = new Playlist($dbh);
	$playlist->title = $_POST['title'];
	$playlist->description = $_POST['description'];

	// attempt to upload the playlist metadata
	$id = $playlist->create();

	// playlist metadata uploaded, store the thumbnail file
	resize_thumb($_FILES['thumbnail']['tmp_name']);
	move_uploaded_file($_FILES['thumbnail']['tmp_name'], '/var/www/data/thumbnails/playlists/' . $id);

	// all went well, respond positively with the playlist's ID
	respond(201, [
		'pid' => $id
	]);
} catch(Exception $e){
	if(isset($id)){
		// remove the playlist
		$playlist->destroy();
	}

	// respond(500, 'Failed to create playlist');
	respond(500, $e->getMessage());
}
