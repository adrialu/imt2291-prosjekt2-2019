<?php

/*
	# POST `/api/playlist/edit.php`

	Edit an existing playlist by ID.  
	Only available to users with the "teacher" role that owns the playlist.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `pid` - playlist ID _(number, required)_
	- `title` - name of the playlist _(string, required)_
	- `description` - description of the playlist _(string, required)_
	- `thumbnail` - the playlist thumbnail _(blob, optional)_

	## Success response

	Returns a JSON object containing the playlist ID.

	```
	{
		"pid": Number // playlist ID
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$pid = $_POST['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');
$pid = (int)$pid;

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name']) && !is_uploaded_file($_FILES['thumbnail']['tmp_name']))
	respond(400, 'Invalid thumbnail file');

try {
	// prepare the playlist object
	$playlist = new Playlist($dbh, $pid);

	// validate the user's access to the given playlist
	if($playlist->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your playlist');

	$playlist->title = $_POST['title'];
	$playlist->description = $_POST['description'];

	// attempt to update the playlist metadata
	$playlist->update();

	// store the new thumbnail file, if any
	if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name'])){
		resize_thumb($_FILES['thumbnail']['tmp_name']);
		move_uploaded_file($_FILES['thumbnail']['tmp_name'], '/var/www/data/thumbnails/playlists/' . $pid);
	}

	// all went well, respond positively with the playlist's ID
	respond(200, [
		'pid' => $pid
	]);
} catch(Exception $e){
	// respond(500, 'Failed to edit playlist');
	respond(500, $e->getMessage());
}
