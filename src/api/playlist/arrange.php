<?php

/*
	# POST `/api/playlist/arrange.php`

	Arrange the position of a video in a playlist.  
	Only available to users with the "teacher" role that owns the playlist.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `pid` - playlist ID _(number, required)_
	- `vid` - video ID _(number, required)_
	- `direction` - either "up" or "down" _(string, required)_

	## Success response

	Returns a JSON object containing a positive message.

	```
	{
		"msg": "OK"
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

$pid = $_POST['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');
$pid = (int)$pid;

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');
$vid = (int)$vid;

$direction = $_POST['direction'];
if(!isset($direction) || empty($direction))
	respond(400, 'Missing direction');
if($direction != 'up' && $direction != 'down')
	respond(400, 'Bad direction');

try {
	// get playlist and validate ownership
	$playlist = new Playlist($dbh, $pid);
	if($playlist->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your playlist');

	// get video and validate ownership
	$video = new Video($dbh, $vid);
	if($video->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your video');

	// validate video is in the playlist
	if(!$playlist->hasVideo($vid))
		respond(400, 'Video isn\'t in the playlist');

	// move the video
	if($direction == 'up')
		$playlist->moveVideoUp($vid);
	else
		$playlist->moveVideoDown($vid);

	// respond positively
	respond(200, 'OK');
} catch(Exception $e){
	respond(400, $e->getMessage());
}
