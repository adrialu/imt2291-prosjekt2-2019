<?php

/*
	# GET `/api/playlist/get.php`

	Returns a playlist by ID.

	## Data constraints

	Takes the following URL parameters:

	- `pid` - playlist ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the playlist.

	```
	{
		"pid": Number, // playlist ID
		"title": String,
		"description": String,
		"updated": DateTime,
		"videos": Number // number of videos in the playlist
		"author": {
			"uid": Number, // user ID of the playlist's author
			"name": String
		}
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$pid = $_GET['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');

try {
	// get playlist
	$playlist = new Playlist($dbh, (int)$pid);
	$author = $playlist->getAuthor();

	// respond with playlist data
	respond(200, [
		'pid'         => (int)$pid,
		'title'       => $playlist->title,
		'description' => $playlist->description,
		'updated'     => $playlist->updated->format(DATE_ISO8601),
		'videos'      => count($playlist->getVideos()),
		'author'      => [
			'uid'  => $author->getID(),
			'name' => $author->givenName . ' ' . $author->familyName,
		]
	]);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
