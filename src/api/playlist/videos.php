<?php

/*
	# GET `/api/playlist/videos.php`

	Returns all videos in a playlist.

	## Data constraints

	Takes the following URL parameters:

	- `pid` - playlist ID _(number, required)_

	## Success response

	Returns a JSON array of objects containing information about each video.

	```
	[
		{
			"vid": Number, // video ID
			"title": String,
			"subject": String,
			"course": String,
			"description": String,
			"views": Number, // number of views
			"uploaded": DateTime,
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$pid = $_GET['pid'];
if(!isset($pid) || empty($pid))
	respond(400, 'Missing playlist ID');

try {
	// get the playlist videos
	$playlist = new Playlist($dbh, $pid);
	$videos = $playlist->getVideos();

	// create associative array
	$res = array();
	foreach($videos as $video){
		$data = [
			'vid'         => $video->getID(),
			'title'       => $video->title,
			'subject'     => $video->subject,
			'course'      => $video->course,
			'description' => $video->description,
			'views'       => $video->views,
			'uploaded'    => $video->uploaded->format(DATE_ISO8601),
		];

		array_push($res, $data);
	}

	// respond with the data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
