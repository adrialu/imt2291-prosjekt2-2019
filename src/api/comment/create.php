<?php

/*
	# POST `/api/comment/create.php`

	Create a new comment on a video.  
	Only available to logged in users.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `vid` - video ID _(number, required)_
	- `comment` - content of the comment _(string, required)_

	## Success response

	Returns a JSON object containing information about the comment.

	```
	{
		"user": {
			"uid": Number, // user ID
			"name": String
		},
		"comment": String,
		"created": DateTime
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';
require_once '/var/www/html/classes/Comment.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn())
	respond(401, 'Unauthorized access');

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

$content = $_POST['comment'];
if(!isset($content) || empty($content))
	respond(400, 'Missing comment body');
if(strlen($content) > 500)
	respond(400, 'Comment too long');
if(strlen($content) < 10)
	respond(400, 'Comment too short');

try {
	$_ = new Video($dbh, (int)$vid);
} catch(Exception $e){
	respond(400, 'Video doesn\'t exist');
}

try {
	// create the new comment
	$comment = new Comment($dbh, (int)$vid);
	$comment->create($content);

	// respond with data
	respond(200, [
		'user' => [
			'uid'  => $user->getID(),
			'name' => $user->givenName . ' ' . $user->familyName,
		],
		'comment' => $content,
		'created' => new DateTime('now', new DateTimeZone(getenv('TZ'))),
	]);
} catch(Exception $e){
	respond(500, $e->getMessage());
	// respond(500, 'Failed to authenticate');
}
