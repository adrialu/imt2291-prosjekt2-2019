<?php

/*
	# GET `/api/comment/user.php`

	Returns all comments by a user.

	## Data constraints

	Requires the following URL parameters:

	- `uid` - user ID _(number, required)_

	## Success response

	Returns a JSON array of objects containing information about the comment.

	```
	[
		{
			"user": {
				"uid": Number, // user ID
				"name": String
			},
			"comment": String,
			"created": DateTime,
			"vid": Number // video ID
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';
require_once '/var/www/html/classes/Comment.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$uid = $_GET['uid'];
if(!isset($uid) || empty($uid))
	respond(400, 'Missing user ID');

try {
	$user = new User($dbh, (int)$uid);
} catch(Exception $e){
	respond(400, 'User doesn\'t exist');
}

try {
	// get all comments for video
	$comment = new Comment($dbh);
	$comments = $comment->getAllFromUser($user);

	// create associative array
	$res = array();
	foreach($comments as $data){
		$video = $data->getVideo();

		array_push($res, [
			'user' => [
				'uid'  => $data->author->getID(),
				'name' => $data->author->givenName . ' ' . $data->author->familyName,
			],
			'comment' => $data->comment,
			'created' => $data->created,
			'video'   => [
				'vid'   => $video->getID(),
				'title' => $video->title,
			]
		]);
	}

	// respond with data
	respond(200, $res);
} catch(Exception $e){
	respond(500, $e->getMessage());
	// respond(500, 'Failed to authenticate');
}
