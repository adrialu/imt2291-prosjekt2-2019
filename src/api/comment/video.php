<?php

/*
	# GET `/api/comment/video.php`

	Returns all comments for a video.

	## Data constraints

	Requires the following URL parameters:

	- `vid` - video ID _(number, required)_

	## Success response

	Returns a JSON array of objects containing information about the comment.

	```
	[
		{
			"user": {
				"uid": Number, // user ID
				"name": String
			},
			"comment": String,
			"created": DateTime
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';
require_once '/var/www/html/classes/Comment.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$vid = $_GET['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

try {
	$_ = new Video($dbh, (int)$vid);
} catch(Exception $e){
	respond(400, 'Video doesn\'t exist');
}

try {
	// get all comments for video
	$comment = new Comment($dbh, (int)$vid);
	$comments = $comment->getAll();

	// create associative array
	$res = array();
	foreach($comments as $data){
		array_push($res, [
			'user' => [
				'uid'  => $data->author->getID(),
				'name' => $data->author->givenName . ' ' . $data->author->familyName,
			],
			'comment' => $data->comment,
			'created' => $data->created,
		]);
	}

	// respond with data
	respond(200, $res);
} catch(Exception $e){
	respond(500, $e->getMessage());
	// respond(500, 'Failed to authenticate');
}
