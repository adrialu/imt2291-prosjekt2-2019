<?php

/*
	# GET `/api/video/rate.php`

	Returns the user's rating for a video.  
	Only available to users with the "student" role.

	## Data constraints

	Requires the following URL parameters:

	- `vid` - video ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the user's rating.

	```
	{
		"vid": Number, // video ID
		"rating": Number, // between 0 and 5
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

/*
	# POST `/api/video/rate.php`

	Sets the user's rating of a video.  
	Only available to users with the "student" role.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `vid` - video ID _(number, required)_
	- `rating` - user's rating, between 0 and 5 _(number, required)_

	## Success response

	Returns a JSON object containing information about the user's rating.

	```
	{
		"vid": Number, // video ID
		"rating": Number, // between 0 and 5
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/Video.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isStudent())
	respond(401, 'Unauthorized access');

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid))
	$vid = $_GET['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

try {
	// get video
	$video = new Video($dbh, $vid);

	$rating = 0;
	if(isset($_POST['vid'])){
		// user is attempting to SET a rating
		$rating = $_POST['rating'];
		if(!isset($rating))
			respond(400, 'Missing rating');

		$rating = (int)$rating;
		if(!is_numeric($rating) || $rating < 0 || $rating > 5)
			respond(400, 'Invalid rating, must be 0-5');

		// set rating
		$video->rate($rating);
	} else {
		// user is attempting to GET a rating
		// get the video rating
		$rating = $video->getRating();
	}

	// respond with data
	respond(200, [
		'vid'    => $video->getID(),
		'rating' => $rating,
	]);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
