<?php

/*
	# POST `/api/video/create.php`

	Create a new video.  
	Only available to users with the "teacher" role.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `title` - name of the video _(string, required)_
	- `subject` - subject of the video _(string, required)_
	- `course` - course of the video _(string, required)_
	- `description` - description of the video _(string, required)_
	- `video` - the video file itself _(blob, required)_
	- `thumbnail` - the video thumbnail _(blob, optional)_
	- `caption` - caption for the video _(blob, optional)_

	**NB:** If a thumbnail is not supplied, one will be generated automatically from the video file.

	## Success response

	Returns a JSON object containing the video ID.

	```
	{
		"vid": Number // video ID
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/vendor/autoload.php';
require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

if(!isset($_FILES['video']) || empty($_FILES['video']['tmp_name']))
	respond(400, 'Missing video file');

if(!is_uploaded_file($_FILES['video']['tmp_name']))
	respond(400, 'Invalid video file');

if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name']) && !is_uploaded_file($_FILES['thumbnail']['tmp_name']))
	respond(400, 'Invalid thumbnail file');

if(isset($_FILES['caption']) && !empty($_FILES['caption']['tmp_name']) && !is_uploaded_file($_FILES['caption']['tmp_name']))
	respond(400, 'Invalid caption file');

try {
	// prepare the video object
	$video = new Video($dbh);
	$video->title = $_POST['title'];
	$video->description = $_POST['description'];
	$video->course = $_POST['course'];
	$video->subject = $_POST['subject'];

	// attempt to upload the video metadata
	$id = $video->upload();

	// video metadata uploaded, store the video and thumbnail files
	move_uploaded_file($_FILES['video']['tmp_name'], '/var/www/data/videos/' . $id);

	if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name'])){
		resize_thumb($_FILES['thumbnail']['tmp_name']);
		move_uploaded_file($_FILES['thumbnail']['tmp_name'], '/var/www/data/thumbnails/videos/' . $id);
	} else {
		// user didn't submit a thumbnail file, we'll create one
		create_thumbnail('/var/www/data/videos/' . $id, '/var/www/data/thumbnails/videos/' . $id);
	}

	if(isset($_FILES['caption']) && !empty($_FILES['caption']['tmp_name'])){
		move_uploaded_file($_FILES['caption']['tmp_name'], '/var/www/data/captions/' . $id);
	}

	// all went well, respond positively with the video's ID
	respond(201, [
		'vid' => $id
	]);
} catch(Exception $e){
	if(isset($id)){
		// remove the video
		$video->destroy();
	}

	// respond(500, 'Failed to upload video');
	respond(500, $e->getMessage());
}

/**
 * Creates a thumbnail from the video file and saves it to a path.
 * The frame chosen is 1 second into the video.
 *
 * @param string $video_path Path to a video file.
 * @param string $save_path  Path to save the thumbnail.
 */
function create_thumbnail(string $video_path, string $save_path){
	$ffmpeg = FFMpeg\FFMpeg::create();
	$video = $ffmpeg->open($video_path);
	$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(1));
	$frame->save($save_path);
}
