<?php

/*
	# GET `/api/video/get.php`

	Returns information about a single video by ID.

	## Data constraints

	Requires the following URL parameters:

	- `vid` - video ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the video.

	```
	{
		"vid": Number, // video ID
		"title": String,
		"subject": String,
		"course": String,
		"description": String,
		"rating": Number, // average rating
		"views": Number, // number of views
		"uploaded": DateTime,
		"author": {
			"uid": Number, // user ID of the video's author
			"name": String
		}
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/Video.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$vid = $_GET['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

try {
	// get video
	$video = new Video($dbh, $vid);
	$author = $video->getAuthor();

	// respond with video data
	respond(200, [
		'vid'         => $video->getID(),
		'title'       => $video->title,
		'subject'     => $video->subject,
		'course'      => $video->course,
		'description' => $video->description,
		'rating'      => $video->getAverageRating(),
		'views'       => $video->views,
		'uploaded'    => $video->uploaded,
		'author' => [
			'uid'  => $author->getID(),
			'name' => $author->givenName . ' ' . $author->familyName,
		],
	]);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
