<?php

/*
	# POST `/api/video/views.php`

	Updates the view count (increment by 1) for a video.
	Returns a video by ID.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `vid` - video ID _(number, required)_

	## Success response

	Returns a JSON object containing information about the video.

	```
	{
		"msg": "OK"
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/Video.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

try {
	// get video
	$video = new Video($dbh, $vid);

	// increase view count
	$video->increaseViewCount();

	// gotta respond with something
	respond(200, 'OK');
} catch(Exception $e){
	respond(400, $e->getMessage());
}
