<?php

/*
	# POST `/api/video/edit.php`

	Edit an existing video.  
	Only available to users with the "teacher" role and owns the video in question.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `vid` - video ID _(number, required)_
	- `title` - name of the video _(string, required)_
	- `subject` - subject of the video _(string, required)_
	- `course` - course of the video _(string, required)_
	- `description` - description of the video _(string, required)_
	- `thumbnail` - the video thumbnail _(blob, optional)_
	- `caption` - caption for the video _(blob, optional)_

	## Success response

	Returns a JSON object containing the video ID.

	```
	{
		"vid": Number // video ID
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid) || !is_numeric($vid))
	respond(400, 'Bad/missing video ID');

if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name']) && !is_uploaded_file($_FILES['thumbnail']['tmp_name']))
	respond(400, 'Invalid thumbnail file');

if(isset($_FILES['caption']) && !empty($_FILES['caption']['tmp_name']) && !is_uploaded_file($_FILES['caption']['tmp_name']))
	respond(400, 'Invalid caption file');

try {
	// prepare the video object
	$video = new Video($dbh, $vid);

	// validate the user's access to the given video
	if($video->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your video');

	// update the video object
	$video->title = $_POST['title'];
	$video->description = $_POST['description'];
	$video->course = $_POST['course'];
	$video->subject = $_POST['subject'];

	// attempt to update the video metadata
	$video->update();

	if(isset($_FILES['thumbnail']) && !empty($_FILES['thumbnail']['tmp_name'])){
		resize_thumb($_FILES['thumbnail']['tmp_name']);

		// remove existing file
		if(file_exists('/var/www/data/thumbnails/videos/' . $vid))
			unlink('/var/www/data/thumbnails/videos/' . $vid);

		move_uploaded_file($_FILES['thumbnail']['tmp_name'], '/var/www/data/thumbnails/videos/' . $vid);
	}

	if(isset($_FILES['caption']) && !empty($_FILES['caption']['tmp_name'])){
		// remove existing file
		if(file_exists('/var/www/data/captions/' . $vid))
			unlink('/var/www/data/captions/' . $vid);

		move_uploaded_file($_FILES['caption']['tmp_name'], '/var/www/data/captions/' . $vid);
	}

	// all went well, respond positively with the video's ID
	respond(201, [
		'vid' => $vid
	]);
} catch(Exception $e){
	// respond(500, 'Failed to edit video');
	respond(500, $e->getMessage());
}
