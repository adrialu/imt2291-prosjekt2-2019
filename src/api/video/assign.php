<?php

/*
	# POST `/api/playlist/assign.php`

	Assign a video to/from a playlist.  
	Only available to users with the "teacher" role and owns the video and playlist in question.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `vid` - video ID _(number, required)_
	- `pid` - playlist ID _(number, required)_
	- `option` - either "add" or "remove" to/from playlist _(string, required)_

	## Success response

	Returns a JSON object containing the video and playlist IDs.

	```
	{
		"pid": Number, // playlist ID
		"vid": Number // video ID
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if(!$user->loggedIn() || !$user->isTeacher())
	respond(401, 'Unauthorized access');

// validate parameters
$pid = $_POST['pid'];
if(!isset($pid) || empty($pid) || !is_numeric($pid))
	respond(400, 'Bad request, missing playlist ID');
$pid = (int)$pid;

$vid = $_POST['vid'];
if(!isset($vid) || empty($vid) || !is_numeric($vid))
	respond(400, 'Bad request, missing video ID');
$vid = (int)$vid;

$option = $_POST['option'];
if(!isset($option) || empty($option) || !($option == 'add' || $option == 'remove'))
	respond(400, 'Bad request, option is missing or invalid');

try {
	// get the video object to just make sure it exists
	$video = new Video($dbh, $vid);

	// validate the user's access to the given video
	if($video->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your video');
} catch(Exception $e){
	respond(400, 'Video doesn\'t exist');
}

try {
	// attempt to get the playlist by ID
	$playlist = new Playlist($dbh, $pid);

	// validate the user's access to the given playlist
	if($playlist->getAuthor()->getID() != $user->getID())
		respond(400, 'This is not your playlist');

	// validate the request
	if($option == 'add' && $playlist->hasVideo($vid))
		respond(400, 'Video already in playlist');

	if($option == 'remove' && !$playlist->hasVideo($vid))
		respond(400, 'Video not already in playlist');

	if($option == 'add' && $playlist->videoHasPlaylist($vid))
		respond(400, 'Video is already in another playlist');

	// attempt to add/remove video to/from playlist
	if($option == 'add')
		$playlist->addVideo($vid);
	else
		$playlist->removeVideo($vid);

	// all went well, respond with the new info
	respond(200, [
		'pid' => $pid,
		'vid' => $vid,
	]);
} catch(Exception $e){
	respond(500, 'Failed to ' + $option + ' video');
}
