<?php

/*
	# GET `/api/video/all.php`

	Returns all videos by the user.

	## Data constraints

	Takes the following URL parameters:

	- `uid` - user ID _(number, optional)_
		- if not supplied, the logged in user is considered

	## Success response

	Returns a JSON array of objects containing information about each video.

	```
	[
		{
			"vid": Number, // video ID
			"title": String,
			"subject": String,
			"course": String,
			"description": String,
			"rating": Number, // average rating
			"views": Number, // number of views
			"uploaded": DateTime,
			"playlist": { // playlist the video is assigned to, if any
				"pid": Number,
				"title": String,
			}
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';
require_once '/var/www/html/classes/Playlist.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$uid = $_GET['uid'];
if(isset($uid) && !empty($uid))
	$uid = (int)$uid;
else
	$uid = -1;

try {
	// get the correct user we're querying
	$user = new User($dbh, $uid);
	if($uid == -1 && !$user->isTeacher())
		respond(401, 'Unauthorized access');

	// get videos
	$video = new Video($dbh);
	$playlist = new Playlist($dbh);
	$videos = $video->getAllFromUser($user);

	// create associative array
	$res = array();
	foreach($videos as $video){
		$vid = $video->getID();
		$data = [
			'vid'         => $vid,
			'title'       => $video->title,
			'subject'     => $video->subject,
			'course'      => $video->course,
			'description' => $video->description,
			'rating'      => $video->getAverageRating(),
			'views'       => $video->views,
			'uploaded'    => $video->uploaded->format(DATE_ISO8601),
		];

		// if the video is in a playlist, add some extra metadata
		if($playlist->videoHasPlaylist($vid)){
			$playlistInfo = $playlist->getVideoPlaylist($vid);

			$data['playlist'] = [
				'pid'   => $playlistInfo->getID(),
				'title' => $playlistInfo->title,
			];
		}

		array_push($res, $data);
	}

	// respond with the data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
