<?php

/*
	# GET `/api/video/stream.php`

	Returns a video by ID.

	## Data constraints

	Requires the following URL parameters:

	- `vid` - video ID _(number, required)_

	## Success response

	The video blob.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';

$vid = $_GET['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

$path = '/var/www/data/videos/' . $vid;
if(!file_exists($path))
	respond(400, 'Video doesn\'t exist');

// serve the file
header('Content-Type: ' . mime_content_type($path));
header('Content-Disposition: attachment; filename="' . $vid . '"');
readfile($path);
