<?php

/*
	# GET `/api/video/caption.php`

	Returns caption (subtitles) for the video, if one has been uploaded.

	## Data constraints

	Requires the following URL parameters:

	- `vid` - video ID _(number, required)_

	## Success response

	The caption blob.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';

$vid = $_GET['vid'];
if(!isset($vid) || empty($vid))
	respond(400, 'Missing video ID');

$path = '/var/www/data/captions/' . $vid;
if(!file_exists($path))
	respond(400, 'Caption doesn\'t exist');

// serve the file
header('Content-Type: text/vtt');
header('Content-Disposition: attachment; filename="' . $vid . '"');
readfile($path);
