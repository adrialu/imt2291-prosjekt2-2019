<?php

/*
	# GET `/api/user/get.php`

	Returns information about a user.  

	## Data constraints

	Requires the following URL parameters:

	- `uid` - user ID _(number, optional)_

	**NB**: if the user ID is not provided, the logged in user is considered, if any.

	## Success response

	Returns a JSON object containing information about the user.

	If the user ID parameter is not provided and the user is not logged in, the following is returned:
	```
	{
		"loggedIn": false
	}
	```

	If the user ID parameter is not provided and the user **is** logged in, the following is returned:
	```
	{
		"uid": Number, // user ID
		"name": String,
		"email": String,
		"type": String, // the user's role, "student", "teacher" or "admin"
		"loggedIn": true
	}
	```

	If the user ID parameter is provided, the following is returned:
	```
	{
		"uid": Number, // user ID
		"name": String,
		"email": String,
		"type": String, // the user's role, "student", "teacher" or "admin"
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

if(!isset($_GET['uid']) || empty($_GET['uid'])){
	// get session user info
	try {
		$user = new User($dbh);
		if(!$user->loggedIn())
			respond(200, [
				'loggedIn' => false,
			]);

		respond(200, [
			'uid'      => $user->getID(),
			'name'     => $user->givenName . ' ' . $user->familyName,
			'email'    => $user->email,
			'type'     => $user->getType(),
			'loggedIn' => true,
		]);
	} catch(Exception $e){
		respond(400, $e->getMessage());
	}
} else {
	// get given user info
	$uid = $_GET['uid'];
	try {
		$user = new User($dbh, $uid);
		respond(200, [
			'uid'   => $user->getID(),
			'name'  => $user->givenName . ' ' . $user->familyName,
			'email' => $user->email,
			'type'  => $user->getType()
		]);
	} catch(Exception $e){
		respond(400, $e->getMessage());
	}
}
