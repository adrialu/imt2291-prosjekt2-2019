<?php

/*
	# POST `/api/user/logout.php`

	Attempt to log the user out.  

	## Success response

	Returns a JSON object containing information about the user.

	```
	{
		"msg": "Logged out"
	}
	```

	**NB**: When the logout is successful, the session cookie is destroyed.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/User.php';

// log the user out
User::logout();

// respond
respond(200, 'Logged out');
