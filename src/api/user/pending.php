<?php

/*
	# GET `/api/user/pending.php`

	Returns information about all users pending promotion to the "teacher" role.  
	Only available to users with the "admin" role.

	## Success response

	Returns a JSON array of objects containing information about each user.

	```
	[
		{
			"uid": Number, // user ID
			"name": String,
			"type": String, // the user's role, "student", "teacher" or "admin"
			"email": String
		},
		...
	}
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

try {
	// only admins can query pending users
	$user = new User($dbh);
	if(!$user->isAdmin())
		respond(401, 'Unauthorized access');

	// get all users
	$users = $user->getAll();

	// create associative array of pending users
	$res = array();
	foreach($users as $user){
		if($user->isRequesting())
			array_push($res, [
				'uid'   => $user->getID(),
				'name'  => $user->givenName . ' ' . $user->familyName,
				'email' => $user->email,
				'type'  => $user->getType(),
			]);
	}

	// respond with the user data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
