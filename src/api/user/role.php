<?php

/*
	# POST `/api/user/role.php`

	Sets the role for a given user, clearing any pending status.  
	Only available to users with the "admin" role.

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `uid` - user ID _(number, required)_
	- `type` - the new role, "student", "teacher" or "admin" _(string, required)_

	**NB**: users can't change their own role.

	## Success response

	Returns a JSON object containing information about the new user.

	```
	{
		"uid": Number, // user ID
		"type": String // the user's role, "student", "teacher" or "admin"
	}
	```

	**NB**: When the registration is successful, a session cookie is created.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$userID = $_POST['uid'];
if(!isset($userID) || empty($userID) || !is_numeric($userID))
	respond(400, 'Bad/missing user ID');
$userID = (int)$userID;

$user = new User($dbh);
if($userID == $user->getID())
	respond(400, 'You can\'t change your own type');

$userType = $_POST['type'];
if(!isset($userType) || empty($userType))
	respond(400, 'Missing user type');

$userTypes = [
	'student' => UserType::Student,
	'teacher' => UserType::Teacher,
	'admin'   => UserType::Admin
];

if(!in_array($userType, $userTypes))
	respond(400, 'Bad user type');

try {
	// only admins can change a user's role
	if(!$user->loggedIn() || !$user->isAdmin())
		respond(401, 'Unauthorized access');

	// get the user to target the update on
	$targetUser = new User($dbh, $userID);
	$targetType = $userTypes[$userType];

	// attempt to update the user
	$user->changeUserType($targetUser, $targetType);

	// all went well, respond with the updated values
	respond(200, [
		'uid'  => $userID,
		'type' => $userType,
	]);
} catch(Exception $e){
	respond(500, 'Failed to change user');
}
