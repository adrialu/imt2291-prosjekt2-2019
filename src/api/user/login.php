<?php

/*
	# POST `/api/user/login.php`

	Attempt to log the user in.  

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `email` - email of the user _(string, required)_
	- `password` - password of the user _(string, required)_

	## Success response

	Returns a JSON object containing information about the user.

	```
	{
		"uid": Number, // user ID
		"name": String,
		"type": String // the user's role, "student", "teacher" or "admin"
	}
	```

	**NB**: When the login is successful, a session cookie is created.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if($user->loggedIn())
	respond(401, 'Unauthorized access');

if(!isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['password']) || empty($_POST['password']))
	respond(400, 'Missing username and/or password');

try {
	// attempt to log user in
	$user->login($_POST['email'], $_POST['password']);

	// all went well, respond with user data
	respond(200, [
		'uid'  => $user->getID(),
		'name' => $user->givenName . ' ' . $user->familyName,
		'type' => $user->getType(),
	]);
} catch(Exception $e){
	respond(500, $e->getMessage());
	// respond(500, 'Failed to authenticate');
}
