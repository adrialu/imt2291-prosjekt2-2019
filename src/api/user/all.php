<?php

/*
	# GET `/api/user/all.php`

	Returns information about all registered users.  
	Only available to users with the "admin" role.

	## Success response

	Returns a JSON array of objects containing information about each user.

	```
	[
		{
			"uid": Number, // user ID
			"name": String,
			"type": String, // the user's role, "student", "teacher" or "admin"
			"email": String
		},
		...
	]
	```

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

try {
	// only admins can query all users
	$user = new User($dbh);
	if(!$user->isAdmin())
		respond(401, 'Unauthorized access');

	// get users
	$users = $user->getAll();

	// create associative array
	$res = array();
	foreach($users as $user){
		array_push($res, [
			'uid'   => $user->getID(),
			'name'  => $user->givenName . ' ' . $user->familyName,
			'type'  => $user->getType(),
			'email' => $user->email,
		]);
	}

	// respond with the user data
	respond(200, $res);
} catch(Exception $e){
	respond(400, $e->getMessage());
}
