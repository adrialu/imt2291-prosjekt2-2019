<?php

/*
	# POST `/api/user/register.php`

	Attempt to log the user in.  

	## Data constraints

	Requires a form data object (content-type "multipart/form-data"), with the following values:

	- `givenName` - first name _(string, required)_
	- `familyName` - last name _(string, required)_
	- `email` - email address _(string, required)_
	- `password` - desired password _(string, required)_
	- `teacher` - if the user is a teacher _(boolean, optional)_

	## Success response

	Returns a JSON object containing information about the new user.

	```
	{
		"uid": Number, // user ID
		"name": String,
		"type": String // the user's role, "student", "teacher" or "admin"
	}
	```

	**NB**: When the registration is successful, a session cookie is created.

	# Failure response

	Returns the error message on failure, with the failing reason.
*/

require_once '/var/www/html/api/cors.php';
require_once '/var/www/html/classes/Utils.php';
require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

try {
	$dbh = DB::getConnection();
} catch(Exception $e){
	respond(500, 'Something went wrong');
}

$user = new User($dbh);
if($user->loggedIn())
	respond(401, 'Unauthorized access');

if(!isset($_POST['givenName']) || empty($_POST['givenName']))
	respond(400, 'Missing first name');
if(!isset($_POST['familyName']) || empty($_POST['familyName']))
	respond(400, 'Missing last name');
if(!isset($_POST['email']) || empty($_POST['email']))
	respond(400, 'Missing email');
if(!isset($_POST['password']) || empty($_POST['password']))
	respond(400, 'Missing password');

try {
	// prepare the query
	$user->givenName = $_POST['givenName'];
	$user->familyName = $_POST['familyName'];
	$user->email = $_POST['email'];
	$user->password = $_POST['password'];
	$user->requesting = $_POST['teacher'];

	// attempt to register user
	$user->register();

	// all went well, respond with user data
	respond(200, [
		'uid'  => $user->getID(),
		'name' => $user->givenName . ' ' . $user->familyName,
		'type' => $user->getType(),
	]);
} catch(Exception $e){
	respond(500, $e->getMessage());
	// respond(500, 'Failed to authenticate');
}
