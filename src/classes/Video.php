<?php

require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';

/**
 * Class that holds information about a video.
 *
 * Think of this as a struct, as PHP doesn't have those.
 */
class VideoInfo {
	// these are strings (why the hell doesn't PHP allow type enforcing?).
	public $title       = '';
	public $description = '';
	public $course      = '';
	public $subject     = '';

	// these are integers
	public $views = 0;
	public $comments = 0;

	// these are instances of the DateTime class
	public $uploaded = null;
}

/**
 * Functional class that handles videos, ratings and comments.
 * Extends the VideoInfo to use the same variables.
 *
 * Note: the variables are public, but they _should_ be private. Meh.
 */
class Video extends VideoInfo {
	// these are instances of the PDO class.
	private $dbh = null;

	// these are instances of the User class.
	protected $user = null;

	// these are integers.
	protected $vid = -1;

	/**
	 *
	 * @param  PDO       $dbh Existing PDO connection.
	 * @param  int       $vid (optional) Existing video ID to fetch data for.
	 * @throws Exception      If the database connection/query failed.
	 */
	function __construct(PDO $dbh, int $vid = -1){
		$this->dbh = $dbh;
		$this->vid = $vid;

		// grab the sessioned user
		$this->user = new User($dbh);

		if($vid != -1){
			// refresh the data for the video
			$this->refresh();
		}
	}

	/**
	 * Searches for a video based on the provided query.
	 * This will match against the title, description, course, and subject.
	 *
	 * @param  string $query Partial/full information about a video's info.
	 * @return array         Indexed array of Video objects that matched the query, in no particular order.
	 * $throws Exception     If the database query failed.
	 */
	public function search(string $q){
		$stmt = "SELECT id FROM videos WHERE title || ' ' || descr || ' ' || course || ' ' || subject ILIKE '%' || ? || '%'";
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $q, PDO::PARAM_STR);
		$query->execute();

		$videos = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($videos, new Video($this->dbh, $row['id']));
		}

		return $videos;
	}

	/**
	 * Uploads a new video.
	 *
	 * @return int       The video's ID.
	 * @throws Exception If the database query failed, or if something is missing/incorrect about the video info.
	 */
	public function upload(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can upload new videos', 1);

		$this->title = trim($this->title);
		if(empty($this->title) || strlen($this->title) < 10 || strlen($this->title) > 50)
			throw new Exception('Title is either empty or out of bounds');

		$this->description = trim($this->description);
		if(empty($this->description) || strlen($this->description) < 10 || strlen($this->description) > 500)
			throw new Exception('Description is either empty or out of bounds');

		$this->course = trim($this->course);
		if(empty($this->course) || strlen($this->course) < 3 || strlen($this->course) > 50)
			throw new Exception('Course is either empty or out of bounds');

		$this->subject = trim($this->subject);
		if(empty($this->subject) || strlen($this->subject) < 10 || strlen($this->subject) > 50)
			throw new Exception('Subject is either empty or out of bounds');

		$stmt = 'INSERT INTO videos (title, descr, course, subject, userid) VALUES (?, ?, ?, ?, ?)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->title, PDO::PARAM_STR);
		$query->bindValue(2, $this->description, PDO::PARAM_STR);
		$query->bindValue(3, $this->course, PDO::PARAM_STR);
		$query->bindValue(4, $this->subject, PDO::PARAM_STR);
		$query->bindValue(5, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();

		$this->vid = $this->dbh->lastInsertId();
		return $this->vid;
	}

	/**
	 * Updates an existing video with the current data.
	 *
	 * @throws Exception If the database query failed, or if something is missing/incorrect about the video info.
	 */
	public function update(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can edit videos', 1);

		$this->title = trim($this->title);
		if(empty($this->title) || strlen($this->title) < 10 || strlen($this->title) > 50)
			throw new Exception('Title is either empty or out of bounds');

		$this->description = trim($this->description);
		if(empty($this->description) || strlen($this->description) < 10 || strlen($this->description) > 500)
			throw new Exception('Description is either empty or out of bounds');

		$this->course = trim($this->course);
		if(empty($this->course) || strlen($this->course) < 3 || strlen($this->course) > 50)
			throw new Exception('Course is either empty or out of bounds');

		$this->subject = trim($this->subject);
		if(empty($this->subject) || strlen($this->subject) < 10 || strlen($this->subject) > 50)
			throw new Exception('Subject is either empty or out of bounds');

		$stmt = 'UPDATE videos SET title=?, descr=?, course=?, subject=? WHERE id=?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->title, PDO::PARAM_STR);
		$query->bindValue(2, $this->description, PDO::PARAM_STR);
		$query->bindValue(3, $this->course, PDO::PARAM_STR);
		$query->bindValue(4, $this->subject, PDO::PARAM_STR);
		$query->bindValue(5, $this->vid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Deletes a video.
	 *
	 * @throws Exception If the database query failed, or if the user has no access to video.
	 */
	public function destroy(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify videos', 1);

		$stmt = 'DELETE FROM videos WHERE id=?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Returns the video's mime type
	 *
	 * @return string $mime The mime type, e.g. 'video/mp4'.
	 */
	public function getMime(){
		return mime_content_type('/var/www/data/videos/' . $this->vid);
	}

	/**
	 * Returns the video's ID.
	 *
	 * @return int The video's ID.
	 */
	public function getID(){
		return $this->vid;
	}

	/**
	 * Returns the video's author.
	 *
	 * @return User      $author Instance of the User class, representing the author.
	 * @throws Exception         If the video doesn't exist or the query failed.
	 */
	public function getAuthor(){
		$stmt = 'SELECT userid FROM videos WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Video doesn\'t exist', 1);
		else
			return new User($this->dbh, (int)$row['userid']);
	}

	/**
	 * Returns all the videos by the (given) user, sorted by newest first.
	 *
	 * @param  User      $author User object to query for (optional).
	 * @return array             An array of Video objects, sorted by newest comment first.
	 * @throws Exception         If the database query failed.
	 */
	public function getAllFromUser(User $author = null){
		$stmt = 'SELECT id FROM videos WHERE userid = ? ORDER BY uploaded DESC';
		$query = $this->dbh->prepare($stmt);

		if(isset($author))
			$query->bindValue(1, $author->getID(), PDO::PARAM_INT);
		else
			$query->bindValue(1, $this->user->getID(), PDO::PARAM_INT);

		$query->execute();

		// TODO: pagination

		$videos = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($videos, new Video($this->dbh, $row['id']));
		}

		return $videos;
	}

	/**
	 * Increases the view count by one.
	 *
	 * @trows Exception If the query failed.
	 */
	public function increaseViewCount(){
		$stmt = 'UPDATE videos SET views = views + 1 WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();

		$this->views = $this->views + 1;
	}

	/**
	 * Sets a student user's rating of the video.
	 *
	 * @param  int       $score A number between 1 and 5. To remove the user's rating of the video, supply the value 0.
	 * @throws Exception        If the database query failed, or if the user doesn't exist or is not a student.
	 */
	public function rate(int $score){
		if(!$this->user->isStudent())
			throw new Exception('Only students can rate videos', 1);

		if($score > 5 || $score < 0)
			throw new Exception('Score is out of bounds', 2);

		$stmt = 'INSERT INTO video_rating(rating, videoid, userid) VALUES(?, ?, ?) ON CONFLICT(videoid, userid) DO UPDATE SET rating = excluded.rating';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $score, PDO::PARAM_INT);
		$query->bindValue(2, $this->vid, PDO::PARAM_INT);
		$query->bindValue(3, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Returns the average rating of the video.
	 *
	 * @return float     The average score of the video, between 1 and 5, or 0 if the video hasn't been rated yet.
	 * @throws Exception If the database query failed.
	 */
	public function getAverageRating(){
		$stmt = 'SELECT AVG(rating) FROM video_rating WHERE videoid = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if($row && $row['avg'])
			return (float)$row['avg'];
		else
			return (float)0; // in case there are no ratings yet, return no rating (0)
	}

	/**
	 * Returns the user's rating of the video.
	 *
	 * @return int       The user's rating of the video, between 1 and 5, or 0 if the user hasn't rated the video yet.
	 * @throws Exception If the database query failed or the user isn't a student.
	 */
	public function getRating(){
		if(!$this->user->isStudent())
			throw new Exception('Only students can have ratings', 1);

		$stmt = 'SELECT rating FROM video_rating WHERE videoid = ? AND userid = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->bindValue(2, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if($row)
			return $row['rating'];
		else
			return 0; // in case there are no ratings yet, return no rating (0)
	}

	/**
	 * Refreshes the video metadata from the database.
	 *
	 * @throws Exception If the database query failed.
	 */
	private function refresh(){
		$stmt = 'SELECT videos.*, COUNT(video_comments.id)
				 FROM videos
				 LEFT JOIN video_comments
				 ON videos.id = video_comments.videoid
				 WHERE videos.id = ?
				 GROUP BY videos.id';

		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Video doesn\'t exist', 1);
		else {
			// update info
			$this->title       = $row['title'];
			$this->description = $row['descr'];
			$this->course      = $row['course'];
			$this->subject     = $row['subject'];
			$this->views       = $row['views'];
			$this->comments    = $row['count'];

			$this->uploaded = new DateTime($row['uploaded']);
		}
	}
}
