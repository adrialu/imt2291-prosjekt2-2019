<?php

/**
 * A database class, used to initiate a connection to a MySQL server.
 */
class DB {
	private static $db = null;
	private $dbh = null;

	// these variables should never be published to Git.
	private const DSN = "pgsql:host=postgres;port=5432;dbname=%s;user=%s;password=%s";

	/**
	 * Creates a PDO connection.
	 *
	 * @throws Exception if the 'database.ini' file is missing or it can't connect.
	 */
	function __construct(){
		// connect to postgres
		$this->dbh = new PDO(sprintf(self::DSN, getenv('DB_NAME'), getenv('DB_USER'), getenv('DB_PASS')));
		$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * Creates a new PDO connection.
	 *
	 * @return PDO          A new connected PDO object.
	 * @throws PDOException If the connection failed.
	 */
	public static function getConnection(){
		if(self::$db == null){
			self::$db = new self();
		}

		return self::$db->dbh;
	}
}
