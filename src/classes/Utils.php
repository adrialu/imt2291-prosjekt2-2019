<?php

require_once '/var/www/html/vendor/autoload.php';

/**
 * Ends the request by response code and message.
 *
 * @param int          $code HTTP status code.
 * @param string|array $msg  Response body, either message or array (to JSON).
 */
function respond(int $code, $data){
	http_response_code($code);

	if($code >= 400){
		header('Content-Type: text/plain; charset=utf-8');
		echo $data; // which is a simple message
	} else {
		if(!is_array($data))
			if(!is_string($data))
				throw new Exception('you done fucked up son');
			else
				$data = ['msg' => $data];

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($data);
	}
	die();
}

/**
 * Redirects the user to the given page.
 *
 * @param string $path Path to redirect to, defaults to root.
 */
function redirect(string $path = '/'){
	header("Location: $path", true, 302);
	die();
}

/**
 * Resizes a thumbail to 1280x720, without preserving aspect ratio.
 *
 * @param string $path   Path to the image file.
 */
function resize_thumb(string $path){
	$image = new \claviska\SimpleImage();
	$image->fromFile($path);
	$image->resize(1280, 720);
	$image->toFile($path, 'image/png');
}
