<?php

require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';

/**
 * Class that holds information about a video.
 *
 * Think of this as a struct, as PHP doesn't have those.
 */
class PlaylistInfo {
	// these are strings (why the hell doesn't PHP allow type enforcing?).
	public $title       = '';
	public $description = '';

	// these are instances of the DateTime class
	public $updated = null;
}

/**
 * Functional class that handles playlists.
 * Extends the PlaylistInfo to use the same variables.
 *
 * Note: the variables are public, but they _should_ be private. Meh.
 */
class Playlist extends PlaylistInfo {
	// these are instances of the PDO class.
	private $dbh = null;

	// these are instances of the User class.
	protected $user = null;

	// these are integers.
	protected $pid = -1;

	// these are arrays
	protected $videos = [];

	/**
	 *
	 * @param  PDO $dbh  Existing PDO connection.
	 * @param  int $id   (optional) Existing playlist ID to fetch data for.
	 * @throws Exception If the database connection/query failed.
	 */
	function __construct(PDO $dbh, int $pid = -1){
		$this->dbh = $dbh;
		$this->pid = $pid;

		// grab the sessioned user
		$this->user = new User($dbh);

		if($pid != -1){
			// refresh the data for the playlist
			$this->refresh();
		}
	}

	/**
	 * Searches for a playlist based on the provided query.
	 * This will match against the title and description.
	 *
	 * @param  string $query Partial/full information about a playlist's info.
	 * @return array         Indexed array of User objects that matched the query.
	 */
	public function search(string $q){
		$stmt = "SELECT id FROM playlists WHERE title || ' ' || descr ILIKE '%' || ? || '%'";
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $q, PDO::PARAM_STR);
		$query->execute();

		$playlists = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($playlists, new Playlist($this->dbh, $row['id']));
		}

		return $playlists;
	}

	/**
	 * Creates a new playlist.
	 *
	 * @return int       The playlist's ID.
	 * @throws Exception If the database query failed, or if the current user is not a teacher.
	 */
	public function create(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can create playlists', 1);

		$this->title = trim($this->title);
		if(empty($this->title) || strlen($this->title) < 10 || strlen($this->title) > 50)
			throw new Exception('Title is either empty or out of bounds');

		$this->description = trim($this->description);
		if(empty($this->description) || strlen($this->description) < 10 || strlen($this->description) > 500)
			throw new Exception('Description is either empty or out of bounds');

		$stmt = 'INSERT INTO playlists (title, descr, videos, userid) VALUES (?, ?, ?, ?)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->title, PDO::PARAM_STR);
		$query->bindValue(2, $this->description, PDO::PARAM_STR);
		$query->bindValue(3, '{}', PDO::PARAM_STR); // empty "array"
		$query->bindValue(4, $this->user->getID(), PDO::PARAM_STR);
		$query->execute();

		$this->pid = $this->dbh->lastInsertId();
		return $this->pid;
	}

	/**
	 * Updates an existing playlist with the current data.
	 *
	 * @throws Exception If the database query failed, or if something is missing/incorrect about the video info.
	 */
	public function update(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		if($this->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only modify their own playlists', 2);

		$this->title = trim($this->title);
		if(empty($this->title) || strlen($this->title) < 10 || strlen($this->title) > 50)
			throw new Exception('Title is either empty or out of bounds');

		$this->description = trim($this->description);
		if(empty($this->description) || strlen($this->description) < 10 || strlen($this->description) > 500)
			throw new Exception('Description is either empty or out of bounds');

		$stmt = 'UPDATE playlists SET title=?, descr=? WHERE id=?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->title, PDO::PARAM_STR);
		$query->bindValue(2, $this->description, PDO::PARAM_STR);
		$query->bindValue(3, $this->getID(), PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Deletes a playlist.
	 *
	 * @throws Exception If the database query failed, or if the user has no access to playlist.
	 */
	public function destroy(){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		$stmt = 'DELETE FROM playlists WHERE id=?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Returns the playlist's ID.
	 *
	 * @return int The playlist's ID.
	 */
	public function getID(){
		return $this->pid;
	}

	/**
	 * Returns the playlist's author.
	 *
	 * @return User      $author Instance of the User class, representing the author.
	 * @throws Exception         If the playlist doesn't exist or the query failed.
	 */
	public function getAuthor(){
		$stmt = 'SELECT userid FROM playlists WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Playlist doesn\'t exist', 1);
		else
			return new User($this->dbh, (int)$row['userid']);
	}

	/**
	 * Returns all the playlists by the (given) user, sorted by most recently first.
	 *
	 * @param  User      $author User object to query for (optional).
	 * @return array             An array of Playlist objects.
	 * @throws Exception         If the database query failed.
	 */
	public function getAllFromUser(User $author = null){
		$stmt = 'SELECT id FROM playlists WHERE userid = ? ORDER BY updated DESC';
		$query = $this->dbh->prepare($stmt);

		if(isset($author))
			$query->bindValue(1, $author->getID(), PDO::PARAM_INT);
		else
			$query->bindValue(1, $this->user->getID(), PDO::PARAM_INT);

		$query->execute();

		// TODO: pagination

		$playlists = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($playlists, new Playlist($this->dbh, $row['id']));
		}

		return $playlists;
	}

	/**
	 * Returns all the videos in the playlist.
	 *
	 * @return array     An array of Video objects.
	 * @throws Exception If the database query failed.
	 */
	public function getVideos(){
		$videos = array();
		foreach($this->videos as $videoID){
			array_push($videos, new Video($this->dbh, $videoID));
		}
		return $videos;
	}

	/**
	 * Checks whether the video is present in the playlist or not.
	 *
	 * @param  int $videoID The video to check is in the playlist or not.
	 * @return bool         Whether the video is in the playlist or not.
	 */
	public function hasVideo(int $videoID){
		return (($index = array_search($videoID, $this->videos)) !== false);
	}

	/**
	 * Adds a video to the playlist.
	 *
	 * @param  int       $videoID The ID of the video to add.
	 * @throws Exception          If the database query failed, or the current user is not a teacher.
	 */
	public function addVideo(int $videoID){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		if($this->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only modify their own playlists', 2);

		$video = new Video($this->dbh, $videoID);
		if($video->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only add videos they\'ve uploaded', 3);

		// add the video to the top, that way it shows up as "most recent"
		array_unshift($this->videos, $videoID);

		// convert the array to postgres array syntax
		$videos = '{' . implode(',', $this->videos) . '}';

		// update the database
		$stmt = 'UPDATE playlists SET videos = ?, updated = NOW() WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videos, PDO::PARAM_STR);
		$query->bindValue(2, $this->pid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Removes a video from the playlist.
	 *
	 * @param  int       $videoID The ID of the video to remove.
	 * @return
	 * @throws Exception          If the database query failed, the current user is not a teacher, or the video didn't exist in the playlist already.
	 */
	public function removeVideo(int $videoID){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		if($this->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only modify their own playlists', 2);

		// remove the video from the array
		if(($index = array_search($videoID, $this->videos)) !== false)
			unset($this->videos[$index]);
		else
			throw new Exception('Video is not in playlist');

		// convert the array to postgres array syntax
		$videos = '{' . implode(',', $this->videos) . '}';

		// update the database
		$stmt = 'UPDATE playlists SET videos = ? WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videos, PDO::PARAM_STR);
		$query->bindValue(2, $this->pid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Checks whether or not the video is already in a playlist.
	 *
	 * @param  int       $videoID ID of the video to check.
	 * @return boolean            Whether or not the video is in a playlist.
	 * @throws Exception          If the database query failed.
	 */
	public function videoHasPlaylist(int $videoID){
		$stmt = 'SELECT id FROM playlists WHERE ? = ANY(videos)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videoID, PDO::PARAM_INT);
		$query->execute();

		return !!$query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Checks the playlist the video is in.
	 *
	 * @param  int       $videoID ID of the video to check.
	 * @return Playlist           Playlist (object) the video is in.
	 * @throws Exception          If the database query failed, or if the video is not in a playlist.
	 */
	public function getVideoPlaylist(int $videoID){
		$stmt = 'SELECT id FROM playlists WHERE ? = ANY(videos)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videoID, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Video is not in a playlist', 1);
		else
			return new Playlist($this->dbh, (int)$row['id']);
	}

	/**
	 * Moves the given video up a slot in the playlist.
	 *
	 * @param  int       $videoID ID of the video to move.
	 * @throws Exception          If the database query failed, the video is not in the playlist, or if the movement was out of bounds.
	 */
	public function moveVideoUp(int $videoID){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		if($this->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only modify their own playlists', 2);

		$temp = $this->moveArrayValue($this->videos, $videoID, -1);
		$this->videos = $temp;

		// convert the array to postgres array syntax
		$videos = '{' . implode(',', $this->videos) . '}';

		// update the database
		$stmt = 'UPDATE playlists SET videos = ? WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videos, PDO::PARAM_STR);
		$query->bindValue(2, $this->pid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Moves the given video down a slot in the playlist.
	 *
	 * @param  int       $videoID ID of the video to move.
	 * @throws Exception          If the database query failed, the video is not in the playlist, or if the movement was out of bounds.
	 */
	public function moveVideoDown(int $videoID){
		if(!$this->user->isTeacher())
			throw new Exception('Only teachers can modify playlists', 1);

		if($this->getAuthor()->getID() != $this->user->getID())
			throw new Exception('Teachers can only modify their own playlists', 2);

		$temp = $this->moveArrayValue($this->videos, $videoID, 1);
		$this->videos = $temp;

		// convert the array to postgres array syntax
		$videos = '{' . implode(',', $this->videos) . '}';

		// update the database
		$stmt = 'UPDATE playlists SET videos = ? WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $videos, PDO::PARAM_STR);
		$query->bindValue(2, $this->pid, PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Moves the value of an array up or down in the array.
	 *
	 * @param  array     $arr   Array the value will be moved in.
	 * @param            $value Value to move.
	 * @param  int       $delta Delta to move, -1 for up, 1 for down.
	 * @return array            The resulting array.
	 * @throws Exception        If any of the parameters are invalid, or if $value will break bounds.
	 */
	private function moveArrayValue(array $arr, $value, int $delta){
		if(!($delta == 1 || $delta == -1))
			throw new Exception('Delta must be positive or negative 1', 1);

		if(empty($arr))
			throw new Exception('Array is empty', 2);

		if(($index = array_search($value, $arr)) === false)
			throw new Exception('Key doesn\'t exist in array', 3);
		else {
			if($delta == -1 && $index == 0)
				throw new Exception('Key is already the first index', 4);
			elseif($delta == 1 && $index == (sizeof($arr) - 1))
				throw new Exception('Key is already the last index', 5);

			array_splice($arr, $index + $delta, 0, array_splice($arr, $index, 1));
			return $arr;
		}
	}

	/**
	 * Refreshes the playlist metadata from the database.
	 *
	 * @throws Exception If the database query failed.
	 */
	private function refresh(){
		$stmt = 'SELECT userid, title, descr, updated, array_to_json(videos) AS videos FROM playlists WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Playlist doesn\'t exist', 1);
		else {
			// update info
			$this->title       = $row['title'];
			$this->description = $row['descr'];
			$this->videos      = json_decode($row['videos']);
			$this->updated     = new DateTime($row['updated']);
		}
	}

	/**
	 * Subscribes the current user to the playlist.
	 *
	 * @throws Exception If the database query failed, the user doesn't exist, the user is not a student, or if the user is already subscribed.
	 */
	public function subscribe(){
		if(!$this->user->isStudent())
			throw new Exception('Only students can subscribe to playlists', 1);

		$stmt = 'INSERT INTO playlist_subscriptions (playlistid, userid) VALUES (?, ?)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->bindValue(2, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Unsubscribes the current user from the playlist.
	 *
	 * @throws Exception If the database query failed, the user doesn't exist, the user is not a student, or if the user isn't already subscribed.
	 */
	public function unsubscribe(){
		if(!$this->user->isStudent())
			throw new Exception('Only students can unsubscribe from playlists', 1);

		if(!$this->isSubscribed())
			throw new Exception('User isn\'t subscribed to this playlist', 2);

		$stmt = 'DELETE FROM playlist_subscriptions WHERE playlistid = ? AND userid = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->bindValue(2, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();
	}

	/**
	 * Checks if the user is subscribed to the playlist.
	 *
	 * @return bool      $subscribed If the user is subscribed or not.
	 * @throws Exception             If the database query failed.
	 */
	public function isSubscribed(){
		if(!$this->user->isStudent())
			throw new Exception('Only students can be subscribed to playlists', 1);

		$stmt = 'SELECT * FROM playlist_subscriptions WHERE playlistid = ? AND userid = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->pid, PDO::PARAM_INT);
		$query->bindValue(2, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();

		return !!$query->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Returns a list of playlists the user is subscribed to.
	 *
	 * @return array     $playlists A list of Playlist objects that the user is subscribed to.
	 * @throws Exception            If the database query failed, the user doesn't exist, the user is not a student.
	 */
	public function getSubscriptions(){
		if(!$this->user->isStudent())
			throw new Exception('Only students have subscriptions', 1);

		$stmt = 'SELECT playlistid
				 FROM playlist_subscriptions AS subs
				 LEFT JOIN playlists
				 ON playlists.id = playlistid
				 WHERE subs.userid = ?
				 ORDER BY playlists.updated DESC';

		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->user->getID(), PDO::PARAM_INT);
		$query->execute();

		$playlists = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($playlists, new Playlist($this->dbh, $row['playlistid']));
		}

		return $playlists;
	}
}
