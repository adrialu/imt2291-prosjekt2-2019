<?php

require_once '/var/www/html/classes/DB.php';
require_once '/var/www/html/classes/User.php';
require_once '/var/www/html/classes/Video.php';

/**
 * Class that holds information about a video's comment.
 *
 * Think of this as a struct, as PHP doesn't have those.
 */
class CommentInfo {
	// these are strings.
	public $comment = '';

	// these are integers.
	public $created = -1;

	// these are instances of the User class.
	public $author = null;
}

/**
 * Functional class that handles videos, ratings and comments.
 * Extends the VideoInfo abstract to use the same variables.
 *
 * Note: the variables are public, but they _should_ be private. Meh.
 */
class Comment extends CommentInfo {
	// these are instances of the PDO class.
	private $dbh = null;

	// these are instances of the User class.
	protected $user = null;

	// these are integers.
	protected $cid = -1;
	protected $vid = -1;

	/**
	 *
	 * @param  PDO       $dbh Existing PDO connection.
	 * @param  int       $vid (optional) Existing video ID the comment belongs to.
	 * @param  int       $cid (optional) Existing comment ID to fetch data for.
	 * @throws Exception      If the database connection/query failed.
	 */
	function __construct(PDO $dbh, int $vid = -1, int $cid = -1){
		$this->dbh = $dbh;
		$this->vid = $vid;
		$this->cid = $cid;

		// grab the sessioned user
		$this->user = new User($dbh);

		if($cid != -1){
			// refresh the data for the comment
			$this->refresh();
		}
	}

	/**
	 * Returns all the comments for the video, sorted by newest first.
	 *
	 * @return array     An array of CommentInfo objects, sorted by newest comment first.
	 * @throws Exception If the database query failed.
	 */
	public function getAll(){
		if($this->vid == -1)
			throw new Exception('No video ID context', 1);

		$stmt = 'SELECT id FROM video_comments WHERE videoid = ? ORDER BY created DESC';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->execute();

		// TODO: pagination

		$comments = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($comments, new Comment($this->dbh, $this->vid, (int)$row['id']));
		}

		return $comments;
	}

	/**
	 * Returns all the comments by the user, sorted by newest first.
	 *
	 * @param  User      $user User to query for comments (optional).
	 * @return array           An array of Comment objects, sorted by newest comment first.
	 * @throws Exception       If the database query failed.
	 */
	public function getAllFromUser(User $user = null){
		$stmt = 'SELECT id FROM video_comments WHERE userid = ? ORDER BY created DESC';
		$query = $this->dbh->prepare($stmt);

		if(isset($user))
			$query->bindValue(1, $user->getID(), PDO::PARAM_INT);
		else
			$query->bindValue(1, $this->user->getID(), PDO::PARAM_INT);

		$query->execute();

		// TODO: pagination

		$comments = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($comments, new Comment($this->dbh, -1, (int)$row['id']));
		}

		return $comments;
	}

	/**
	 * Returns the video the comment is for.
	 *
	 * @return Video     Video object.
	 * @throws Exception If the database query failed, or if there is no video.
	 */
	public function getVideo(){
		if($this->vid == -1)
			throw new Exception('No video ID context', 1);

		return new Video($this->dbh, $this->vid);
	}

	/**
	 * Adds a comment to the video.
	 *
	 * @param  string    $comment The comment body.
	 * @return int       $id      The comment's ID.
	 * @throws Exception          If the database query failed, or if the user doesn't exist or is not a student.
	 */
	public function create(string $comment){
		if(!($this->user->isStudent() || $this->user->isTeacher()))
			throw new Exception('Only students and teachers can add comments', 1);

		if($this->vid <= 0)
			throw new Exception('No video ID context', 2);

		$comment = trim($comment);
		if(strlen($comment) < 10 || strlen($comment) > 500)
			throw new Exception('Comment content is out of bounds', 3);

		$stmt = 'INSERT INTO video_comments (videoid, userid, comment) VALUES (?, ?, ?)';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->vid, PDO::PARAM_INT);
		$query->bindValue(2, $this->user->getID(), PDO::PARAM_INT);
		$query->bindValue(3, $comment, PDO::PARAM_STR);
		$query->execute();

		$this->cid = $this->dbh->lastInsertId();
		return $this->cid;
	}

	/**
	 * Refreshes the comment metadata from the database.
	 *
	 * @throws Exception If the database query failed.
	 */
	private function refresh(){
		$stmt = 'SELECT * FROM video_comments WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->cid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Comment doesn\'t exist', 1);
		else {
			// update info
			$this->vid     = $row['videoid'];
			$this->comment = $row['comment'];
			$this->created = new DateTime($row['created']);
			$this->author  = new User($this->dbh, $row['userid']);
		}
	}
}
