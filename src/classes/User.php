<?php

require_once '/var/www/html/classes/DB.php';

// where the user class is used, a session needs to exist
session_start();

/**
 * Abstract class that holds constants for user types.
 *
 * Think of this as an enum, as PHP doesn't have those.
 */
abstract class UserType {
	const Student = 'student';
	const Teacher = 'teacher';
	const Admin   = 'admin';
}

/**
 * Class that holds information about a user.
 *
 * Think of this as a struct, as PHP doesn't have those.
 */
class UserInfo {
	// these are strings (why the hell doesn't PHP allow type enforcing?).
	public $givenName = '';
	public $familyName = '';
	public $email = '';
	public $password = '';

	// these are booleans.
	public $requesting = false;
}

/**
 * Functional class that handles users.
 * Extends UserInfo to use the same variables.
 *
 * Note: the variables are public, but they _should_ be private. Meh.
 */
class User extends UserInfo {
	// these are instances of the PDO class.
	private $dbh = null;

	// these are integers.
	protected $uid = -1;

	// these are booleans.
	protected $exists = false;

	// these are instances of the UserType class.
	protected $userType = UserType::Student;

	/**
	 *
	 * @param  PDO $dbh  Existing PDO connection.
	 * @param  int $id   (optional) Existing user ID to fetch data for.
	 * @throws Exception If the database connection/query failed.
	 */
	function __construct(PDO $dbh, int $id = -1){
		$this->dbh = $dbh;
		$this->exists = false;

		if($id > -1)
			$this->uid = $id;
		elseif(isset($_SESSION['uid']))
			$this->uid = $_SESSION['uid'];
		else
			$this->uid = -1;

		if($this->uid > -1)
			// try {
				// refresh the data for the user
				$this->refresh();
			// } catch(Exception $e){
				// do nothing
			// }
	}

	/**
	 * Searches for a user based on the provided query.
	 * This will match against both family- and birth name.
	 *
	 * @param  PDO    $dbh   Existing PDO connection.
	 * @param  string $query Partial/full information about a user's name.
	 * @return array         Indexed array of User objects that matched the query.
	 */
	public function search(string $q){
		$stmt = "SELECT id FROM users WHERE givenname || ' ' || familyname ILIKE '%' || ? || '%'";
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $q, PDO::PARAM_STR);
		$query->execute();

		$users = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($users, new User($this->dbh, $row['id']));
		}

		return $users;
	}

	/**
	 * Returns the user's ID.
	 *
	 * @return int The user's ID.
	 */
	public function getID(){
		return $this->uid;
	}

	/**
	 * Returns all users in the system.
	 *
	 * @return array     An array of User objects.
	 * @throws Exception If the database query failed.
	 */
	public function getAll(){
		$stmt = 'SELECT id FROM users';
		$query = $this->dbh->prepare($stmt);
		$query->execute();

		// TODO: pagination

		$users = array();
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){
			array_push($users, new User($this->dbh, $row['id']));
		}

		return $users;
	}

	/**
	 * Sets the given user's type.
	 *
	 * Only admin users are able to use this.
	 *
	 * @param  User      $user User information, including the new user type to set.
	 * @throws Exception       If the database query failed, invalid user, or if unauthorized access.
	 */
	public function changeUserType(User $target, $userType){
		if(!$this->isAdmin())
			throw new Exception('Unauthorized access', 1);

		if(!$target->exists)
			throw new Exception('Invalid user info', 2);

		// we'll update the user type AND clear the verify flag
		// we want it to be true if the user has registered with the checkbox, then remove it
		// once the type has been set, irregardles of whether the flag was true in the first
		// place
		$stmt = 'UPDATE users SET type = ?, verifyme = FALSE WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $userType, PDO::PARAM_STR);
		$query->bindValue(2, $target->uid, PDO::PARAM_INT);

		if(!$query->execute())
			throw new Exception('Unable to update the user', 3);
	}

	/**
	 * Checks and returns if the user is a student.
	 *
	 * @return bool If the user is a student or not.
	 */
	public function isStudent(){
		return ($this->userType == UserType::Student);
	}

	/**
	 * Checks and returns if the user is a teacher.
	 *
	 * @return bool If the user is a teacher or not.
	 */
	public function isTeacher(){
		return ($this->userType == UserType::Teacher);
	}

	/**
	 * Checks and returns if the user is an admin.
	 *
	 * @return bool If the user is an admin or not.
	 */
	public function isAdmin(){
		return ($this->userType == UserType::Admin);
	}

	/**
	 * Returns user type.
	 *
	 * @return string User's type.
	 */
	public function getType(){
		return $this->userType;
	}

	/**
	 * Checks if a user is requesting to become a teacher.
	 *
	 * @return bool If the user is requesting or not.
	 */
	public function isRequesting(){
		return !!$this->requesting;
	}

	/**
	 * Registers a new user.
	 *
	 * If the user attempts to register as an admin, an exception will be thrown.
	 * If the user attempts to register as a teacher, he/she will be registered
	 * as a student and put in a queue for promotion by one of the existing admins.
	 *
	 * @throws Exception If the database query failed, there was missing/incorrect user info.
	 */
	public function register(){
		$this->email = trim(strtolower($this->email));
		if(empty($this->email) || strlen($this->email) < 5 || strlen($this->email) > 80)
			throw new Exception('Email is either empty or out of bounds', 1);

		if(!filter_var($this->email, FILTER_VALIDATE_EMAIL))
			throw new Exception('Email is not valid', 1);

		if(empty($this->password) || strlen($this->password) < 5 || strlen($this->password) > 72)
			throw new Exception('Password is either empty or out of bounds', 2);

		$this->givenName = trim($this->givenName);
		if(empty($this->givenName) || strlen($this->givenName) < 3 || strlen($this->givenName) > 80)
			throw new Exception('Given name is either empty or out of bounds', 3);

		$this->familyName = trim($this->familyName);
		if(empty($this->familyName) || strlen($this->familyName) < 3 || strlen($this->familyName) > 80)
			throw new Exception('Family name is either empty or out of bounds', 4);

		$stmt = "INSERT INTO users(email, password, givenname, familyname, verifyme) VALUES(?, crypt(?, gen_salt('bf')), ?, ?, ?)";
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->email, PDO::PARAM_STR);
		$query->bindValue(2, $this->password, PDO::PARAM_STR);
		$query->bindValue(3, $this->givenName, PDO::PARAM_STR);
		$query->bindValue(4, $this->familyName, PDO::PARAM_STR);
		$query->bindValue(5, $this->requesting, PDO::PARAM_BOOL);
		$query->execute();

		if($query->rowCount()){
			// might as well just log the user in directly
			$this->login($this->email, $this->password);
		} else
			throw new Exception('Unable to register new user', 5);
	}

	/**
	 * Deletes the user.
	 *
	 * @throws Exception If the database query failed.
	 */
	public function destroy(){
		$stmt = 'DELETE FROM users WHERE id=?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->uid, PDO::PARAM_INT);
		$query->execute();

		// remove the session too
		$this->logout();
	}

	/**
	 * Logs the user in, setting a session cookie in the process.
	 *
	 * @param  string    $email    Email address.
	 * @param  string    $password Plain-text password.
	 * @param  bool                If the session should be persistent or not.
	 * @throws Exception           If the database query failed, or the parameters were invalid.
	 */
	public function login(string $email, string $password, bool $persistent=false){
		// TODO: handle persistency

		if(empty($email) || strlen($email) < 5 || strlen($email) > 80)
			throw new Exception('Email is either empty or out of bounds', 1);

		if(empty($password) || strlen($password) < 5 || strlen($password) > 72)
			throw new Exception('Password is either empty or out of bounds', 2);

		$query = $this->dbh->prepare('SELECT * FROM users WHERE email = ? AND password = crypt(?, password)');
		$query->bindValue(1, $email, PDO::PARAM_STR);
		$query->bindValue(2, $password, PDO::PARAM_STR);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('Invalid credentials', 3);
		else {
			$this->uid = $row['id'];

			// refresh the session
			$this->refresh();

			// set the session
			$_SESSION['uid'] = $this->uid;
		}
	}

	/**
	 * Checks if the current user is logged in or not.
	 *
	 * @return boolean If the user is logged in or not.
	 */
	public function loggedIn(){
		return ($this->exists && $this->uid > -1);
	}

	/**
	 * Logs the user out by killing the session.
	 * Can be called without creating a User object.
	 */
	public static function logout(){
		unset($_SESSION['uid']);
	}

	/**
	 * Refreshes the session and updates the user information.
	 *
	 * @throws Exception If the database query failed.
	 */
	private function refresh(){
		// fetch latest info from database
		$stmt = 'SELECT * FROM users WHERE id = ?';
		$query = $this->dbh->prepare($stmt);
		$query->bindValue(1, $this->uid, PDO::PARAM_INT);
		$query->execute();

		$row = $query->fetch(PDO::FETCH_ASSOC);
		if(!$row)
			throw new Exception('User doesn\'t exist', 1);
		else {
			// update info
			$this->givenName = $row['givenname'];
			$this->familyName = $row['familyname'];
			$this->email = $row['email'];
			$this->userType = $row['type'];
			$this->requesting = $row['verifyme'];
			$this->exists = true;
		}
	}
}
