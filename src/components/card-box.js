import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-lumo-styles/all-imports.js'

/**
 * A box to put other boxes in. It also has a slot for an optional header.
 * Very much inspired by the cards from Bootstrap.
 */
class CardBox extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'card-box'
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			div {
				display: flex;
				flex-direction: column;
				border: 1px solid #dfdfdf;
				border-radius: 0.25rem;
				margin: 20px 0;
			}

			header {
				border-bottom: 1px solid #dfdfdf;
				background: var(--app-background);
				padding: 0.75rem 1.25rem;
				display: flex;
				justify-content: space-between;
			}

			header > ::slotted(*) {
				margin: 0;
				display: inline-block;
			}

			:host([no-header]) > div > header {
				display: none;
			}

			:host([no-padding-header]) > div > header {
				padding: 0;
			}

			:host([no-margin]) > div > main {
				margin: 0;
			}

			:host([no-margin-outside]) > div {
				margin: 0;
			}

			:host([no-radius]) > div {
				border-radius: unset;
			}

			:host([no-radius-top]) > div {
				border-top-right-radius: unset;
				border-top-left-radius: unset;
			}

			:host([no-radius-bottom]) > div {
				border-bottom-right-radius: unset;
				border-bottom-left-radius: unset;
			}

			:host([no-border-top]) > div {
				border-top: none;
			}

			:host([no-border]) > div {
				border: none;
			}

			:host([no-margin-top]) > div {
				margin-top: 0;
			}

			:host([no-margin-bottom]) > div {
				margin-bottom: 0;
			}

			main {
				position: relative;
				flex: 1 1 auto;
				margin: 1.25rem;
			}

			main > ::slotted(h1),
			main > ::slotted(h2),
			main > ::slotted(h3),
			main > ::slotted(h4),
			main > ::slotted(h5),
			main > ::slotted(h6),
			main > ::slotted(p) {
				margin: 0;
			}
		`
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<style include='lumo-color lumo-typography'>
				/* polymer-cli linter breaks with empty line */
			</style>

			<div>
				<header>
					<slot name='header'></slot>
				</header>
				<main>
					<slot></slot>
				</main>
			</div>
		`
	}
}

// define the class as a component
customElements.define(CardBox.is, CardBox)
