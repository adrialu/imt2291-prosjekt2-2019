import {html, css, LitElement} from 'lit-element'

import './shared-style.js'

/**
 * Class that implements a preview box for a video.
 * Very much inspired by the cards from Bootstrap.
 */
class LectureVideoBox extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'lecture-video-box'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			link: String,
			title: String,
			thumbnail: String,
			description: String,
			meta: String,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				flex-direction: column;
				border: 1px solid #dfdfdf;
				border-radius: 0.25rem;
			}

			img {
				border-top-left-radius: 0.25rem;
				border-top-right-radius: 0.25rem;
				width: 100%;
			}

			a:not(:first-of-type),
			p.descr {
				padding: 10px;
				padding-top: 1px;
			}

			p.descr {
				height: 3em;
				overflow: hidden;
			}

			p.meta {
				align-self: center;
				color: #999;
				margin-bottom: 10px;
			}
		`
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<custom-style style='display:none;'>
				<style include='lumo-typography lumo-color shared-style'>
					/* lit-html will freak out if the style tag is empty */
				</style>
			</custom-style>

			<a href='${this.link}'><img src='${this.thumbnail}'></a>
			<a href='${this.link}'>${this.title}</a>
			<p class='descr'>${this.description}</p>
			<p class='meta'>${this.meta}</p>
		`
	}
}

// define the class as a component
customElements.define(LectureVideoBox.is, LectureVideoBox)
