import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-icons'
import '@polymer/iron-icon'

import {get, post} from './../utils/request.js'
import notify from './../utils/notify.js'

/**
 * Class that implements a 5-star rating system for videos.
 */
class LectureStars extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'lecture-stars'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			rating: Number,
			video: Number,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				width: 200px;
			}

			input {
				display: none;
			}

			label {
				color: #ddd;
				float: right;
				cursor: pointer;
				padding: 2px;
				font-size: 1.25em;
			}

			label:last-of-type {
				color: #f00 !important;
				display: none;
			}

			label:last-of-type:hover {
				color: #900 !important;
			}

			:host(:hover) > label:last-of-type {
				display: inline-block;
			}

			label:hover,
			label:hover ~ label,
			input:checked ~ label {
				color: #ffd700;
			}

			input:checked + label:hover,
			input:checked ~ label:hover,
			input:checked ~ label:hover ~ label,
			input:hover ~ input:checked ~ label {
				color: #ffed85;
			}

			input:last-of-type:checked + label {
				display: none;
			}
		`
	}

	/**
	 * Called when the component is added to DOM.
	 */
	firstUpdated(){
		// fetch the current rating from the server
		get('api/video/rate.php?vid=' + this.video).then(data => {
			// update rating
			this.rating = data.rating

			// update rendered stars
			this._updateRating()
		}).catch(err => {
			notify('Failed to get rating', err)
		})
	}

	/**
	 * Update the active stars based on current rating.
	 * Only needs to run once on load.
	 */
	_updateRating(){
		this.shadowRoot.querySelectorAll('input').forEach(star => {
			if(this.rating == star.value)
				star.checked = true
		})
	}

	/**
	 * Sets the new rating by querying the server.
	 */
	_rate(e){
		const data = new FormData()
		data.append('vid', this.video)
		data.append('rating', +e.originalTarget.value)

		post('api/video/rate.php', data).catch(err => {
			notify('Failed to set rating', err)
		}) // don't care about the normal response
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<input type='radio' id='star5' value='5' name='rating' @click='${this._rate}'>
			<label for='star5'>
				<iron-icon icon='vaadin:star'></iron-icon>
			</label>
			<input type='radio' id='star4' value='4' name='rating' @click='${this._rate}'>
			<label for='star4'>
				<iron-icon icon='vaadin:star'></iron-icon>
			</label>
			<input type='radio' id='star3' value='3' name='rating' @click='${this._rate}'>
			<label for='star3'>
				<iron-icon icon='vaadin:star'></iron-icon>
			</label>
			<input type='radio' id='star2' value='2' name='rating' @click='${this._rate}'>
			<label for='star2'>
				<iron-icon icon='vaadin:star'></iron-icon>
			</label>
			<input type='radio' id='star1' value='1' name='rating' @click='${this._rate}'>
			<label for='star1'>
				<iron-icon icon='vaadin:star'></iron-icon>
			</label>
			<input type='radio' id='star0' value='0' name='rating' @click='${this._rate}'>
			<label for='star0'>
				<iron-icon icon='vaadin:close-circle'></iron-icon>
			</label>
		`
	}
}

customElements.define(LectureStars.is, LectureStars)
