import '@polymer/polymer/polymer-element.js'

/**
 * Class that injects a set of shared styles as a CSS module.
 */
const $_documentContainer = document.createElement('template')
$_documentContainer.innerHTML = `
	<dom-module id='shared-style'>
		<template>
			<style>
				p,
				h1,
				h2,
				h3,
				h4,
				h5 {
					margin: 0;
				}

				vaadin-button {
					cursor: pointer;
				}
			</style>
		</template>
	</dom-module>
`

document.head.appendChild($_documentContainer.content)
