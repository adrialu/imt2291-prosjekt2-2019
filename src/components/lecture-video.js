import {html, css, LitElement} from 'lit-element'

import '@vaadin/vaadin-icons'
import '@vaadin/vaadin-button'
import '@vaadin/vaadin-list-box'
import '@polymer/iron-icon'

/**
 * Class that implements a video container with support for captions.
 * It also has a set of custom controls:
 * - play/pause button
 * - mute button (with volume slider sub-menu)
 * - time seeker slider
 * - playback rate menu
 * - fullscreen button
 *
 * The size of the elements are scaled based on the available screen size.
 * The captions can be clicked to navigate the video in time.
 */
class LectureVideo extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'lecture-video'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			video: String,
			caption: String,
			thumbnail: String,
			cues: Array,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
			}

			:host(:fullscreen) {
				justify-content: start;
			}

			:host(:fullscreen) video {
				width: 100%;
			}

			:host(:fullscreen) #player {
				width: 100%;
				align-self: center;
			}

			#player {
				position: relative;
			}

			video {
				width: 1066px; /* 1066x600 is 16:9 ratio */
				cursor: default;
			}

			@media only screen and (max-width: 1400px){
				video {
					width: 711px; /* 711x400 is 16:9 ratio */
				}
			}

			video.no-cursor {
				cursor: none;
			}

			#controls {
				position: absolute;
				bottom: 0;
				width: 100%;
				margin: 8px 0;
				display: grid;
				grid-template-columns: 60px 60px auto 60px 60px;
			}

			#controls[hidden] {
				display: none;
			}

			#controls vaadin-button {
				margin: 0 8px;
			}

			#controls iron-icon {
				margin-left: -2px;
			}

			#controls .menu {
				position: absolute;
				bottom: 50px;
				height: 100px;
				border-radius: 6px;
				background: rgba(100, 100, 100, 0.8);
				padding: 10px;
			}

			#controls #change-speed .menu {
				right: 0;
				width: 55px;
				text-align: left;
				font-size: var(--lumo-font-size-m);
				display: flex;
			}

			#controls #toggle-mute .menu {
				left: 0;
				width: 24px;
			}

			#controls #volume-bar {
				width: 16px;
				height: 96px;
				-webkit-appearance: slider-vertical;
			}

			#controls #change-speed .menu[hidden] {
				display: none;
			}

			#controls #change-speed .menu b {
				text-decoration: underline;
			}

			#controls #change-speed .menu vaadin-item:hover {
				background-color: #aaa;
				cursor: pointer;
			}

			/* fancy range inputs for Firefox */
			@-moz-document url-prefix() {
				#controls input[type=range] {
					margin: 8px;
					background: transparent;
				}

				#controls input[type=range][orient=vertical] {
					margin: 0;
				}

				#controls input[type=range]:focus {
					outline: none
				}

				#controls input[type=range]::-moz-range-track {
					height: 12px;
					cursor: pointer;
					border-radius: 8px;
					background: var(--lumo-primary-color);
				}

				#controls input[type=range][orient=vertical]::-moz-range-track {
					height: 92px;
					width: 12px;
				}

				#controls input[type=range]::-moz-range-thumb {
					height: 20px;
					width: 20px;
					border: 1px solid #ccc;
					border-radius: 10px;
					background-color: white;
				}
			}

			#caption {
				overflow: scroll;
				scrollbar-width: none;
			}

			#caption::-webkit-scrollbar {
				width: 0px !important;
				height: 0px !important;
			}

			#caption ul {
				list-style: none;
				padding-left: 0;
				padding-top: 150px;
				padding-bottom: 150px;
			}

			#caption li {
				cursor: pointer;
				padding: 8px 5px;
			}

			#caption li.active {
				background: #444;
				color: white;
			}

			#caption li:not(.active) {
				color: #888;
			}

			#caption li:hover {
				background: #333;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// init
		this.cues = []
	}

	/**
	 * Triggered when the track is loaded in the view.
	 * Loads, hides and stores the original cues, then injects elements into the 'caption' list.
	 */
	_loadCaption(e){
		const video = this.shadowRoot.querySelector('video');
		const list = this.shadowRoot.querySelector('#caption ul')

		// hide the original track on load
		const track = (e.target.track || video.textTracks[0])
		track.mode = 'hidden'

		// listen to cue changes
		track.addEventListener('cuechange', e => this._updateCaption(e))

		// store the cues
		for(let i = 0; i < track.cues.length; i++){
			const cue = track.cues[i]
			this.cues.push(cue)

			// create and inject cue element(s)
			const li = document.createElement('li')
			li.dataset.id = cue.id
			li.innerHTML = cue.text
			li.addEventListener('click', e => this._selectCaption(e))

			list.appendChild(li)
		}
	}

	/**
	 * Triggered when a cue is clicked from the caption list.
	 * Will seek the video to the appropriate time.
	 */
	_selectCaption(e){
		const target = (e.originalTarget || e.target)
		const id = target.dataset.id - 1

		// wind video
		this.shadowRoot.querySelector('video').currentTime = this.cues[id].startTime
	}

	/**
	 * Selects a cue from the caption list based on the video current time.
	 * It also scrolls the caption list so the current cue is centered(-ish).
	 */
	_updateCaption(e){
		// remove active state from all cues
		const caption = this.shadowRoot.getElementById('caption')
		caption.querySelectorAll('li').forEach(el => el.classList.remove('active'))

		// iterate through the active cues
		const active = e.target.activeCues
		for(let i = 0; i < active.length; i++){
			// add active state to current cue
			const cue = caption.querySelector(`li[data-id='${active[i].id}']`)
			cue.classList.add('active')

			// get height of parent frame
			const height = caption.offsetHeight

			// center the cue (ish)
			caption.scrollTo({
				top: cue.offsetTop - (height / 2),
				left: 0,
				behavior: 'smooth'
			})
		}
	}

	/**
	 * Toggle video playing state, and update the play/pause button icon.
	 */
	_togglePlay(){
		const video = this.shadowRoot.querySelector('video')
		video.paused ? video.play() : video.pause()

		const icon = this.shadowRoot.querySelector('#toggle-play iron-icon')
		icon.icon = video.paused ? 'vaadin:play' : 'vaadin:pause'
	}

	/**
	 * Pause the video.
	 */
	_pause(){
		this.shadowRoot.querySelector('video').pause()
	}

	/**
	 * Play the video.
	 */
	_play(){
		this.shadowRoot.querySelector('video').play()
	}

	/**
	 * Toggle volume mute, and update the mute button icon.
	 */
	_toggleMute(e){
		const button = this.shadowRoot.getElementById('toggle-mute')

		// make sure we're clicking the mute button directly, and not the volume slider
		let target = (e.originalTarget || e.path[0])
		if(target.offsetParent != button)
			return

		const video = this.shadowRoot.querySelector('video')
		video.muted = !video.muted

		const icon = button.querySelector('iron-icon')
		icon.icon = video.muted ? 'vaadin:volume-off' : 'vaadin:volume-up'
	}

	/**
	 * Updates the video seek bar based on duration viewed.
	 */
	_updateTime(){
		const video = this.shadowRoot.querySelector('video')
		const input = this.shadowRoot.getElementById('seek-bar')
		input.value = (100 / video.duration) * video.currentTime
	}

	/**
	 * Changes the video volume based on the volume slider.
	 */
	_changeVolume(){
		const video = this.shadowRoot.querySelector('video')
		const input = this.shadowRoot.getElementById('volume-bar')
		video.volume = input.value
	}

	/**
	 * Triggered when mouse enters the mute button or volume slider box.
	 * Will show the volume slider box.
	 */
	_volumeEnter(e){
		// clear the timeout set in _volumeLeave
		if(this.volumeMenuTimeout){
			clearTimeout(this.volumeMenuTimeout)
			this.volumeMenuTimeout = null
		}

		// show the volume slider box
		this.shadowRoot.querySelector('#controls #toggle-mute .menu').removeAttribute('hidden')
	}

	/**
	 * Triggered when mouse leaves the mute button or volume slider box.
	 * Will initiate a 0.5 second timeout that hides the volume slider box.
	 */
	_volumeLeave(e){
		// initiate a 0.5 second timeout
		this.volumeMenuTimeout = setTimeout(_ => {
			// hide the volume slider box
			this.shadowRoot.querySelector('#controls #toggle-mute .menu').setAttribute('hidden', '')
		}, 500)
	}

	/**
	 * Triggered when the mouse double-clicks the video or clicks the fullscreen button.
	 * Will toggle fullscreen mode for the video's parent element.
	 */
	_toggleFullscreen(){
		if(document.fullscreenElement)
			document.exitFullscreen()
		else
			this.shadowRoot.host.requestFullscreen()
	}

	/**
	 * Seeks to a specific time by the seek bar.
	 *
	 * FIXME: this doesn't work with Chrome (server compatibility with serving bytes?), so it's
	 * disabled.
	 */
	_changeTime(){
		// bail out if we're on chrome
		if(/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor))
			return

		const video = this.shadowRoot.querySelector('video')
		const input = this.shadowRoot.getElementById('seek-bar')
		video.currentTime = video.duration * (input.value / 100)
	}

	/**
	 * Triggered by clicking the video playback speed button.
	 * Will toggle the video playback speed menu.
	 */
	_toggleSpeedMenu(){
		const menu = this.shadowRoot.querySelector('#controls #change-speed .menu')
		if(menu.hasAttribute('hidden'))
			menu.removeAttribute('hidden')
		else
			menu.setAttribute('hidden', '')
	}

	/**
	 * Triggered by clicking one of the entries in the playback speed menu.
	 * Will change the video playback speed.
	 */
	_changeSpeed(e){
		let target = (e.originalTarget || e.path[0])
		const video = this.shadowRoot.querySelector('video')
		video.playbackRate = target.dataset.speed
	}

	/**
	 * After a 2.5 second delay this function will hide the cursor if it stopped moving and if the
	 * video is still playing. It also hides the cursor if it's resting on top of the video.
	 */
	_toggleControls(){
		// clear any existing timeout
		if(this._controlHideTimeout){
			clearInterval(this._controlHideTimeout)
			this._controlHideTimeout = null
		}

		// make sure the controls are shown, if this function is called the mouse is moving
		// around the video element
		const controls = this.shadowRoot.getElementById('controls')
		controls.removeAttribute('hidden')

		// also make sure the cursor is shown
		const video = this.shadowRoot.querySelector('video')
		video.classList.remove('no-cursor')

		// reset the hide delay
		this._controlHideDelay = 1
		this._controlHideTimeout = setInterval(_ => {
			if(this._controlHideDelay == 5){
				// if 2.5 seconds went past (500ms on the interval, counted 5 times) we'll want to
				// hide the menu and the cursor, but only if the video is playing
				if(!video.paused){
					controls.setAttribute('hidden', '')
					video.classList.add('no-cursor')

				}

				// stop the timeout so it doesn't continue running, regardless
				clearInterval(this._controlHideTimeout)
			}

			this._controlHideDelay = this._controlHideDelay + 1
		}, 500)
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<main id='player'>
				<video poster='${this.thumbnail}' @click='${this._togglePlay}' @dblclick='${this._toggleFullscreen}' @timeupdate='${this._updateTime}' @mousemove='${this._toggleControls}' crossorigin='anonymous'>
					<source src='${this.video}'>
					<track default src='${this.caption}' @load='${this._loadCaption}'>
				</video>
				<div id='controls'>
					<vaadin-button @click='${this._togglePlay}' id='toggle-play' theme='primary icon large'>
						<iron-icon icon='vaadin:play'></iron-icon>
					</vaadin-button>
					<vaadin-button @click='${this._toggleMute}' @mouseover='${this._volumeEnter}' @mouseout='${this._volumeLeave}' id='toggle-mute' theme='primary icon large'>
						<iron-icon icon='vaadin:volume-up'></iron-icon>
						<div class='menu' hidden>
							<input @change='${this._changeVolume}' type='range' id='volume-bar' min='0' max='1' step='0.1' value='0.5' orient='vertical'>
						</div>
					</vaadin-button>

					<input @change='${this._changeTime}' @mousedown='${this._pause}' @mouseup='${this._play}' type='range' id='seek-bar' value='0'>

					<vaadin-button @click='${this._toggleSpeedMenu}' id='change-speed' theme='primary icon large'>
						<iron-icon icon='vaadin:timer'></iron-icon>
						<div class='menu' hidden>
							<vaadin-list-box selected='2'>
								<b>Speed</b>
								<vaadin-item @click='${this._changeSpeed}' data-speed='2'>2x</vaadin-item>
								<vaadin-item @click='${this._changeSpeed}' data-speed='1.5'>1.5x</vaadin-item>
								<vaadin-item @click='${this._changeSpeed}' data-speed='1'>1x</vaadin-item>
								<vaadin-item @click='${this._changeSpeed}' data-speed='0.5'>0.5x</vaadin-item>
							</vaadin-list-box>
						</div>
					</vaadin-button>
					<vaadin-button @click='${this._toggleFullscreen}' id='toggle-fullscreen' theme='primary icon large'>
						<iron-icon icon='vaadin:expand-full'></iron-icon>
					</vaadin-button>
				</div>
			</main>
			<aside id='caption'>
				<ul></ul>
			</aside>
		`
	}
}

customElements.define(LectureVideo.is, LectureVideo)
