import {html, css, LitElement} from 'lit-element'
import {connectRouter} from 'lit-redux-router'

import '@vaadin/vaadin-icons/vaadin-icons.js'
import '@vaadin/vaadin-button/vaadin-button.js'
import '@vaadin/vaadin-text-field/vaadin-text-field.js'
import '@polymer/iron-form/iron-form.js'

import {get, post} from './utils/request.js'
import navigate from './utils/navigate.js'
import notify from './utils/notify.js'
import store from './redux/store.js'
import {login} from './redux/actions.js'

// connect lit-redux-router to our redux store
connectRouter(store)

/**
 *
 */
class LecturesApp extends LitElement {
	/**
	 * Element name getter for the element.
	 */
	static get is(){
		return 'lectures-app'
	}

	/**
	 * Properties getter for the element.
	 */
	static get properties(){
		return {
			user: Object,
		}
	}

	/**
	 * Style getter for the element.
	 */
	static get styles(){
		return css`
			:host {
				display: flex;
				width: 100%;
				justify-content: center;
				margin-top: 60px;
			}

			header {
				position: fixed;
				top: 0;
				left: 0;
				right: 0;
				background-color: var(--app-background);
				padding: 8px 16px;
				display: flex;
				justify-content: space-between;
				z-index: 9999;
			}

			header a {
				text-decoration: none;
			}

			vaadin-button {
				cursor: pointer;
			}

			vaadin-button.user {
				margin-right: 10px;
			}

			main {
				width: 1000px;
			}
		`
	}

	/**
	 * Constructor for the element, called when the element is loaded.
	 */
	constructor(){
		// let LitElement load its constructor first
		super()

		// load user from storage
		const state = store.getState()
		this.user = state.user

		// subscribe to state changes in storage
		store.subscribe(state => {
			// store the new state
			this.user = store.getState().user
		})

		// fetch user status from the server
		get('api/user/get.php').then(user => {
			if(user.loggedIn){
				// the user is logged in, update the state
				store.dispatch(login({
					uid: user.uid,
					name: user.name,
					type: user.type,
					isStudent: user.type == 'student',
					isTeacher: user.type == 'teacher',
					isAdmin: user.type == 'admin',
				}))
			}
		}).catch(err => {
			notify('Failed to get user status', err)
		})
	}

	/**
	 * Renderer for the element.
	 */
	render(){
		return html`
			<header>
				<!-- home button -->
				<a href='/'>
					<vaadin-button theme='primary icon' title='Home'>
						<iron-icon icon='vaadin:play-circle'></iron-icon>
					</vaadin-button>
				</a>

				<!-- search form -->
				<iron-form class='search'>
					<form>
						<vaadin-text-field placeholder='Search' name='query' clear-button-visible @keyup='${this.searchKey}'>
						</vaadin-text-field>
						<vaadin-button theme='secondary icon' @click='${this.search}'>
							<iron-icon icon='vaadin:search'></iron-icon>
						</vaadin-button>
					</form>
				</iron-form>

				<!-- user control -->
				<div>
					${this.renderSession()}
				</div>
			</header>

			<main>
				<!-- handle routes, lazy-loading components -->
				<lit-route path='/'                   component='view-home'          .resolve='${() => import("./views/home.js")}'></lit-route>
				<lit-route path='/login'              component='view-login'         .resolve='${() => import("./views/login.js")}'></lit-route>
				<lit-route path='/logout'             component='view-logout'        .resolve='${() => import("./views/logout.js")}'></lit-route>
				<lit-route path='/register'           component='view-register'      .resolve='${() => import("./views/register.js")}'></lit-route>
				<lit-route path='/new/video'          component='view-video-new'     .resolve='${() => import("./views/video-new.js")}'></lit-route>
				<lit-route path='/video/:vid'         component='view-video'         .resolve='${() => import("./views/video.js")}'></lit-route>
				<lit-route path='/edit/video/:vid'    component='view-video-edit'    .resolve='${() => import("./views/video-edit.js")}'></lit-route>
				<lit-route path='/new/playlist'       component='view-playlist-new'  .resolve='${() => import("./views/playlist-new.js")}'></lit-route>
				<lit-route path='/playlist/:pid'      component='view-playlist'      .resolve='${() => import("./views/playlist.js")}'></lit-route>
				<lit-route path='/edit/playlist/:pid' component='view-playlist-edit' .resolve='${() => import("./views/playlist-edit.js")}'></lit-route>
				<lit-route path='/user/:uid'          component='view-user'          .resolve='${() => import("./views/user.js")}'></lit-route>
				<lit-route path='/search/:query'      component='view-search'        .resolve='${() => import("./views/search.js")}'></lit-route>
				<lit-route path='/error'              component='view-error'         .resolve='${() => import("./views/error.js")}'></lit-route>
			</main>
		`
	}

	/**
	 * Renderer for the user information and buttons in the header.
	 */
	renderSession(){
		if(this.user.uid)
			return html`
				<!-- logged in users will see this -->
				<a href='/user/${this.user.uid}'>
					<vaadin-button theme='tertiary' class='user'>${this.user.name}</vaadin-button>
				</a>
				<a href='/logout'>
					<vaadin-button theme='secondary icon' title='Log out'>
						<iron-icon icon='vaadin:power-off'></iron-icon>
					</vaadin-button>
				</a>
			`
		else
			return html`
				<!-- guests will see this -->
				<a href='/login'>
					<vaadin-button theme='secondary icon' title='Log in'>
						<iron-icon icon='vaadin:user'></iron-icon>
					</vaadin-button>
				</a>
			`
	}

	/**
	 * Handles searching by serializing the form data and passing it along to the search view.
	 */
	search(){
		// grab form data and serialize it
		const form = this.shadowRoot.querySelector('iron-form.search')
		const serialized = form.serializeForm()

		// navigate to the search page and let it deal with the query
		navigate('/search/' + serialized.query)
	}

	/**
	 * Handler for keyUp on the search field, will attempt search when the Enter key is pressed.
	 */
	searchKey(e){
		if(e.keyCode == 13) // Enter key
			this.search()
	}
}

// define the class as a component
customElements.define(LecturesApp.is, LecturesApp)
