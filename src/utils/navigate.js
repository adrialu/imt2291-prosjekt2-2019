import {navigate as route} from 'lit-redux-router'
import store from './../redux/store.js'

// shorthand to trigger lit-redux-router to go to a path
const navigate = path => {
	store.dispatch(route(path))
}

export default navigate
