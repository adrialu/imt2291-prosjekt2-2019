/**
 * Formats the date from string to a "time since" string.
 */
const formatTime = str => {
	// get that and current time/date
	const date = new Date(str)
	const now = new Date()

	// if it's a long time ago, just return now
	if(date.getFullYear() != now.getFullYear())
		return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`

	// get difference
	let diff = now - date
	let diffMin = ~~(diff / 1000) / 60
	let diffHour = diffMin / 60
	let diffDay = diffHour / 24

	// return string based on difference
	if(diffMin < 1)
		return 'Just now'
	if(diffMin < 2)
		return 'A minute ago'
	if(diffHour < 1)
		return ~~diffMin + ' minutes ago'
	if(diffHour < 2)
		return 'An hour ago'
	if(diffDay < 1)
		return ~~diffHour + ' hours ago'
	if(diffDay < 2)
		return 'A day ago'
	if(diffDay < 7)
		return ~~diffDay + ' days ago'

	// if all else fails, return localized short date
	return date.toLocaleString(navigator.language, {month: 'long', day: 'numeric'})
}

export default formatTime
