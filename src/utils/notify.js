import '@vaadin/vaadin-button'
import '@vaadin/vaadin-notification'

/**
 * Displays a message in a notification box with a close button.
 */
const notify = (title, msg) => {
	// create notification box
	const box = window.document.createElement('vaadin-notification')
	box.position = 'top-center'
	box.duration = 0
	box.opened = true

	// modify how it renders
	box.renderer = (root, owner) => {
		// bail out if we've already created the contents
		if(root.firstElementChild)
			return

		// styling/positioning
		root.style.zIndex = '99999'
		root.style.top = '100px'

		// container
		const container = window.document.createElement('div')

		// title and message
		const b = window.document.createElement('b')
		b.innerText = title
		container.appendChild(b)
		container.appendChild(window.document.createElement('br'))
		container.appendChild(window.document.createTextNode(msg))
		root.appendChild(container)

		// close button
		const button = window.document.createElement('vaadin-button')
		button.textContent = 'Ok'
		button.setAttribute('theme', 'tertiary')
		button.addEventListener('click', _ => owner.close())
		root.appendChild(button)
	}

	// append to body
	window.document.body.appendChild(box)
}

export default notify
