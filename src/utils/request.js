// this is handy in some places, so expose it
export const serverURL = window.process.env.SERVER_URL

/**
 * Wrapper for 'fetch', just taking the typical tasks off our hands.
 */
const request = (path, options = {}) => {
	options.credentials = 'include'

	return fetch(serverURL + path, options).then(res => {
		return new Promise((resolve, reject) => {
			if(res.ok)
				res.json().then(resolve)
			else
				res.text().then(reject)
		})
	})
}

/**
 * Wrapper for the above function, just for GET requests.
 */
export const get = path => {
	return request(path)
}

/**
 * Wrapper for the above function, just for POST requests with data.
 */
export const post = (path, data) => {
	return request(path, {
		method: 'POST',
		body: data
	})
}

// browser backwards compatibility
const listeners = Symbol ? Symbol() : '__listeners'

/**
 * An extended version of Promise that takes a third type of listener; progress.
 * This listener stays open during the entire Promise and can be used to feed information
 * progressively along the call.
 */
class ProgressPromise extends Promise {
	constructor(callback){
		// invoke Promise's super, passing the callback
		super((resolve, reject) => callback(resolve, reject, value => {
			// here we re-use the normal resolve (then) and reject (catch) listeners.
			// we also introduce the progress listener, which will stay open for the duration
			// of the call.
			try {
				return this[listeners].forEach(listener => listener(value))
			} catch(error){
				reject(error)
			}
		}))

		this[listeners] = []
	}

	/**
	 * Handler for the 'progress' listener, which is required in use.
	 * Any invocations will pass the data to the listener, like a trickle effect.
	 */
	progress(handler){
		if(typeof handler !== 'function')
			throw new Error('Missing progress listener')
		this[listeners].push(handler)
		return this
	}
}

/**
 * Similar to the 'post' export above, just using XHR as the backend, with ProgressPromise for
 * a nicer interface.
 */
export const upload = (path, data) => {
	// create a new request object
	const xhr = new XMLHttpRequest()

	// initiate and return a ProgressPromise
	return new ProgressPromise((resolve, reject, progress) => {
		// hook listeners, some with parsed data passed through
		xhr.upload.addEventListener('progress', e => progress(e))
		xhr.upload.addEventListener('error', _ => reject(xhr.responseText))
		xhr.upload.addEventListener('abort', _ => reject(xhr.responseText))
		xhr.addEventListener('load', _ => {
			if(xhr.status < 400)
				resolve(JSON.parse(xhr.responseText))
			else
				reject(xhr.responseText)
		})

		// open connection and send data as body
		xhr.open('POST', serverURL + path)
		xhr.withCredentials = true
		xhr.send(data)
	})
}
