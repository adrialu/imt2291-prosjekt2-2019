Dette prosjektet er en fortsettelse på prosjekt 1 i den forstand at dere skal jobbe videre med et nettsted for undervisningsvideoer. I motsetning til prosjekt 1 hvor fokuset var på serversideteknologi vil nå fokuset være på klientsiden. Applikasjonen skal denne gangen utvikles som en single page application ved hjelp av Polymer (https://www.polymer-project.org/)

Mye av koden dere skrev for prosjekt 1 kan brukes opp igjen i dette prosjektet men det blir også en god del ny kode også på serversiden.

Det skal være felles pålogging for studenter, lærere og adminstratorer. Ikke påloggede brukere skal kunne søke og se på videoer/spillelister på samme måte som påloggede brukere.

Lærere kan opprette og vedlikeholde spillelister (endre beskrivelse, legge til/fjerne videoer, endre rekkefølge på videoer.) Lærere skal også kunne legge til videoer. Legg også til mulighet til å velge en vilkårlig fra fra en video som thumbnailbilde for videoen. Det skal også være mulig å laste opp teksting av videoer. Gi Lærere mulighet til å redigere en video, i redigering av video skal brukerne kunne endre thumbnailbilde, endre tittel og beskrivelse og også laste opp ny fil som inneholder teksting av videoen.

Studenter skal kunne legge til favorittspillelister og få oversikt over nye videoer i disse spillelistene.
Administratorer kan endre status for brukere (dvs at når en bruker registrerer seg og ber om å bli lærer så skal administrator få en oversikt over dette og få mulighet til å gi disse lærertilgang.)

Ved visning av video skal tekstingen vises ved siden av videoen med aktuell tekst uthevet, det skal også være mulig å klikke på en del av teksten å få videoen til å hoppe direkte til den aktuelle delen av videoen. Et godt eksempel på dette er bruken hos edX, anbefaler å melde seg på kurset «Introduksjon til JavaScript» (https://www.edx.org/course/javascript-introduction-w3cx-js-0x-0), kurset er gratis og viser både denne måten å bruke video på og lar dere laste ned videoer og tekstfiler med teksting (som enkelt kan gjøres om til VTT filer) som dere kan bruke som testfiler.
Gi mulighet for å vise video i fullskjerm (sammen med tekstingen), video bør også kunne vises i både høyere og lavere hastighet enn den vanlige (f.eks. dobbel hastighet og halv hastighet.)

# Dokumentasjon

Før dere begynner å kode skal dere lage wireframe skisser av de ulike sidene i applikasjonen, dette gjøres enklest med penn og papir. Bruk disse til å få en oversikt over funksjonaliteten til applikasjonen og hvordan denne kan implementeres. Ta bildet av disse skissene og legg dem inn i Wikien.

Alle klasser og funksjoner (både i PHP og JavaScript) skal selvfølgelig dokumenteres via kommentarer i koden. I mange tilfeller hjelper det også på lesbarheten av koden om en kommenterer if/else if/else slik at det fremkommer tydelig hva hver enkelt grein faktisk gjør (hvorfor koden kjøres.) Noen ganger er dette åpenbart, andre ganger krever det mye finlesing av koden for å finne ut hva som egentlig skjer.

I tillegg til dokumentasjon i form av kommentarer i koden skal dere lage en oversikt i Wikien over alle endepunktene dere lager i PHP, dette blir dokumentasjonen av API’en til applikasjonen. For eksempel så vil dere ha en søkefunksjon, i fra nettsiden vil dere via AJAX kalle et PHP skript for å returnere videoer og spillelister som matcher et gitt søkekriterium. Dere må da beskrive hvilke parametere skriptet forventer og hva som returneres. Med en god dokumentasjon i PHP filene kan dette kopieres derfra. Skriv denne dokumentasjonen underveis i prosessen, dere vil trenge den selv under utviklingen av applikasjonen. Grupper de ulike PHP filene etter funksjon, dvs at det blir enkelt å finne de som har med brukerrelaterte operasjoner å gjøre (login, logut, registrere bruker, liste brukere, endre brukerstatus osv).

Lag en oversikt over alle egendefinerte tagger dere har laget, beskriv hvorfor de er laget, hvilke attributter som kan settes, hvilke funksjoner som en vil kalle og hvilke events som genereres fra disse.

Lag en tegning av trestrukturen som viser hvordan applikasjonen henger sammen på klientsiden. Dvs at dere tegner ut DOM strukturen for applikasjonen (finn et hensiktsmessig nivå å gjøre dette på, det blir meningsløst å ta med absolutt hele strukturen.) Ut ifra denne trestrukturen beskriver dere kort all funksjonalitet i applikasjonen.
