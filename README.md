# Prosjekt 2 IMT2291 våren 2019

1. [Prosjektdeltakere](#prosjektdeltakere)
2. [Feil](#feil)
3. [Kjøre](#kjøre)
	- [Konfigurere](#konfigurere)
	- [Tester](#tester)

## Prosjektdeltakere

- Adrian L Lange, 473115, <adrian.lund-lange@ntnu.no>

## Feil

Har testet primært mot Firefox, så all funksjonalitet skal fungere der, mens Chrome har enkelte problemer.

1. Chrome fungerer ikke med søking i tid i videoer, men fungerer helt fint i Firefox.
    - dette skal visstnok være en feil med serveroppsettet (må sende byte ranges), noe som er satt opp men fungerer ikke likevel
    - dette betyr og at søking fra teksting fungerer ikke
2. Enkelte stiler (f.eks. søkelinjen i video) fungerer ikke med Chrome, men gjør i Firefox.

## Kjøre

Alt er pakket inn i Docker Compose, så det er bare å kjøre dette:

	docker-compose up -d
	docker-compose exec php composer install
	docker-compose exec www yarn

Når alt er oppe å kjøre skal nettsiden være tilgjengelig på <localhost:8000>.

#### Konfigurere

Det er et par ting som er pre-konfigurert, men man vil gjerne endre:

- `.env` definerer brukernavn, passord og navn til databasen
- `db/02_admin.sql` definerer brukernavn, passord og navn til administrator-brukeren
	- Denne brukeren vil alltid ha ID = 1, og kan ikke slettes

#### Tester

Codeception skal kjøre allerede i en Docker container, så man kan bruke den i kommandolinje:

	docker-compose exec tests codecept run
