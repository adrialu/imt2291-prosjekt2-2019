FROM php:7.3-apache

# install header files for PostgreSQL
RUN apt-get update
RUN apt-get install -y libpq-dev

# install PDO and PostgreSQL PDO modules
RUN docker-php-ext-install pdo pdo_pgsql

# install FFmpeg and GD for thumnail stuff
RUN apt-get install -y ffmpeg libpng-dev libfreetype6-dev libjpeg62-turbo-dev libzip-dev
RUN docker-php-ext-install zip
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install gd

# install composer
RUN curl -sS https://getcomposer.org/installer -o /tmp/composer.php
RUN php /tmp/composer.php --install-dir=/usr/local/bin --filename=composer
RUN rm /tmp/composer.php

# enable large file uploads
RUN echo 'upload_max_filesize=1G' > /usr/local/etc/php/conf.d/upload.ini

# customize the include path
RUN echo 'include_path=".:/var/www/html"' > /usr/local/etc/php/conf.d/include.ini

# create directories and set permissions
RUN mkdir -p /var/www/data/videos \
			 /var/www/data/thumbnails/videos \
			 /var/www/data/thumbnails/playlists \
			 /var/www/data/captions
RUN chown -R www-data:www-data /var/www/data
RUN chmod -R 755 /var/www/data
